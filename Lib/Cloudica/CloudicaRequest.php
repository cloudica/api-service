<?php
App::uses('Xml', 'Utility');
require_once ROOT.DS.APP_DIR.DS.'Lib'.DS.'Cake'.DS.'Network'.DS.'CakeRequest.php';
/**
 * extends the CakeRequest by wrapping GET, POST and PUT headers in one object the requestData object
 * @author Lorelie Dazo
 * @since November 2012
 * @version 1.0
 * @lastupdated November 26, 2012
 */
class CloudicaRequest extends CakeRequest
{
	/**
	 * contains all the request headers regardless of request type
	 * @var object
	 */
	public $requestData;

	/**
	 * tag if the request is an array from POST/GET/PUT or an object request created from XML/JSON request
	 * @var string
	 */
	public $requestType;

	/**
	 * overrides the CakeRequest constructor by creating requestData object from the POST, PUT and GET headers including possible CURL request that contains XML or JSON headers
	 * @param string $url
	 * @param bool $parseEnvironment
	 */
	public function __construct($url = null, $parseEnvironment = true) {
		parent::__construct($url, $parseEnvironment);
	 	$this->requestData = array();

	 	$this->requestType = "array";
		if($parseEnvironment)
		{
			if(!empty($_POST))
				$this->requestData = $this->data;
			else
			{
				if($this->isXml())
				{
					$data = file_get_contents('php://input');
					$xmlstr = trim( $data );

					$this->requestData = Xml::build($xmlstr);

					$this->requestType = "xml";
				}
				 else if($this->isJson())
				 {
				 	$data = file_get_contents('php://input');
					$jsonString = trim( stripslashes( $data ) );

					$this->requestData = json_decode($jsonString);

					$this->requestType = "json";
				 }
				 else if($this->isPut())
				 {
				 	$data = file_get_contents('php://input');
				 	parse_str($data, $this->requestData);
				 }
			}

			//push get
			if(sizeof($this->query))
			{
				foreach($this->query as $key=>$value)
				{
					if(is_array($this->query))
						$this->requestData[$key] = $value;
					else
						$this->requestData->$key = $value;
				}
			}


			if($this->requestType == "array")
				$this->requestData = (object) $this->requestData;
		}
	}

	public function isXml()
	{
		return $this->is("xml");
	}

	public function isJson()
	{
		return $this->is("json");
	}

	public function isPut()
	{
		return $this->is("put");
	}

	public function is($type)
	{
		$is = false;

		switch($type)
		{
			case "xml":
			case "json":
				if(isset($_SERVER["CONTENT_TYPE"]))
					$is = preg_match("/[a-z]*".$type."/", $_SERVER["CONTENT_TYPE"]);
				break;
			case "put":
					$is = isset($_SERVER["REQUEST_METHOD"]) && $_SERVER["REQUEST_METHOD"] == "PUT";
				break;
			default:
				$i = parent::is($type);
				break;
		}
		return $is;
	}

}
/** END OF FILE **/