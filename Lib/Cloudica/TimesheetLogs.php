<?php
/**
 * used by TimesheetController.php
 * @author Romeo Loyola
 * @since 	2014-12-05
**/
class TimesheetLogs {

	public $userId;
	
	public $machineId;
	
	public $companyId;
	
	public $localTime;
	
	public $startTime;
	
	public $endTime;
	
	public $status;
	
	public $statusMessage;
	
	public $workTime;
	
	public $machineStartTime;
	
	public $machineEndTime;
	
	public $dataCompromised;
	
	const ALLOWED_INTERVAL= 1;
	
    public function Add($log)
    {
		if ($this->allowAdd($log))
        {
            $this->machineEndTime = $log->machineEndTime;
			
			$this->endTime = $log->endTime;
			
            $this->status = $log->status;

            $this->workTime = (strtotime($this->endTime) - strtotime($this->startTime))/60;

            return true;
        }
		
        return false;
    }
	
	public function allowAdd($log)
    {
		return ($this->withinAllowedInterval($log)  //if within allowed interval
				&& ( $this->status == $log->status ? true : false )  // and same status
				? true : false );
    }

    public function withinAllowedInterval($log)
    {
        return ($this->timeDifference($log) <= self::ALLOWED_INTERVAL); 
    }

	public function timeDifference($log)
    {
        if (!isset($log)) return 0;
			
        return (strtotime($log->startTime) - strtotime($this->endTime))/60; //convert sec to minutes
    }

	public function toCSV($extraValues)
	{
		$values =  array_merge(get_object_vars($this),$extraValues);
		
		$csvArray = array();
		
		foreach($values as $val){
			if($val=="NOW()")
				$csvArray[] .= $val;
			else
				$csvArray[] .= '"' . $val .  '"';
		}

		return "(" .  implode(",",$csvArray ).")";
	}

}
?>