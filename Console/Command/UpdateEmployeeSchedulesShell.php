<?php
	require_once('AppShell.php');

 	class UpdateEmployeeSchedulesShell extends AppShell
 	{
		var $uses = array("ShiftSchedule", "Company", "User", "Minion");
		static $log;
		static $today;


		var $operations = array(); //operations per instance
		var $Companies = array(); //params per company

		function __validateDate($date)
		{
			$d = DateTime::createFromFormat('Y-m-d', $date);
    		return $d && $d->format('Y-m-d') == $date;
		}

		function startup()
		{
			$today = new DateTime();
			$today = $today->format("Y-m-d H:i:s");

			self::$today = $today;
			self::$log = new Log( 'UpdateEmployeeSchedules', 'Update Employee Schedules');

			self::$log->lwrite("+++++++++++++++++++++++++++++++++++++++++++++");
		}

		var $mode = "all";


 		function main()
		{
			$log = self::$log;
			$log->lwrite("Start Routine");
	 		$options = array("fields"=> array("company_id", "company_domain"));

			$options['conditions'] = array("company_id" => 2);
			try
			{
				$companies = $this->Company->find("all", $options);


				$log->lwrite("There are ".sizeof($companies)." companies");

				foreach($companies as $company)
				{
					$company = $company[$this->Company->alias];
					try
					{
						$log->lwrite(json_encode($company));
						$this->Run($company);
						$log->lwrite("------------------");
					}
					catch(Exception $e)
					{
						$log->lwrite("Unable to continue routine");
						$log->lwrite($e->getMessage());
					}
				}

				$log->lwrite("End Routine");
			}
			catch(Exception $e)
			{
				$log->lwrite("Failed to perform routine");
				$log->lwrite($e->getMessage());
			}

		}

		function Run($company)
		{
			$log = self::$log;

			//check if i can connect to minion
			$Changes = $this->__importModel("EmployeeScheduleChanges", $company);
			$Employee = $this->__importModel("Employee", $company);

			$dbconfig = $Employee->useDbConfig;
			$Employee->Schedule->schemaName =
			$Employee->Schedule->useDbConfig = $this->Employee->useDbConfig;


			if(!$Changes)
			{
				throw new Exception("Unable to import db for company");
			}

			$changes = $Changes->GetScheduleChanges();
			$employeeIds = Set::extract($changes, "{n}.EmployeeScheduleChanges.employee_id");
			$changes = Set::combine($changes, "{n}.EmployeeScheduleChanges.user_id", "{n}.EmployeeScheduleChanges");
			$userIds = array_keys($changes);

			$employees =  $Employee->find("all", array(
				"conditions" => array(
					"parent_id" => 0,
					"Employee.id" => $employeeIds
				)
			));

			$employees = Set::combine($employees, "{n}.Employee.id", "{n}.Employee");
			debug($employees);


			$users = $this->User->find("all", array("conditions"=> array("user_id"=>$userIds), "fields"=> array("timezone", "user_id")));
			$users = Set::combine($users, "{n}.User.user_id", "{n}.User.timezone");

			foreach($changes as $userId => $change)
			{
				$timezone = isset($users[$userId]) ? $users[$userId] : false;

				$today = new DateTime();
 				$changesStartDate = DateTime::createFromFormat('Y-m-d', $change['start_date']);

				if($timezone)
				{
					$timezone = new DateTimeZone($timezone);
					$today = new DateTime(null, $timezone);
 					$changesStartDate = DateTime::createFromFormat('Y-m-d H:i:s', $change['start_date']. ' 00:00:00', $timezone);
				}

				$log->lwrite("User " . $userId . " time: " .$today->format("Y-m-d h:i:s a"));
				$log->lwrite("User " . $userId . " time: " .$changesStartDate->format("Y-m-d h:i:s a"));

				$shell = 'UpdateEmployeeSchedule';
				$applyImmediately = $today >= $changesStartDate;
 				if($applyImmediately)
 				{
 					debug("Apply changes");

					if(isset($employees[$change['employee_id']]))
					{
						$employee = $employees[$change['employee_id']];

						$employee['parent_id'] = $employee['id'];
						$employee['created_by'] = $shell;
						$employee['updated_by'] = $shell;
						$employee['created'] = date("Y-m-d H:i:s");
						unset($employee['id']);

						$Employee->create();
						$Employee->save($employee);

						$log->lwrite("Employee history written");

						$data = array(
							"id" => $change['employee_id'],
							"updated_by" => $shell,
							"schedule_id" => $change['schedule_id']
						);

						$Employee->id = $change['employee_id'];
						$Employee->save($data);
						$log->lwrite("Employee schedule updated");


						// client changes
						$ClientChanges = $this->__importModel("Changes", $company);
						$ClientChanges->session = $company;
						$clientChanges = $ClientChanges->getUserChanges($userId,  $change['employee_id']);

						if($clientChanges)
							$ClientChanges->updateColumn($clientChanges[$ClientChanges->alias]["id"], "schedules");

						$log->lwrite("Changes for schedule updated");
						$Changes->query(
							"UPDATE clm_employee_schedule_changes SET is_current = 0 WHERE id = " . $change['id']
						);

					}

 				}
				else {
					debug("Changes are not applied");
				}
			}

		}

	}
/** END OF FILE **/
