<?php
define("LATE_EARLY_MARGIN", 15); //in minutes
define("ABSENT", "Absent");
define("LATE_IN", "Late in");
define("LATE_OUT", "Late out");
define("EARLY_IN", "Early in");
define("EARLY_OUT", "Early out");
define("DATE_MIN_VALUE", "0001-01-01 00:00:00");
define("AUTOMATED_MESSAGE_FLAGGING", "Automated");
define("TIMESHEET_FLAGGING", "TimesheetFlaggingShell");
define("UTC", false);

/*
 * @last_updated 2015-09-17
 */
class TimesheetFlaggingShell extends AppShell
{

	var $name = "Test";
	var $arguments = array();

	static $log;
	static $today; //DateTime based on Server (TimesheetFlaggingDate)
	static $startDay; //DateTime based on Server (TimesheetFlaggingDate)
	static $endDay; //DateTime based on Server (TimesheetFlaggingDate)

	var $manual = false;

	var $uses = array("Company", "Account", "User");

	/**
	 * Entry Point of Code
	 */
	 function main()
	 {
			self::$log = new Log(TIMESHEET_FLAGGING, TIMESHEET_FLAGGING);
			self::$log->lwrite("Start TimesheetFlagging Routine");

			$this->arguments = array("companies"=>array());

			//check for arguments (command line entry)
			$manual = isset($this->args[0]) && $this->args[0] == "true";

			if($manual)
			{
				$this->__enterArgumentsManually();
			}
			else
			{
				$this->__startUpArguments();
			}

			self::$log->lwrite("Date Range: ". self::$startDay->format("Y-m-d") . " - " . self::$endDay->format("Y-m-d"));

			if(sizeof(array_keys($this->arguments["companies"])) == 0)
			{
				self::$log->lwrite("No companies to flag");
			}

			$companies = $this->Company->find("all", array(
				"fields" => array("company_id", "company_domain", "company_name"),
				"conditions"=>array("is_deleted"=>0, "company_id"=>array_keys($this->arguments["companies"])))
			);

			$companies = Set::combine($companies, "{n}.".$this->Company->alias.".company_id", "{n}.".$this->Company->alias);

			foreach($this->arguments["companies"] as $companyId => &$companyArguments)
			{
				if(isset($companies[$companyId]))
				{
					$companyArguments = array_merge($companyArguments, $companies[$companyId]);

					self::$log->lwrite("Start for company: " . $companyId);

					try
					{
						if(!isset($companyArguments["users"])) $companyArguments["users"] = array();

						$this->__getUsers($companyId);


						//RESET SESSION
						unset($this->ShiftSchedule);
						unset($this->Timesheet);
						unset($this->TimesheetFlag);
						unset($this->NoWork);
						unset($this->MethodCall);
						unset($this->StatusLog);

						//initialize session
						$this->session = array(
							"company_id" => $companyArguments["company_id"],
							"company_domain" => $companyArguments["company_domain"]
						);

						//REIMPORT MODELS
						$this->__importModel("ShiftSchedule", $this->session);
						$this->__importModel("Timesheet", $this->session);

						$this->Timesheet->fields = "*";

						$this->__importModel("TimesheetFlag", $this->session);
						$this->__importModel("NoWork", $this->session);
						$this->__importModel("MethodCall", $this->session);
						$this->__importModel("StatusLog", $this->session);

						$companyId = (int) $companyId;

						if(sizeof(array_keys($companyArguments["users"])) > 0)
						{
							foreach($companyArguments["users"] as $userId => &$user)
							{
								self::$log->lwrite("Start for user: $userId ") ; // removed because user is not in utf8. json_encode($user));
								$employeeId = (int) $user["employee_id"];


								if($employeeId > 0)
								{
									$schedule = $this->ShiftSchedule->getScheduleDetailsEmployeeId($employeeId);

									self::$log->lwrite("User schedule details: " . json_encode($schedule));
									if(isset($schedule["id"]))
									{
										$schedule["shifts"] = $this->ShiftSchedule->getSchedule($schedule["id"], $schedule["schedule_type"], false);

									}
									else {
										if(isset($user['timezone']))
										{
											$schedule = array(
												'timezone' => $user['timezone'],
												'schedule_type' => 'Fixed',
												'id' => 0
											);
										}
										else
										{
											self::$log->lwrite("User has no defined timezone");
										}
									}
								}
								else
								{
									$schedule = array(
										'timezone' => $user['timezone'],
										'schedule_type' => 'Fixed',
										'id' => 0
									);
									self::$log->lwrite("Employee Profile Not Defined");
								}

								$this->__updateSchedule($schedule);

								$user["schedule"] = $schedule;

								$this->__updateTimesheets($user);
								$this->__checkForAbsences($user);


								self::$log->lwrite("End for user: " . $userId);
							}
						}
						else
						{
							self::$log->lwrite("No users to flag");
						}
					}
					catch (Exception $e)
					{
						self::$log->lwrite("Unable to continue: ". $e->getMessage());
					}

					self::$log->lwrite("End for company: " . $companyId);
				}
				else
				{
					self::$log->lwrite("Company $companyId is not defined");
				}
			}

			self::$log->lwrite("End TimesheetFlagging Routine");

	 }


	//allow entering company_id, user_id, and start_time/end_time
	// starttime and endtime are strings
	 function __enterArgumentsManually()
	 {
	 	$this->out("Manually input arguments");

		$companyId = (int) $this->in("Enter company_id");

		if(!$companyId)
		{
			$this->out("Invalid input");
			return;
		}

		$this->arguments["companies"][$companyId] = array("users"=>array()) ;

		$localDate =   $this->in("Enter Date (YYYY-MM-DD)");

		if(strtotime($localDate))
		{
			self::$startDay = new TimesheetFlaggingDate($localDate);
			self::$endDay = new TimesheetFlaggingDate($localDate);
		}
		else
		{
			$this->out("Invalid input");
			self::$endDay =  self::$startDay = new TimesheetFlaggingDate(  new DateTime('now', new DateTimeZone('UTC')));

			return;
		}

		$user_id = (int) $this->in("Enter user_id");

		if($user_id)
			$this->arguments["companies"][$companyId]["users"][$user_id]  = array();

		$this->manual = true;
	 }

	// automatically assigns arguments
	// automatically assign date to today (UTC) startday and enday are date objects
	 function __startUpArguments()
	{
		$end =  date('Y-m-d'); //"now";
        if (date('w') === '0') {
            $start = $end;
        } else {
            $start = date('Y-m-d', strtotime('last Sunday')); //"this week";
        }

 		self::$endDay = self::$today = new TimesheetFlaggingDate(new DateTime($end, new DateTimeZone('UTC')));

		//start Sunday
		$startDay = new DateTime($start, new DateTimeZone("UTC"));
		$startDay->sub(new DateInterval("P1D"));
		self::$startDay = new TimesheetFlaggingDate($startDay);

		$this->arguments = array("companies" => array(2=>array()));

	}

	//queries user and account info including employeeId
	function __getUsers($companyId)
	{
		if(!is_int($companyId))
		{
			 throw new Exception("Invalid Arguments");
		}

		self::$log->lwrite("Fetching list of users");

		//$filterusers
		$users = array_keys($this->arguments["companies"][$companyId]["users"] );

		$conditions =array(
			"company_id" => $companyId,
			"is_deleted"=>0
		);

		if(sizeof($users) != 0 ) $conditions["user_id"] =  $users;

		self::$log->lwrite("Conditions: " . json_encode($conditions));

		$accounts =  $this->Account->find("all", array(
			"conditions"=>$conditions, "recursive"=>-1,
			"fields" => array("user_id", "employee_id")
		));
		$userIds = Set::extract("{n}.".$this->Account->alias.".user_id", $accounts);
		$users = $this->User->find("all", array(
				"fields" => array("user_id", "user_email", "firstname", "lastname", "timezone"),
				"conditions"=>array("user_id" =>$userIds)
			)
		);

		$users = Set::combine($users, "{n}.".$this->User->alias.".user_id",  "{n}.".$this->User->alias);
		$employeeIds = array();

		foreach($accounts as $account)
		{
			$account =  $account[$this->Account->alias];
			$userId = $account["user_id"];
			if(isset($users[$userId]))
			{
				$employeeIds[] = $users[$userId]['employee_id'] = $account["employee_id"];
			}
		}

		$this->arguments["companies"][$companyId]["users"] = $users;

		self::$log->lwrite("Company has " . sizeof($users) . " users");
	}

	//updates schedule details with proper start time and end time in datetime format
	// schedule shifts is always based on the timezone of schedule
	function __updateSchedule($schedule)
	{
		if(!$schedule) throw new Exception("Invalid Arguments");

		if(!UTC)
		{
			self::$startDay->setTimezone(new DateTimeZone($schedule['timezone']));
			self::$endDay->setTimezone(new DateTimeZone($schedule['timezone']));
		}

		$schedule['utc_start_date'] = self::$startDay->format("Y-m-d");
		$schedule['utc_end_date'] = self::$endDay->format("Y-m-d");

		extract($schedule);
		if(!isset($schedule["timezone"]) || !$schedule["timezone"])
		{
			self::$log->lwrite("Schedule Timezone is not defined");
		}
		else
		{
			$startDate = self::$startDay->setTimezone($schedule['timezone']);
			$endDate =  self::$endDay->setTimezone($schedule['timezone']);
			$schedule['start_date'] = $startDate->format("Y-m-d");
			$schedule['end_date'] = $endDate->format("Y-m-d");
			$shifts = isset($schedule['shifts']) ? $schedule['shifts'] : array();

			self::$log->lwrite("Updating schedule from " . $startDate->format("Y-m-d"). ' to '. $endDate->format("Y-m-d"));

			if(sizeof($shifts) == 0)
			{
				self::$log->lwrite("No defined shifts");
				//formulate a daily schedule that starts from start of date range, and end of date range
				$shifts = $this->__generateShifts($startDate, $endDate);
			}
			else
			{
				$this->__updateShifts($shifts, $startDate, $endDate, $timezone);
			}

			$schedule['shifts'] = $shifts;
		}

	}

	function __updateShifts($shifts, $startDate, $endDate, $timezone)
	{
		$_shifts = $shifts;
		$shifts = array();
		foreach($_shifts as $sched)
		{
			$scheduleId = $sched["C"]["schedule_id"];
			$sched = $sched["D"];
			$sched["schedule_id"] = $scheduleId;

			if(!isset($shifts[$sched['start_day']]))
			{
				$shifts[$sched['start_day']] = array();
			}

			$shifts[$sched['start_day']][$sched['id']] = $sched;
		}

		$diff = ($startDate->diff($endDate));

		if($diff->d > 7) throw new Exception("Date Range should only be within a week");


		$sd = clone $startDate;
		$end = false;

		while (!$end)
		{
			$end = $sd >= $endDate;
			$dayOfWeek = $sd->format("N");

			if(isset($shifts[$dayOfWeek]))
			{

				foreach($shifts[$dayOfWeek] as $shift)
				{
					$shift["local_date"] = $sd->format("Y-m-d");
					$this->updateShift($shift, $timezone);
				}
			}

			if(!$end)
			{
				$sd->add(new DateInterval("P1D"));
			}
		}
	}

	function __generateShifts($startDate, $endDate)
	{
		self::$log->lwrite("Creating schedule automatically");

		$end = false;
		$sd = clone $startDate;
		$schedule = array();
		while (!$end)
		{
			$end = $sd >= $endDate;
			$dayOfWeek = $sd->format("N");
			$date = $sd->format("Y-m-d");

			if(!isset($schedule[$dayOfWeek]))
			{
				  $schedule[$dayOfWeek]
					 =	array(
					 	0 =>
						//start date from early 00AM to 12AM
						array(
							'id'=> 0,
							'start_day' => $dayOfWeek,
							'start_time' => '00:00:00',
							'schedule_id' => 0,
							'local_date' => $date,
							'start_shift' => $date. " 00:00:00",
							'end_shift' => $date. " 11:59:59",
							"company_id" => $this->session['company_id'],
							'utc_start_shift' => $date. " 00:00:00",
							'utc_end_shift' => $date. " 11:59:59",
						)
				);
			}

			if(!$end)
			{
				$sd->add(new DateInterval("P1D"));
			}
		}
		return $schedule;
	}

	function updateShift(array $shift, $timezone)
		{
			if(!is_array($shift))
				 throw new Exception("Invalid Arguments");

			if(!isset($shift["local_date"])) return;

			if(isset($shift['end_shift']) && !AppModel::IsMinValue($shift['end_shift'])) return $shift;

			$starTime = new DateTime($shift["local_date"], new DateTimeZone($timezone));
			$time = explode(":", $shift['start_time']);
			$h = isset($time[0]) ? $time[0]  : 0;
			$m = isset($time[1]) ? $time[1]  : 0;
			$s = isset($time[2]) ? $time[2]  : 0;
			$starTime->setTime($h, $m, $s);

			$endTime = clone $starTime;

			$requiredHours = $shift['required_hours'];
			$totalMinutes = $requiredHours*60;

			if($shift['allowed_break_time'])
			{
				$totalMinutes += $shift['allowed_break_time'];
			}

			$hours = (int) ($totalMinutes / 60) ;
			$minutes = $totalMinutes % 60;
			$in = "PT". $hours . "H";
			if($minutes)
			{
				$in .= $minutes. "M";
			}

			$endTime->add(new DateInterval($in));

			$shift["start_shift"] = $starTime->format("Y-m-d H:i:s");
			$shift["end_shift"] = $endTime->format("Y-m-d H:i:s");

			$utcStartTime = clone  $starTime;
			$utcEndTime = clone  $endTime;

			if(UTC)
			{
				$utcStartTime->setTimezone(new DateTimeZone("UTC"));
				$utcEndTime->setTimezone(new DateTimeZone("UTC"));
			}

			$shift["utc_start_shift"] = $utcStartTime->format("Y-m-d H:i:s");
			$shift["utc_end_shift"] = $utcEndTime->format("Y-m-d H:i:s");
		}

	function __updateTimesheets($user)
	{
		self::$log->lwrite("Start Updating Timesheet Routine");

		if(!isset($user['user_id']))
			throw new Exception("Invalid Arguments: user_id");
		if(!isset($user['schedule']))
			throw new Exception("Invalid Arguments: schedule");
		if(!isset($user['schedule']['utc_start_date']))
			throw new Exception("Invalid Arguments: schedule utc start date");
		if(!isset($user['schedule']['utc_end_date']))
			throw new Exception("Invalid Arguments: schedule utc end date");


		$userId = $user['user_id'];
		$companyId = $this->session['company_id'];

		$conditions = array(
				"user_id" => $userId,
				"company_id" =>$this->session["company_id"],
				"DATE(local_date) >=" => $user['schedule']['utc_start_date'], //timesheet is now using utc
				"DATE(local_date) <=" => $user['schedule']['utc_end_date'], //timesheet is now using utc
				"machine_id != -1" //do not include server initiated timesheet
		);

		$fields = array("*");

		//late in field
		$fields[] = "(ABS (TIME_TO_SEC( TIMEDIFF(  start_shift,  timein ) )/60 ) > ".LATE_EARLY_MARGIN."  AND start_shift > timein) as early_in";
		//early in field
	 	$fields[] = "(ABS (TIME_TO_SEC( TIMEDIFF(  start_shift,  timein ) )/60 ) > ".LATE_EARLY_MARGIN."  AND start_shift < timein) as late_in";

		//early out field
		$fields[] = "(ABS (TIME_TO_SEC( TIMEDIFF(  end_shift,  timeout ) )/60 ) > ".LATE_EARLY_MARGIN."  AND end_shift > timeout) as early_out";
		//late out field
	 	$fields[] = "(ABS (TIME_TO_SEC( TIMEDIFF(  end_shift,  timeout ) )/60 ) > ".LATE_EARLY_MARGIN."  AND end_shift < timeout) as late_out";


		self::$log->lwrite("Searching for timesheets: ".json_encode($conditions));

		$timesheets = $this->Timesheet->find('all', array("fields"=>$fields, 'conditions'=>$conditions));

		self::$log->lwrite("Found " . sizeof($timesheets) . " timesheets");

		$users["timesheets"] = array();

		foreach($timesheets as &$timesheet)
		{
			$flags = $timesheet['0'];
			$timesheet = $timesheet[$this->Timesheet->alias];

			 self::$log->lwrite("TIMESHEET: " . json_encode($timesheet));
			 self::$log->lwrite("FLAGS: " . json_encode($flags));

			if(AppModel::IsMinValue($timesheet["timein"]) ||  AppModel::IsMinValue($timesheet["timeout"]))
			{
				if(AppModel::IsMinValue($timesheet["timein"]))
				{
					self::$log->lwrite("Timesheet has no timein");
				}

				if(AppModel::IsMinValue($timesheet["timeout"]))
				{
					self::$log->lwrite("Timesheet has no timeout");
				}
			}
			else
			{
				//check if has been flagged absent
				$this->__checkIfAbsent($timesheet);
				$this->__flag($timesheet, $flags);
				$this->__recalculateTimesheet($timesheet);
			}
		}


		self::$log->lwrite("End Updating Timesheet Routine");
	}

	function __flag($timesheet, $flags)
	{
		extract($timesheet);
		self::$log->lwrite("Start Timesheet Flagging");
		if(
			(int) $schedule_id == 0 ||
			(int) $shift_id == 0 ||
			AppModel::IsMinValue($start_shift)  &&
			AppModel::IsMinValue($end_shift)
		)
		{
			self::$log->lwrite("Timesheet has no schedule/shift, start_shift/end_shift");
			return;
		}

		$flagResult = $this->TimesheetFlag->find("all", array("conditions"=>array(
			"timesheet_id" => $timesheet["id"]
		)));
		$flagResult = Set::combine($flagResult, "{n}.".$this->TimesheetFlag->alias.".flag", "{n}.".$this->TimesheetFlag->alias);

		if(sizeof($flagResult) > 0)
			self::$log->lwrite("Timesheet already has the following flags ". json_encode(array_keys($flagResult)));
		else
			self::$log->lwrite("Timesheet has no flags.");


		//check for early in late in
		if((int) $flags["early_in"] || (int) $flags["late_in"])
		{
			$isEarlyIn = (int) $flags["early_in"] ? true : false;
			$flag = $isEarlyIn ? EARLY_IN : LATE_IN;
			$notFlag = $isEarlyIn ? LATE_IN : EARLY_IN;

			self::$log->lwrite("Flag Timesheet: ". $flag);

			if(isset($flagResult[$notFlag]))
			{
				//delete
				self::$log->lwrite("Delete existing flag for Timesheet: ". $notFlag);
				$this->TimesheetFlag->delete($flagResult[$notFlag]["id"]);
			}

			if(isset($flagResult[$flag]))
			{
				self::$log->lwrite("Flag already exists");
			}
			else
			{
				$this->TimesheetFlag->create();
				$this->TimesheetFlag->save(
					array(
						"company_id" => $company_id,
						"timesheet_id" => $timesheet["id"],
						"flag" => $flag,
						"last_updated" => date("Y-m-d H:i:s"),
						"created" => date("Y-m-d H:i:s"),
						"created_by" => TIMESHEET_FLAGGING,
						"updated_by" => TIMESHEET_FLAGGING,
						"notes" => AUTOMATED_MESSAGE_FLAGGING
					)
				);
			}

		}
		else
		{
			if(isset($flagResult[EARLY_IN]))
			{
				self::$log->lwrite("Delete existing flag for Timesheet: ". EARLY_IN);
				$this->TimesheetFlag->delete($flagResult[EARLY_IN]["id"]);
				//delete

			}
			if(isset($flagResult[LATE_IN]))
			{
				self::$log->lwrite("Delete existing flag for Timesheet: ". LATE_IN);
				$this->TimesheetFlag->delete($flagResult[LATE_IN]["id"]);
				//delete

			}
		}

		//check for early out late out
		if((int) $flags["early_out"] || (int) $flags["late_out"])
		{
			$isEarlyOut = (int) $flags["early_out"] ? true : false;
			$flag = $isEarlyOut ? EARLY_OUT : LATE_OUT;
			$notFlag = $isEarlyOut ? LATE_OUT : EARLY_OUT;

			self::$log->lwrite("Flag Timesheet: ". $flag);

			if(isset($flagResult[$notFlag]))
			{
				//delete
				self::$log->lwrite("Delete existing flag for Timesheet: ". $notFlag);
				$this->TimesheetFlag->delete($flagResult[$notFlag]["id"]);
			}

			if(isset($flagResult[$flag]))
			{
				self::$log->lwrite("Flag already exists");
			}
			else
			{
				$this->TimesheetFlag->create();
				$this->TimesheetFlag->save(
					array(
						"company_id" => $company_id,
						"timesheet_id" => $timesheet["id"],
						"flag" => $flag,
						"last_updated" => date("Y-m-d H:i:s"),
						"created" => date("Y-m-d H:i:s"),
						"created_by" => TIMESHEET_FLAGGING,
						"updated_by" => TIMESHEET_FLAGGING,
						"notes" => AUTOMATED_MESSAGE_FLAGGING
					)
				);
			}

		}
		else
		{
			if(isset($flagResult[EARLY_OUT]))
			{
				self::$log->lwrite("Delete existing flag for Timesheet: ". EARLY_OUT);
				$this->TimesheetFlag->delete($flagResult[EARLY_OUT]["id"]);
				//delete

			}
			if(isset($flagResult[LATE_OUT]))
			{
				self::$log->lwrite("Delete existing flag for Timesheet: ". LATE_OUT);
				$this->TimesheetFlag->delete($flagResult[LATE_IN]["id"]);
				//delete

			}
		}


		self::$log->lwrite("End Timesheet Flagging");


	}

	function __checkIfAbsent($timesheet)
	{
		extract($timesheet);
		//check if absent
		$conditions = array(
			"user_id" => $user_id,
			"company_id" => $company_id,
			"start_shift" => $start_shift,
			"end_shift" => $end_shift
		);

		$id = $this->NoWork->field("id", $conditions);

		if((int) $id > 0)
		{
			self::$log->lwrite("deleting no work for: " . json_encode($conditions));
			$this->NoWork->delete($id);
			self::$log->lwrite("deleted");
		}
	}

	/**
	 * recalculates timesheet
	 * creates a new instance of timesheet where machine_id = -1 (for server) marks as recalculated timesheet
	 * Calculate only when shift has ended and user has timeout
	 * timesheet timestamps is in UTC
	 */
	function __recalculateTimesheet($timesheet)
	{
		self::$log->lwrite("Start recalculation Routine");
		if(!is_array($timesheet))
			 throw new Exception("Invalid Arguments");

		extract($timesheet);

		//calculate if has timeout

		if(AppModel::IsMinValue($timeout))
		{
			self::$log->lwrite("Time out is not set, do not recalculate");
			return;
		}

		//calculate based on timesheet's schedule
		$conditions = array(
			"schedule_id" => $timesheet['schedule_id'],
			"shift_id" => $timesheet["shift_id"],
			"user_id" => $timesheet["user_id"],
			"start_shift" => $timesheet["start_shift"],
			"end_shift" => $timesheet["end_shift"],
			"company_id" => $timesheet["company_id"],
			"machine_id" => -1 //server timesheet
 		);

		//check if server timesheet already exists;
		self::$log->lwrite("Rechecking existing server timesheet: ". json_encode($conditions));

		unset($this->Timesheet->id);
		$serverTimesheet = $this->Timesheet->find("first", array("conditions"=>$conditions));

		//already calculated
		if(isset($serverTimesheet[$this->Timesheet->alias]))
		{
			$serverTimesheet = $serverTimesheet[$this->Timesheet->alias];

			if( (int) $serverTimesheet["work_time"] && !$this->manual)
			{
				self::$log->lwrite("ServerTimesheet Has Been Calculated/Initiated");
				return;
			}
		}
		else {
			$serverTimesheet = $timesheet;
			//reset everything
			unset($serverTimesheet["id"]);
		}
			$serverTimesheet["machine_id"] = -1;
			$serverTimesheet["machine_name"] = "SERVER";
			$serverTimesheet["idle_time"] =
				$serverTimesheet["work_time"]=
				$serverTimesheet["temp_idle_time"]  =
				$serverTimesheet["temp_work_time"] = 0;



		self::$log->lwrite("Calculate routine");

		$today = new DateTime("now", new DateTimeZone("UTC"));

		$serverTimesheet["last_calculated"] = $today->format("Y-m-d H:i:s");

		if(AppModel::IsMinValue($serverTimesheet["timein"]))
		{
			$serverTimesheet["timein"] = $timesheet["timein"];
		}

		if(AppModel::IsMinValue($serverTimesheet["timeout"]))
		{
			$serverTimesheet["timeout"] = $timesheet["timeout"];
		}


		$timein = $serverTimesheet["timein"];
		$timeout = $serverTimesheet["timeout"];
		$startShift = $serverTimesheet["start_shift"];
		$endShift = $serverTimesheet["end_shift"];
		$localDate = $serverTimesheet["local_date"];


		if(AppModel::IsMinValue($localDate))
		{
			self::$log->lwrite("local date is min value");
			return false;
		}
		if(AppModel::IsMinValue($startShift))
		{
			$startShift = $localDate . " 00:00:00";
		}

		if(AppModel::IsMinValue($endShift))
		{
			$endShift = $localDate . " 23:59:59";
		}

		//timein and timeout should not be minvalue
		if(AppModel::IsMinValue($timein) || AppModel::IsMinValue($timeout))
		{
			self::$log->lwrite("timein or timeout date is min value");
			return false;
		}

		if(!isset($serverTimesheet["id"]))
		{
			self::$log->lwrite("New server timesheet");
			$this->Timesheet->create();
		}
		else
				$this->Timesheet->id = $serverTimesheet["id"];

		$this->Timesheet->save($serverTimesheet);

		self::$log->lwrite("Server timesheet, saved");

		if(!isset($serverTimesheet["id"]))
				$serverTimesheet["id"] = $this->Timesheet->getInsertID();


		$companyId = $serverTimesheet["company_id"];
		$userId = $serverTimesheet["user_id"];

		self::$log->lwrite(json_encode($serverTimesheet));

		//have to check if there are other timesheets with earlier timein or late timeout

		$otherTimesheets = $this->Timesheet->find("all", array("conditions" => array(
			"user_id" => $serverTimesheet["user_id"],
			"company_id" => $serverTimesheet["company_id"],
			"start_shift" => $serverTimesheet["start_shift"],
			"end_shift" => $serverTimesheet["end_shift"],
			"schedule_id" => $serverTimesheet["schedule_id"],
			"shift_id" => $serverTimesheet["shift_id"],
			"machine_id != -1", //do not include servertimesheet

			),
			"order" => "timein ASC",
			"fields" => array("timein", "machine_timein", "timeout", "machine_timeout")
		));
		if(sizeof($otherTimesheets))
		{
			if( isset($otherTimesheets[0][$this->Timesheet->alias]))
			{
				$first = $otherTimesheets[0][$this->Timesheet->alias];

				if(strtotime($first["timein"]) < strtotime($serverTimesheet["timein"]))
				{
					$this->Timesheet->saveField("timein", $first["timein"]);
				}
				if(strtotime($first["machine_timein"]) < strtotime($serverTimesheet["machine_timein"]))
				{
					$this->Timesheet->saveField("machine_timein", $first["machine_timein"]);
				}

				//reorder by timeout$sts = array();
				$timeouts = array();

				foreach($otherTimesheets as $key => $value)
				{
					$value = $value[$this->Timesheet->alias];
					$timeouts[] = new DateTime(  $value['timeout'] );
				}

				array_multisort($timeouts, SORT_DESC,  $otherTimesheets);

				$first = $otherTimesheets[0][$this->Timesheet->alias];

				if(strtotime($first["timeout"]) > strtotime($serverTimesheet["timeout"]))
				{
					$this->Timesheet->saveField("timeout", $first["timeout"]);
				}
				if(strtotime($first["machine_timeout"]) > strtotime($serverTimesheet["machine_timeout"]))
				{
					$this->Timesheet->saveField("machine_timeout", $first["machine_timeout"]);
				}
			}

		}

		self::$log->lwrite("Recalculation Starts");
		require_once(APP.DS."Model".DS."TimesheetCalculator.php");


		$calculation = new TimesheetCalculator();
		$calculation->timesheet = $serverTimesheet;
		$calculation->shift = array(
			"schedule_id" => $serverTimesheet["schedule_id"],
			"id" => $serverTimesheet["shift_id"],
			"start_time" => $serverTimesheet["start_shift"],
			"end_time" => $serverTimesheet["end_shift"],
			"start_date" => $serverTimesheet["local_date"]
		);
		$calculation->session = $this->session;

		//set models
		$calculation->models = array(
			"Timesheet" =>  $this->Timesheet,
			"StatusLog" =>  $this->StatusLog,
			"MethodCall" => $this->MethodCall
		);
		$calculation->Timesheet = $this->Timesheet;
		$calculation->StatusLog = $this->StatusLog;
		$calculation->MethodCall = $this->MethodCall;

		$calculation->StartTimesheetCalculation(true);
		self::$log->lwrite("End recalculation Routine");
	}

	// checks schedule vs timesheet if it has no timesheet entry for said schedules
	function __checkForAbsences($user)
	{
		self::$log->lwrite("Start Check for absences routine");
		extract($user);

		if(!isset($schedule))
		{
			self::$log->lwrite("User has no defined schedule");
			return;
		}

		extract($schedule);
		if(!isset($shifts))
		{
			self::$log->lwrite("User has no defined shifts");
			return;
		}

		foreach($shifts as $start_day => $_shifts)
		{
			foreach($_shifts as $shift)
			{
				if(isset($shift['utc_start_shift']) && isset($shift['utc_end_shift']))
				{
					$conditions =  array( "user_id" => $user_id,
						"company_id" => $this->session["company_id"],
						"schedule_id" => $shift['schedule_id'],
						"shift_id" => $shift['id'],
						"start_shift" => $shift['utc_start_shift'],
						"end_shift" => $shift['utc_end_shift']
					);
					//check if has timesheet
					$count = (int) $this->Timesheet->find("count",
						array(
							"conditions"=>$conditions
						));

					if($count)
					{
						self::$log->lwrite("Timesheet found for " . json_encode($shift));
						unset ($this->NoWork->id);
						$xid = (int) $this->NoWork->field("id", $conditions);

						if($xid)
						{
							self::$log->lwrite("User has no work data for shift. this should be deleted");
							$this->NoWork->delete($xid);
						}
					}
					else
					{
						self::$log->lwrite("Timesheet not found for " . json_encode($shift));
						self::$log->lwrite("Add new data to no work.");
						//add to absent
						unset ($this->NoWork->id);
						$count = (int) $this->NoWork->find("count",  array(
							"conditions"=>$conditions
						));

						if($count)
						{
							self::$log->lwrite("User already has no work data for shift.");
						}
						else
						{
							$this->NoWork->create();
							$this->NoWork->save(
								array(
									"user_id" => $user_id,
									"company_id" => $this->session["company_id"],
									"schedule_id" => $shift['schedule_id'],
									"shift_id" => $shift['id'],
									"start_shift" => $shift['utc_start_shift'],
									"end_shift" => $shift['utc_end_shift'],
									"notes" => AUTOMATED_MESSAGE_FLAGGING
								)
							);
							self::$log->lwrite("No work data created.");
						}
					}
				}

			}
		}


		self::$log->lwrite("End Check for absences routine");
	}

}

class TimesheetFlaggingDate
{
	var $date ;
	var $timezone;

	function __construct($date)
	{
		$this->date = $date;
	}

	function toString($format)
	{
		if(is_string($this->date)) return $this->date;

		if(is_object($this->date))
			return $this->date->format($format);
	}

	function format($format)
	{
		return $this->toString($format);
	}

	function toDateString()
	{
		return $this->toString("Y-m-d");
	}

	// timezone should be a valid timezone object or timezone string
	//returns a valid DateObject
	function setTimezone($timezone)
	{
		$date = $this->date;

		if(is_string($timezone)) $timezone = new DateTimeZone($timezone);

		if(is_string($this->date)) $date = new DateTime($this->date, $timezone);
		//if it is string, the date is as is :)
		$date = clone $date;

		$date->setTimezone($timezone);

		return $date;
	}
}
/** END OF FILE **/
