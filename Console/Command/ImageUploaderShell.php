<?php
	require_once('AppShell.php');
	require_once('FileUpload.php');

 	class ImageUploaderShell extends AppShell
 	{
		static $log;
		static $today;
		var $uses = array("Company",  "Minion", "Snapshot", "ActiveWindowInstance");

 		function startup()
		{
			$today = new DateTime();
			$today = $today->format("Y-m-d H:i:s");

			self::$today = $today;
			self::$log = new Log( 'ConvertImageStringToFile', 'Convert ImageString To File');

			self::$log->lwrite("+++++++++++++++++++++++++++++++++++++++++++++");
		}

		function main()
		{
			//$this->test();
			//return;
			$log = self::$log;
			$log->lwrite("Start Routine");
	 		$options = array("fields"=> array("company_id", "company_domain"));
			$options["conditions"] = array('company_id' => 2);
			try
			{
				$companies = $this->Company->find("all", $options);

				$log->lwrite("There are ".sizeof($companies)." companies");

				foreach($companies as $company)
				{
					$company = $company[$this->Company->alias];
					$this->session = $company;
					try
					{
						unset($this->Snapshot);
						unset($this->ActiveWindowInstance);

						$this->__importModel("Snapshot", $company);
						$this->__importModel("ActiveWindowInstance", $company);
						$this->__importModel("ApplicationRaw", $company);

						$this->__processSnapshots();

						//no longer needed May 17, 2019 @lorelie defensor
						//$this->__processAppUsage();

						//forward compatibility September 7, 2015
						$this->__processAppUsage_NewRoutine();
					}
					catch(Exception $e)
					{
						$log->lwrite("Unable to continue routine");
						$log->lwrite($e->getMessage());
					}
				}

				$log->lwrite("End Routine");
			}
			catch(Exception $e)
			{
				$log->lwrite("Failed to perform routine");
				$log->lwrite($e->getMessage());
			}

		}
 
		function __processSnapshots()
		{
			try
			{
				self::$log->lwrite("Process Snapshots");
				//storage for latest images
				$snapshots = array();
				//get the snapshot images
				$companyId = $this->session["company_id"];
				$this->Snapshot->query("UPDATE clm_snapshots_raw SET script_id = '".self::$today ."', is_processing = 1 WHERE company_id = $companyId AND is_processing = 0");
				$data = $this->Snapshot->find("all", array(
						"conditions"=>array("company_id"=>$companyId, "script_id"=>self::$today)
					)
				);

				foreach($data as $d)
				{
					$d = $d[$this->Snapshot->alias];

					$lastSnapshotOfUser = isset($snapshots[$d['user_id']]) ? $snapshots[$d['user_id']] : false;

					//process each image
					try
					{
						$imageFile = $this->__processImage($d, "snapshot");

						if(file_exists($imageFile->path.$imageFile->fileName))
						{
							$processed = $d;
							unset($processed['snapshot_id']);
							unset($processed['image_raw']);
							unset($processed['is_processing']);
							unset($processed['script_id']);

							$processed['file_name'] = $imageFile->fileName;
							$processed['file_path'] = $imageFile->_path;

							$fields = array_keys($processed);
							$values = array_map(function($x){
								return '"'.$x.'"';
							}, $processed);
							$_data = array(
								"table" => "clm_snap_shots",
								"fields" =>  implode(",", $fields),
								"values" => implode(",", $values),
								"updateFields" => $this->Snapshot->__getUniqueFieldsStatement($fields)
							);

							$sql = $this->Snapshot->renderStatement("insert_on_update", $_data);
							$this->Snapshot->query($sql);

							$this->Snapshot->id = $d['snapshot_id'];
							$this->Snapshot->delete();


							if(
								(
									$lastSnapshotOfUser &&
									(strtotime($processed['local_time']) > strtotime($lastSnapshotOfUser['local_time']))
								)
								||
								$lastSnapshotOfUser === false
							)
							{
								$snapshots[$d['user_id']]	= $processed;
							}
						}
						else
						{
							//failed
							$this->Snapshot->query("UPDATE clm_snapshots_raw SET is_processing = 0, script_id = null WHERE snapshot_id = ".$d['snapshot_id']);
						}
					}
					catch(Exception $e)
					{
						debug($e->getMessage());
					}
				}
				foreach($snapshots as $snapshot)
				{
					$lastActivity = array(
						'user_id' => $snapshot['user_id'],
						'company_id' => $snapshot['company_id'],
						'last_snapshot_filename' => $snapshot['file_name'],
						'last_snapshot_filepath'=> $snapshot['file_path'],
						'last_snapshot_local_time' => $snapshot['local_time']
					);
					$fields = array_keys($lastActivity);
					$values = array_map(function($x){
						return '"'.$x.'"';
					}, $lastActivity);
					$_data = array(
						"table" => "clm_last_activity",
						"fields" =>  implode(",", $fields),
						"values" => implode(",", $values),
						"updateFields" => $this->Snapshot->__getUniqueFieldsStatement($fields)
					);

					$sql = $this->Snapshot->renderStatement("insert_on_update", $_data);
					$this->Snapshot->query($sql);
				}
			}
			catch(Exception $e)
			{

			}



		}

		function __processAppUsage()
		{
			try
			{
				self::$log->lwrite("Process Applications");
				$apps =array();
				//get the snapshot images
				$companyId = $this->session["company_id"];
				$this->ActiveWindowInstance->query("UPDATE clm_awi_raw SET script_id = '".self::$today ."', is_processing = 1 WHERE company_id = $companyId AND is_processing = 0");
				$data = $this->ActiveWindowInstance->find("all", array(
						"conditions"=>array("company_id"=>$companyId, "script_id"=>self::$today)
					)
				);

				foreach($data as $d)
				{
					$d = $d[$this->ActiveWindowInstance->alias];

					$lastAppUsage = isset($apps[$d['user_id']]) ? $apps[$d['user_id']] : false;


					//process each image
					try
					{
						//original
						$imageFile = $this->__processImage($d, "original");

						//thumbnail
						//second path
						$thumbnail = clone $imageFile;
						$thumbnail->path = str_replace("original", "thumbnail",  $imageFile->path);
						$this->__makeSurePathExists($thumbnail->path);

						$thumbnail->__generateThumbnail($imageFile->path . $thumbnail->fileName);

						if(file_exists($imageFile->path.$imageFile->fileName))
						{
							$processed = $d;
							unset($processed['id']);
							unset($processed['image_raw']);
							unset($processed['is_processing']);
							unset($processed['script_id']);

							$processed['file_name'] = $imageFile->fileName;
							$processed['file_path'] = $imageFile->_path;

							$fields = array_keys($processed);
							$values = array_map(function($x){
								return '"'.$x.'"';
							}, $processed);
							$_data = array(
								"table" => "clm_activewindow_instance",
								"fields" =>  implode(",", $fields),
								"values" => implode(",", $values),
								"updateFields" => $this->ActiveWindowInstance->__getUniqueFieldsStatement($fields)
							);

							$sql = $this->ActiveWindowInstance->renderStatement("insert_on_update", $_data);
							$this->ActiveWindowInstance->query($sql);

							$this->ActiveWindowInstance->id = $d['id'];
							$this->ActiveWindowInstance->delete();

							if(
								(
									$lastAppUsage &&
									(strtotime($processed['local_time']) > strtotime($lastAppUsage['local_time']))
								)
								||
								$lastAppUsage === false
							)
							{
								$apps[$d['user_id']]	= $processed;
							}
						}
						else
						{
							//failed
							$this->ActiveWindowInstance->query("UPDATE clm_awi_raw SET is_processing = 0, script_id = null WHERE id = ".$d['id']);
						}
					}
					catch(Exception $e)
					{
						debug($e->getMessage());
					}
				}

				foreach($apps as $app)
				{
					$lastActivity = array(
						'user_id' => $app['user_id'],
						'company_id' => $app['company_id'],
						'last_screenshot_filename' => $app['file_name'],
						'last_screenshot_filepath'=> $app['file_path'],
						'last_application_local_time' => $app['local_time'],
						'last_application' => base64_decode($app['app_id']),
						'last_keystrokes' => $app['keystrokes_formatted_data'],
						'last_application_machine_id' => $app['machine_id']
					);
					$fields = array_keys($lastActivity);
					$values = array_map(function($x){
						return '"'.$x.'"';
					}, $lastActivity);
					$_data = array(
						"table" => "clm_last_activity",
						"fields" =>  implode(",", $fields),
						"values" => implode(",", $values),
						"updateFields" => $this->ActiveWindowInstance->__getUniqueFieldsStatement($fields)
					);

					$sql = $this->ActiveWindowInstance->renderStatement("insert_on_update", $_data);
					$this->ActiveWindowInstance->query($sql);
				}
			}
			catch(Exception $e)
			{

			}
		}

		function __processImage($imageData, $type, $timeIndex = "local_time")
		{
			$keys = array(
				"user_id" => $imageData['user_id'],
				"company_id" => $imageData['company_id'],
				"machine_id" => $imageData['machine_id'],
				$timeIndex => str_replace(" ", "", str_replace(":", "", $imageData[$timeIndex]))
			);

			$imageKey = implode("_", $keys);
			$imagePath = substr($imageData[$timeIndex], 0, 10);

			$imageFile = new FileUpload($imageData["image_raw"]);

			//PATH
			$imagePath = "CLOUDICA_IMAGES_" . $imagePath;
			$imagePath = WWW_ROOT."upload". DS. $imagePath;
			$this->__makeSurePathExists($imagePath);

			$imageFile->_path = str_replace(WWW_ROOT."upload".DS, "",   $imagePath);

			$imagePath .= DS. $type  ;
			$this->__makeSurePathExists($imagePath);
			$imageFile->path = $imagePath . DS;
			// END PATH

			$imageFile->fileName = $imageKey . ".jpeg";
			$imageFile->fileType = "image/jpeg";
			$imageFile->resize = false;

			//UPLOAD
			if(!file_exists($imageFile->path.$imageFile->fileName))
				$imageFile->upload();
			//END UPLOAD

			return $imageFile;
		}

		function __makeSurePathExists($path)
		{
			if(!file_exists($path)) @mkdir ($path);

			if(!file_exists($path . DS . "index.html")) fopen ($path . DS . "index.html", "w");
		}

		function __processAppUsage_NewRoutine()
		{
			try
			{
				self::$log->lwrite("Process Applications");
				$apps =array();

				$companyId = $this->session["company_id"];

 
				$query = "UPDATE clm_application_raw SET script_id = '".self::$today ."', is_processing = 1 WHERE company_id = $companyId AND is_processing = 0;";

				self::$log->lwrite($query);

				$this->ApplicationRaw->query($query);

				$data = $this->ApplicationRaw->find("all", array(
						"conditions"=>array("company_id"=>$companyId, "script_id"=>self::$today)
					)
				);

				foreach($data as $d)
				{
					$d = $d[$this->ApplicationRaw->alias];

					$lastAppUsage = isset($apps[$d['user_id']]) ? $apps[$d['user_id']] : false;

					//process each image
					try
					{

						$imageRaw = $d["image_raw"];

						if($imageRaw)
						{
							//original
							$imageFile = $this->__processImage($d, "original", "start_time");

							//thumbnail
							//second path
							$thumbnail = clone $imageFile;
							$thumbnail->path = str_replace("original", "thumbnail",  $imageFile->path);
							$this->__makeSurePathExists($thumbnail->path);

							$thumbnail->__generateThumbnail($imageFile->path . $thumbnail->fileName);
						}


						if( !$imageRaw
							||  file_exists($imageFile->path.$imageFile->fileName) )
						{

							$processed = $d;
							unset($processed['id']);
							unset($processed['image_raw']);
							unset($processed['is_processing']);
							unset($processed['script_id']);

							$processed['file_name'] = $imageRaw ?  $imageFile->fileName : "";
							$processed['file_path'] = $imageRaw ?  $imageFile->_path : "";

							//decode
							$processed["title"] = base64_decode($processed["title"]);
							$processed["name"] = base64_decode($processed["name"]);
							$processed["keystroke_formatted"] = base64_decode($processed["keystroke_formatted"]);
							$processed["keystroke_raw"] = base64_decode($processed["keystroke_raw"]);

							$fields = array_keys($processed);
							$values = array_map(function($x){
								return '"'.$x.'"';
							}, $processed);
							$_data = array(
								"table" => "clm_applications",
								"fields" =>  implode(",", $fields),
								"values" => implode(",", $values),
								"updateFields" => $this->ActiveWindowInstance->__getUniqueFieldsStatement($fields)
							);

							$sql = $this->ApplicationRaw->renderStatement("insert_on_update", $_data);
							self::$log->lwrite($sql);
							$this->ApplicationRaw->query($sql);

							$this->ApplicationRaw->id = $d['id'];
							$this->ApplicationRaw->delete();

							if(
								(
									$lastAppUsage &&
									(strtotime($processed['start_time']) > strtotime($lastAppUsage['start_time']))
								)
								||
								$lastAppUsage === false
							)
							{
								$apps[$d['user_id']]	= $processed;
							}
						}
						else
						{
							//failed
							$this->ApplicationRaw->query("UPDATE clm_application_raw SET is_processing = 0, script_id = null WHERE id = ".$d['id']);
						}
					}
					catch(Exception $e)
					{
						self::$log->lwrite($e->getMessage());
					}
				}

				foreach($apps as $app)
				{
					$lastActivity = array(
						'user_id' => $app['user_id'],
						'company_id' => $app['company_id'],
						'last_screenshot_filename' => $app['file_name'],
						'last_screenshot_filepath'=> $app['file_path'],
						'last_application_local_time' => $app['start_time'],
						'last_application' =>  ($app['title']),
						'last_keystrokes' => $app['keystroke_formatted'],
						'last_application_machine_id' => $app['machine_id']
					);
					$fields = array_keys($lastActivity);
					$values = array_map(function($x){
						return '"'.$x.'"';
					}, $lastActivity);
					$_data = array(
						"table" => "clm_last_activity",
						"fields" =>  implode(",", $fields),
						"values" => implode(",", $values),
						"updateFields" => $this->ApplicationRaw->__getUniqueFieldsStatement($fields)
					);

					$sql = $this->ApplicationRaw->renderStatement("insert_on_update", $_data);
					$this->ApplicationRaw->query($sql);
				}
			}
			catch(Exception $e)
			{
				self::$log->lwrite($e->getMessage());
			}
		}

	}

/** END OF FILE **/
