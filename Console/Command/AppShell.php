<?php
/**
 * AppShell file
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Shell', 'Console');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
require_once('Task'.DS.'LogClass.php');
class AppShell extends Shell {

	static $log;

	var $uses = array("Minion");
	function __importModel($modelClass, $company = null)
	{
		if(!$modelClass) return;

		if($company == null) throw new Exception ("Company is null");

		$minion = $this->__getMinionDB($company);
		if($minion == null)
		{
			throw new Exception ("There is no company db");
		}

		App::import('Model', $modelClass);
		$class = new $modelClass( false, null, $minion->name);
		$class->Now = isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");
		$this->$modelClass = $class;

		return $class;
	}

	function __getMinionDB($company)
	{
		return $this->Minion->getDatabase($company);
	}

}
