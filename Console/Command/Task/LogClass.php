<?php

class Log{

	var $path;
	var $script_name;
	private $fp = null;
	var $location;

	function __construct($path, $script_name)
	{
		$this->location =  ROOT.DS.APP_DIR.DS."tmp".DS."logs".DS;
		$this->path = $this->location.$path;
		$this->script_name =  $script_name;
	}
	public function lwrite($message){
		if (!$this->fp) $this->lopen();
		$script_name =$this->script_name;
		$time = date('H:i:s');
		fwrite($this->fp, "$time ($script_name) $message\n");
		echo ">>".$message."\n\n";
	}
	private function lopen(){
		$today = date('Y-m-d');
		$lfile = $this->path;
		$this->fp = fopen($lfile . '_' . $today. '.log', 'a') or exit("Can't open $lfile!");
	}
}

?>