<?php

class FileUpload
{
	const DELETE_FILE = "DEL";

	var $base64EncodedString;
	var $fileType;
	var $path;
	var $maxSize;
	var $output = "";
	var $errorCode = "";
	var $error = "";
	var $fileName = "";
	var $size = 200;
	var $resize = true;
	var $createFromString = false;

	var $file;

	function __construct($base64EncodedString = "", $createFromString = false)
	{
		$base64EncodedString = str_replace(' ', '+', $base64EncodedString);
		if(!empty($base64EncodedString))
		{
			$this->base64EncodedString = $base64EncodedString;
		}

		$this->createFromString = $createFromString;
	}

	function upload()
	{
		if(!$this->base64EncodedString)
		{
			$this->errorCode = 1001;
			return false;
		}

		if($this->base64EncodedString == self::DELETE_FILE)
		{
			$this->output = "";
			$this->fileName = "";
			return true;
		}

		$this->base64EncodedString = str_replace(' ', '+', $this->base64EncodedString);


		$fileType = $this->fileType ? $this->fileType : $this->getFileType()  ;


		if(!$this->base64EncodedString && $fileType)
		{
			return false;
		}

		if(!$fileType)
		{
			return false;
		}

		$this->output = $this->processFile();

		return true;
	}


	function getFileType()
	{
		if(!$this->base64EncodedString || $this->base64EncodedString == self::DELETE_FILE) return false;

		preg_match('/^data:(image\/jpeg|image\/png|application\/pdf);base64,/', $this->base64EncodedString, $type);

		if(sizeof($type) == 2)
		{
			$type = $type[1];
			$this->fileType = $type;
			return $type;
		}
		else {

			preg_match('/\.(jpeg|jpg|pdf|png)$/', $this->base64EncodedString, $type);
			if(isset($type[0]))
			{
				$type = $type[0];
				$type = str_replace(".", "", $type);
				$this->fileName = $this->base64EncodedString;
				$this->base64EncodedString = "";
				switch($type)
				{
					case "jpeg": case "jpg":

					$type = "image/jpeg";
						break;
					case "png":
					$type = "image/png";
						break;
					case "pdf":
					$ype = "application/pdf";
						break;
					default:
						$this->errorCode = 1001;
						$this->error = "Invalid Format";
						break;
				}

				if(!$this->errorCode)
				{
					$this->fileType = $type;
					return $type;
				}
			}
		}


		$this->errorCode = 1001;
		$this->error = "Invalid File Type";


		return false;
	}

	function getFileExtension()
	{
		$fileExtension = false;
		switch($this->fileType)
		{
			case "image/jpeg":
			case "image/png":
			case "application/pdf":
				$a = explode("/", $this->fileType);
				$fileExtension = $a[1];
				break;
			default:
				$this->errorCode = 1001;
				$this->error = "Invalid Format";
				break;
		}

		return $fileExtension;
	}

	function processFile()
	{
		$directory = $this->path;
		$fileName = $this->fileName;

		if(!$this->base64EncodedString) return;

		try
		{
			$extension = $this->getFileExtension();
			$this->__createJPEG();
		}
		catch (Exception $e)
		{
			$this->errorCode = 1001;
			$this->error = $e->getMessage();
			return false;
		}
	}

	function __createJPEG()
	{
		 $imgData = base64_decode($this->base64EncodedString);

		$image = imagecreatefromstring($imgData);

		if($image)
		{
			imagejpeg($image, $this->path . $this->fileName);
		}
		else return false;
	}



	function __generateThumbnail($source)
	{
		if(!file_exists($source)) return false;
		
		$size = @getimagesize($source);
		$image = imagecreatefromjpeg($source);

		if($size && $image)
		{

			$width = $size[0];
			$height = $size[1];

			if ($width > $height)
			{
				$rate = $width / SYS_THUMBNAIL_WIDTH;
				$newWidth = SYS_THUMBNAIL_WIDTH;
				$newHeight = (int)($height / $rate);
			}
			else
			{
				$rate = $height / SYS_THUMBNAIL_HEIGHT;
				$newHeight = SYS_THUMBNAIL_HEIGHT;
				$newWidth = (int)($width / $rate);
			}

			try
			{
				$resizedImg = imagecreatetruecolor($newWidth, $newHeight);
				imagecopyresized($resizedImg, $image, 0, 5, 0, 0, $newWidth, $newHeight, $width, $height);
				imagejpeg ($resizedImg, $this->path . $this->fileName);
				imagedestroy ($resizedImg);
				imagedestroy ($image);

			}
			catch (Exception $e)
			{
				return false;
			}
		}


		else return false;
	}



}
/** END OF FILE **/
