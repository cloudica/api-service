<?php
require_once("ClientDataModel.php");
class Schedule extends ClientDataModel
{
	var $name = "Schedule";

	var $useTable = "schedules";

	var $fields = array(
		"id",
		"schedule_name",
		"schedule_type",
		"timezone"
	);

	function returnResponse($userId, $companyId)
	{
		if(!isset($this->account)
		|| !isset($this->account['Account'])
		|| !isset($this->account['Account']['schedule_id'])
		|| !isset($this->account['Account']['schedule_type'])
		|| !isset($this->account['Account']['employee_id']))
			return "";

		$employeeId = (int) $this->account['Account']['employee_id'];

		if(!$employeeId) return "";

		return $this->getScheduleFromEmployeeId($employeeId);
	}

	function getScheduleFromEmployeeId($employeeId,$toCSV = true){

		if(is_array($employeeId)) $employeeId = $employeeId[0];

		$sid = $this->query("SELECT schedule_id FROM clm_employee_profile as A WHERE id = " . (int) $employeeId);

		$sid = isset($sid[0]) && isset($sid[0]["A"]) && isset($sid[0]["A"]["schedule_id"]) ? $sid[0]["A"]["schedule_id"] : 0;

		$data = $this->find('all', array(
			'conditions' => array('id' => $sid),
			'fields' => $this->fields
		));

		if(!$toCSV) return $data;

		$csv = $this->ToCSV($data,$this->fields);
		   // CakeLog::write('shift', "csv" . $csv );
	 	return $csv;
	}

}
