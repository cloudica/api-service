<?php
/**
 * @author Romeo Loyola
 */
require_once("TimesheetReport.php");
class TaskNotes extends TimesheetReport
{
	var $name ="TaskNotes";
	var $useTable = "task_notes";

	var $fields = array
	(
		"is_compromised",
		"is_calibrated",
		"id",
		"parent_id",
		"company_id",
		"is_deleted",
		"task_id",
		"notes",
		"local_time",
		"machine_time",
	);

	var $validate = array();

}