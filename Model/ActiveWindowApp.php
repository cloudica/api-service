<?php

require_once("ClientDataModel.php");
class ActiveWindowApp extends ClientDataModel
{
	var $name ="ActiveWindowApp";
	var $useTable = "activewindow_1";
	var $primaryKey = "id";

	var $fields = array(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"app_id",
		"win_id",
		"app_title",
		"app_name",
		"instance",
		"time_usage"
	);

	var $uniqueKeys = array(
		"machine_id",
		"company_id",
		"local_time",
		"app_id",
		"user_id"
	);
}
/** END OF FILE **/