<?php
class SynchData
{
	public $lastSynched = ""; // timestamp where data has last synchedToServer
	public $type = ""; // dataModel to operate on
	public $csvString = ""; // the data to return
	public $success = false; // tag if successfully synched or not
	public $localId;
	public $ReferenceId;
	public $requestId;
	public $localRequestId;

	//dtr changes
	public $hasImportedAllData;

	protected $models = array(
			"changes" => array ("model" => "Changes"),
			"application" => array ("model"=>"ApplicationRaw"),

			"activewindowinstance" => array ("model"=>"ActiveWindowInstance"),
			"activewindowapp" => array ("model"=>"ActiveWindowApp"),

			"timesheet" => array ("model"=>"Timesheet"),

			"process" => array ("model"=>"ProcessData"),
			"processapp" => array ("model"=>"ProcessApp"),
			"processinstance" => array ("model"=>"ProcessInstance"),
			"methodcall" => array ("model"=>"MethodCall"),
			"snapshot" => array ("model"=>"Snapshot"),
			"statusmessage" => array ("model"=>"StatusMessage"),
			"statuslog" => array ("model"=>"StatusLog"),
			"timesheetreport" => array ("model"=>"TimesheetReport"),
			"tasknotes" => array ("model" => "TaskNotes"),
			"workbreak" => array ("model"=>"WorkBreak"),
			"worktask" => array ("model"=>"WorkTask"),
			"milestone" => array ("model"=>"Milestone"),

		);

	public static $ServerToClientModels = array(
		"userconfig" => array("model"=>"ClientModuleConfig"),
		"shiftschedule" => array("model"=>"ShiftSchedule"),
		"manager" => array("model"=>"Managers"),
		"schedule" => array("model"=>"Schedule")
	);


	function __construct(){$success = false;}

	public function __get($property)
	{
		if($property == 'model')
		{
			$model = "";
			if(isset($this->models[$this->type]) && isset($this->models[$this->type]["model"]))
			{
				$model = $this->models[$this->type]["model"];
			}
			$this->model = $model;
			return $this->model;
		}
	}
	static function Deserialize(SimpleXMLElement $xml, $skip_empty_lines = true, $trim_fields  = true)
	{
		$synchData = new SynchData();

		$synchData->type = (string) $xml["type"];
		$synchData->success = (string) $xml["success"];
		$synchData->lastSynched =(string)  $xml["lastSynch"];
		$synchData->_csvString = $synchData->csvString = trim((string) $xml);

		$synchData->_rows = $synchData->rows = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', trim($synchData->csvString) );

		return $synchData;
	}


	static function Serialize(SynchData $synchData)
	{
		$xmlElement = new SimpleXmlElement("<DATA><![CDATA[".$synchData->csvString."]]></DATA>", null, false);

		$xmlElement->addAttribute("type", $synchData->type);
		$xmlElement->addAttribute("lastSynch", $synchData->lastSynched);
		$xmlElement->addAttribute("success", (int) $synchData->success);
		return $xmlElement;
	}

}
/** END OF FILE **/