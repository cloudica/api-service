<?php
/**
 * pseudo model
 * @author lorelie dazo
 *
 */
class Version extends AppModel
{

	var $name = "Version";
	var $useTable = "updates";

	var $version = "";
	var $build = "";
	var $mainDBBuild = "";
	var $localDBBuild = "";
	var $configDBBuild = "";
	var $content = "";

	var $versionNumber;
	var $versionSerial;
	protected $releases = array("Alpha", "Beta", "Release");

	function __construct($str)
	{
		$content = explode("!", $str);
		if(sizeof($content) == 5)
		{
			$this->content = $str;

			$this->version = $content[0];
			$this->build = $content[1];
			$this->mainDBBuild = $content[2];
			$this->localDBBuild = $content[3];

			$v = explode(".", $this->version);

			if(isset($v[0]))
			{
				$this->versionNumber = (int) $v[0];
				if(isset($v[1]))
				{
					$this->versionNumber += ((int) $v[1] )*0.1;
				}
			}


			$d = str_replace($this->versionNumber.".", "", $this->version);
			$this->versionSerial = floatval($d);
		}
	}


	function getLatestFromBuildNumber($oldBuild)
	{
		$oldBuild = floatval($oldBuild);
		$query = "SELECT filename, MAX(version) as version, MAX(version_serial) as version_serial, MAX(build) as build FROM clm_updates  as `0` where  version = ". $this->versionNumber." AND (build <= ".$this->build."  AND build > ".$oldBuild.") GROUP BY filename ORDER BY filename ASC, version ASC, version_serial ASC, build DESC" ;
		$data = $this->query($query);

		return $data;
	}

	function getLatestFromVersionSerial($oldSerial)
	{
		$oldSerial = floatval($oldSerial);
		$query = "SELECT filename, MAX(version) as version, MAX(version_serial) as version_serial, MAX(build) as build FROM clm_updates  as `0` where version = ".$this->versionNumber." AND (version_serial <= ".$this->version_serial." AND version_serial > ".$oldSerial.") GROUP BY filename ORDER BY filename ASC, version ASC, version_serial ASC, build DESC";

		$data = $this->query($query);

		return $data;
	}


	function getLatestFromVersionNumber($oldVersion)
	{
		$oldVersion = floatval($oldVersion);
		$query = "SELECT filename, MAX(version) as version, MAX(version_serial) as version_serial, MAX(build) as build FROM clm_updates  as `0` where version <= ".$this->versionNumber." AND version > ".$oldVersion." GROUP BY filename ORDER BY filename ASC, version ASC, version_serial ASC, build DESC";

		$data = $this->query($query);

		return $data;
	}

}
/** END OF FILE **/