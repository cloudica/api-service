<?php
class Minion  extends AppModel
{
	var $name = "Minion";
	var $useTable = false;

	function getDatabase($session)
	{
		if(!isset($session["company_id"]) || !isset($session["company_domain"]))
		{
			throw new Exception ("Unable to determine company DB");
		}

		$dbName = $session["company_domain"] . "_" . $session["company_id"];

		$minionSource = ConnectionManager::getDataSource("minion");
		$config = $minionSource->config;

		if(!$config)
		{
			return null;
		}

		$minion = $config;
		$minion["database"] = $dbName;

		$dbSource = ConnectionManager::create($dbName, $minion);
		// if($dbSource)
		// {
		//	$dbSource->name = $dbName;
		// }

		return $dbSource;
	}
}
/** END OF FILE **/