<?php
class Bug extends AppModel
{
	var $name ="Bug";
	var $useTable = "bugs";

	function getBugId($report)
	{
		$bugs = $this->getBugByMessageAndSource($report->Message, $report->Source);

		if($bugs == null)
		{
			//create new bug

			$bug = array($this->alias=>array(
				"source" =>$report->Source,
				"message"=>$report->Message,
				"stacktrace"=>$report->StackTrace,
				"client_crashed"=>$report->Crashed,
				"bug_status"=>"not fixed",
				"build_fixed"=>""
			));

			return $this->save($bug);
		}
		else if (sizeof($bugs) ==1)
		{
			return $bugs[0][$this->alias]["id"];
		}
		else
		{
			return "too many to mention";
		}
	}

	function save($data = null, $validate = true, $fieldList = array())
	{
		$this->create();
		parent::save($data, $validate, $fieldList);

		$bugId = $this->getInsertId();

		$data[$this->alias]["id"];

		return $bugId;
	}

	function getBugByMessageAndSource($message, $source)
	{
		$data = $this->find('all', array(
			'conditions' => array('message' => $message, 'source' => $source )
		));

		if(sizeof($data) == 0)
		{
			return null;
		}
		else
		{
			return $data;
		}

	}

}