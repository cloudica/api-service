<?php
/**
 * @copyright     Copyright 2015
 * @link          http://cloudica.com
 * @author		  Lorelie Dazo
 * @version 	  Cloudica1.1
 * @package
 * @since         November 04, 2015
 * @license
 *
 */


require_once("ClientToServerToClientModel.php");

class MachineBasedClientData extends ClientToServerToClientModel
{
	var $timestampKey = "local_time";
	/**
	 * for machine based data, return only the data from same machine_id
	 */

	function GetNewerDataWhereClause()
	{
		 if(!isset($this->synchData)) return array();

		 $conditions = parent::GetNewerDataWhereClause();

		 $machineField = 'machine_id';
		 $lastUpdatedField = $this->timestampKey." >";

		if(sizeof($this->belongsTo))
		{
			$lastUpdatedField = $this->alias.".".$lastUpdatedField;
			 $machineField = $this->alias.".".$machineField;
		}

		//add machine ID Clause
		$conditions[$machineField] = $this->session['machine_id'];

		 //if lastSynched is minvalue return current shift
		$lastSynched = $this->synchData->lastSynched;
		if(!strtotime($lastSynched))
		{
			 if(isset($this->currentTimesheet) && $this->currentTimesheet
			 	&& isset($this->currentTimesheet['start_shift']))
			 {
					$startShift = $this->currentTimesheet['start_shift'];
					$timein = $this->currentTimesheet['timein'];

					if(strtotime($timein) < strtotime($startShift)) $startShift = $timein;

					$conditions[$lastUpdatedField] = $startShift;
			 }
		}

		return $conditions;
	}
}

/** END OF FILE **/