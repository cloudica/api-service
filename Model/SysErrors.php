<?php
class SysErrors extends AppModel
{
	var $name = "SysErrors";
	var $useTable = "sys_errors";
	var $primaryKey ="error_code";

	/**
	 * returns a string containing the error given by code
	 * @param $code
	 * @return array(message, type)
	 */
	function getErrorByCode($code)
	{
		$data = $this->findByErrorCode($code);
		if(!isset($data["SysErrors"]))
		{
			$data = null;
		}
		else
		{
			$data = $data["SysErrors"];
		}

		return $data;
	}

}