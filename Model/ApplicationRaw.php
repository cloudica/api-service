<?php
require_once("ClientDataModel.php");
class ApplicationRaw extends ClientDataModel
{
	var $name ="ApplicationRaw";
	var $useTable = "application_raw";

	var $saveLatestData = false;

	var $fields = array
	(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"win_id",
		"title",
		"name",
		"start_time",
		"end_time",
		"machine_starttime",
		"machine_endtime",
		"keystroke_formatted",
		"keystroke_raw",
		"image_raw",
		"image_string_file_size"
	);

	var $uniqueKeys = array(
		"machine_id",
		"company_id",
		"start_time",
		"user_id",
		"win_id"
	);
}
/** END OF FILE **/