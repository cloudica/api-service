<?php
/**
 * @author Romeo Loyola
 */
require_once("ClientToServerToClientModel.php");
class WorkTask extends ClientToServerToClientModel
{
	var $name ="WorkTask";
	var $useTable = "work_task";

	var $machineData = false;


	var $order = "WorkTask.last_updated ";
	var $belongsTo =  array(
		"Milestone"=> array(
			"class"=>"Milestone",
			"foreignKey" => "milestone_id"
		)
	);

	var $virtualFields = array(
		"is_synch" => "CONCAT('Synched')", // synch status automate
		"session_id" =>  "CONCAT('0')", //no session id
		"log_status_0" => "CONCAT('0')", //force status to be 0 again
		"milestone" => "Milestone.milestone",
		"assigned_by_user" => "CONCAT('')"
	);




	var $fields = array
	(
		"is_compromised",
		"is_calibrated",
		"id",
		"parent_id",
		"company_id",
		"is_deleted",
		"title",
		"description",
		"start_date",
		"due_date",
		"assigned_by",
		"status", //should be OnGoing, NotYetStarted Or Completed
		"milestone_id"
	);


	var $returnFields = array
	(
		"is_synch",
		"session_id",
		"is_compromised",
		"is_calibrated",
		"id",
		"parent_id",
		"company_id",
		"is_deleted",
		"title",
		"description",
		"start_date",
		"due_date",
		"assigned_by",
		"status", //should be OnGoing, NotYetStarted Or Completed
		"milestone_id",
		"milestone",
		"assigned_by_user"
	);

	var $validate = array(
		"title" => array(
			"rule1" => array(
				"rule" =>  "notEmpty",
				"required"=>true,
				"allowEmpty"=>false,
				"message"=>"Title is required"
			),
			"rule2" => array(
				"rule" =>  "areUnique",
				"required"=>true,
				"allowEmpty"=>false,
				"message"=>"A task with the same title already exists",
				"otherFields" => array("user_id", "company_id", "parent_id", "description", "start_date", "due_date", "assigned_by", "status", "milestone_id")
			)
		)
	);

	var $mergeData = true;
	var $findVariables = array(
		"conditions"=>array("parent_id" => "0")
	);

	function __getValidationErrors($data)
	{
		$errors = array();
		if(isset($this->validationErrors["title"]) && isset($this->validationErrors["title"][0]) && isset($data["title"]))
		{

			$validationError = $this->validationErrors["title"][0];
			$title = $data["title"];
			$errors[] = '"'.$title.'","'.$validationError.'"';
		}
		return $errors;
	}

	function afterFind($data = array())
	{
		if($data)
		{
			//get all assignedByID
			$assignedById = Set::classicExtract($data, "{n}.WorkTask.assigned_by");

			App::import("Model", "User");
			$User = new User();
			$users = $User->find("all", array("conditions"=>array("user_id"=>$assignedById), "fields"=>array("user_id", "firstname", "lastname")));

 			$users = Set::combine($users, "{n}.User.user_id", "{n}.User");

			foreach($data as $key=> &$value)
			{
				if(isset($value["WorkTask"]) && isset($value["WorkTask"]["assigned_by"]) &&  isset($users[$value["WorkTask"]["assigned_by"]]))
				{
					$user = $users[$value["WorkTask"]["assigned_by"]];
					$value["WorkTask"]["assigned_by_user"] = $user["firstname"] . " " . $user["lastname"];
				} 			}

		}

		return $data;
	}


}