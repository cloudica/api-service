<?php
require_once("ClientDataModel.php");
class DebugRequests extends ClientDataModel
{
	var $name = "DebugRequests";
	var $useTable = "debugging_requests";

	var $fields = array(
		"id",
		"local_date",
		"request_type",
		"company_id",
		"user_id",
		"requested_by",
		"data_type",
		"request_status",
	);
}