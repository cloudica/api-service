<?php
require_once("ClientToServerToClientModel.php");
class Timesheet extends ClientToServerToClientModel
{
	var $name ="Timesheet";
	var $useTable = "timesheet_1";

	var $fields = array
	(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"id",
		"schedule_id",
		"shift_id",
		"timein",
		"timeout",
		"temp_timeout",
		"machine_timein",
		"machine_timeout",
		"tempmachine_timeout",
		"timein_notes",
		"timeout_notes",
		"idle_time",
		"work_time",
		"local_date",
		"last_calculated",
		"merged",
		"temp_work_time",
		"temp_idle_time",
		"start_shift",
		"end_shift",
		"schedule_changed",
		"machine_name"
	);



	var $virtualFields = array(
		"local_id" =>  "CONCAT('0')", //return this, because timesheet is deserialized and not imported as csv
		"is_synch" => "CONCAT('Synched')", // synch status automate
		"session_id" =>  "CONCAT('0')", //no session id
		"log_status_0" => "CONCAT('0')" //force status to be 0 again
	);

	//automate the return fields expected by client
	var $returnFields = array(
		"local_id",
		"is_synch",
		"session_id",
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"id",
		"schedule_id",
		"shift_id",
		"timein",
		"timeout",
		"temp_timeout",
		"machine_timein",
		"machine_timeout",
		"tempmachine_timeout",
		"timein_notes",
		"timeout_notes",
		"idle_time",
		"work_time",
		"local_date",
		"last_calculated",
		"merged",
		"temp_work_time",
		"temp_idle_time",
		"start_shift",
		"end_shift",
		"work_time", //uploaded work time
		"idle_time", //uploaded idle time
		"session_id", // tiemin session id
		"session_id", //timeout session id
		"schedule_changed",
		"machine_name"
	);


	var $uniqueKeys = array(
		"company_id",
		"user_id",
		"start_shift",
		"end_shift",
		"machine_id"
	);

	var $saveLatestData = true;
	var $latestDataFields = array(
		"shift_id" => "shift_id",
		"start_shift" => "start_shift",
		"schedule_id" => "schedule_id",
		"end_shift" => "end_shift",
		"work_hours" => "work_time",
		"work_hours_local_time" => "temp_timeout",
		"last_timein" => "timein",
		"work_hours_machine_id" =>  array("field"=>"machine_id", "transform"=>"getMachineID"),
	);


	function ProcessSynchData($returnId = false)
	{
		$this->synchData->_rows = $this->synchData->rows;

		$success=   $this->SaveSynchData($returnId);

		//get the latest one
		if($success)
		{
			$newerData = $this->GetNewerData();
			if(sizeof($newerData) > 0)
			{
				$returnCSV = $this->ToCSV($newerData, $this->GetReturnFields());
				$this->synchData->csvString = $returnCSV;
			}
		}

		return $success;
	}

	function __automateFields(){
		if(!isset($this->synchData) && $this->session)
		{
			return;
		}
		$current = array(
			"company_id" =>  (int) $this->session['company_id'],
			"start_shift" => (string) $this->requestData->STARTSHIFT,
			"end_shift" => (string) $this->requestData->ENDSHIFT,
			"machine_id" => (int) $this->session['machine_id']
		);

		$fromRequest = false;
		$fromRequestIndex = -1;

		if(isset($this->synchData->rows))
		{
			foreach($this->synchData->rows as $index => &$row)
			{
				$same = false;
				$__row = str_getcsv($row);
				if(sizeof($this->fields) != sizeof($__row))
				{
					//momentarily remove "machine_name from this fields"
					$fields = $this->fields;
					$key_machine_name = array_search("machine_name", $fields);
					if($key_machine_name >= 0)
					{
						unset($fields[$key_machine_name]);
						$this->fields = $fields;
					}
				}


				if(sizeof($this->fields) == sizeof($__row))
				{
					$__row = array_combine($this->fields, $__row);

					if((int)  $__row['company_id'] == (int)  $current['company_id']
						&& $__row['start_shift'] == $current['start_shift']
						&& $__row['end_shift'] == $current['end_shift']
						&& (int)  $__row['machine_id'] == (int)  $current['machine_id']
						&& $__row["id"] == 0 // ASSUME THAT THE OLD DATA WAS OVERIDDEN FROM SAME MACHINE
					)
					{
						$existingClause = $this->__getCurrentShiftClause();
						$existingClause['machine_id'] = $current['machine_id'];

						$existingOne = $this->find('first', array('conditions'=>$existingClause));

						if(isset($existingOne[$this->alias]))
						{
							$same = true;
							$fromRequest = $__row;
							$fromRequestIndex = $index;
							$existingOne = $existingOne[$this->alias];

							//check if timein is earlier
							$machine_time_in = strtotime($existingOne['machine_timein']);
							$time_in = strtotime($existingOne['timein']);

							if($time_in < strtotime($__row['timein']))
							{
								$__row['id'] = $existingOne["id"];
								$__row['machine_time_in'] = $existingOne['machine_timein'];
								$__row['timein'] = $existingOne['timein'];
							}
						}

					}
				}

				if(!$same)
					$this->__updateRow($row, $index);
			}
		}

		if($fromRequest)
		{
			unset($this->synchData->rows[$fromRequestIndex]);
			//we will not save this and force client to recalculate :)
		}
	}

	function __getCurrentShiftClause()
	{
		//return current
		return array(
			"machine_id !=" => -1, // not equal to machine server
			"user_id" => $this->session["user_id"],
			"company_id" => $this->session["company_id"],
			"start_shift" => (string) $this->requestData->STARTSHIFT,
			"end_shift" => (string) $this->requestData->ENDSHIFT,
		);

	}


	function GetNewerDataWhereClause($automateLastSynched = true)
	{
		$or = parent::GetNewerDataWhereClause();
		$or["machine_id != "] = 0 ;

		//return current
		$or2 = $this->__getCurrentShiftClause();

		$conditions = array("OR"=>array($or, $or2));

		debug($conditions);

		return $conditions;
	}

	function getMachineID()
	{
		if($this->session)
		{
			return $this->session["machine_id"];
		}
	}

	public function exists($id = null) {
		if ($id === null) {
			$id = $this->getID();
		}
		if ($id === false) {
			return false;
		}
		$conditions = array($this->alias . '.' . $this->primaryKey => $id);
		$query = array('conditions' => $conditions, 'recursive' => -1, 'callbacks' => false);
		return ($this->find('count', $query) > 0);
	}

	public static function WithinRange($timesheet, $time)
	{
		if(!$timesheet) return;

		$timesheet = isset($timesheet["Timesheet"]) ? $timesheet["Timesheet"] : $timesheet;

		$startShift = strtotime($timesheet["start_shift"]);
		$endShift =  strtotime($timesheet["end_shift"]);
		$time =  strtotime($time);

		return ($startShift <= $time && $endShift >= $time);
	}

	public static function GetStartTimeIn($timesheet)
	{
		if(!$timesheet) return;

		$timein = $timesheet['timein'];
		$machinetimein = $timesheet['machine_timein'];
	}

	public function getFirstLastMethodCall($startShift, $endShift)
	{
		$sql = "SELECT * FROM clm_method_calls_1 as A  WHERE local_time >= '$startShift'  AND local_time <= '$endShift'";
		$data = $this->query($sql);
		if($data && sizeof($data) > 0)
		{
			$count = sizeof($data);
			return array("first"=>$data[0]["A"], "last"=>$data[$count-1]["A"]);
		}
	}



	function GetReturnFields()
	{
		if(!in_array("machine_name", $this->fields) && in_array("machine_name", $this->returnFields))
		{
			$index = array_search("machine_name", $this->returnFields);
			unset($this->returnFields[$index]);
		}

		if(!$this->requestData->ID)
		{
			return $this->fields;
		}

		if(isset($this->returnFields))
		{
			return $this->returnFields;
		}

		if(isset($this->extraReturnFields))
		{
			return array_merge($this->extraReturnFields, $this->fields);
		}

		return $this->fields;
	}


}

/** END OF FILE **/