<?php
require_once("ClientDataModel.php");
class Snapshot extends ClientDataModel
{
	var $directory = "snapshot";
	var $name ="Snapshot";
	var $useTable = "snapshots_raw";
	var $primaryKey = "snapshot_id";


	var $fields = array (
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"device_name",
		"image_raw"
	);


	var $uniqueKeys = array(
		"machine_id",
		"company_id",
		"local_time",
		"user_id"
	);

	var $companyId;
	var $userId;
	var $index;

	var $saveLatestData = false;
	var $latestDataFields = array(
		"last_snapshot_filepath" => "file_path",
		"last_snapshot_filename" =>  "file_name",
		"last_snapshot_local_time" =>  "local_time",
	);


	function __saveImage($image, $id)
	{
		$snapshot =clone $image;
		//make the original image folder  available
		$snapshot->path = $image->path . "snapshot" . DS;
		$this->__makeSurePathExists($snapshot->path);

		if(!file_exists($snapshot->path . $snapshot->fileName))
			$snapshot->upload();


		$this->__updateImageData($image, $id);
	}




}
/** END OF FILE **/