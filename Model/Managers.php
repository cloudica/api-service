<?php
require_once("ClientDataModel.php");
class Managers extends ClientDataModel
{
	var $name = "Managers";

	var $useTable = "managers_users";


	var $fields = array(
		"user_id",
		"company_id",
		"firstname",
		"lastname",
		"user_email"
	);

	function returnResponse($userId, $companyId)
	{
		return $this->getManagers($userId, $companyId);
	}

	function getManagers($userId, $companyId = false)
	{
		if(!$companyId)
		{
			$companyId = isset($userId[1]) ? $userId[1] : false;
			$userId = isset($userId[0]) ? $userId[0]: false;
		}

		if(!$userId || !$companyId) return;

		$data = $this->find("all",
			array(
				"conditions"=> array(
				"user_id"=>$userId,
				"company_id"=>$companyId
				),
				"fields"=>"manager_id"
			));

		if($data)
		{
			$data = array_values(Set::flatten($data));

			//switch db
			$this->useDbConfig = "default";
			$this->table = "users";

			$data = $this->find("all", array(
				"conditions"=>array("user_id" => $data),
				"fields"=>$this->fields
			));

			foreach($data as $key=>&$value)
			{
				$value[$this->alias]["company_id"] = $companyId;
			}

			return $this->ToCSV($data, $this->fields);

		}



		return "";
	}

}
