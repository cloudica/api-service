<?php
require_once("MachineBasedClientData.php");
class StatusMessage extends MachineBasedClientData
{
	var $name = "StatusMessage";
	var $useTable = "status_messages_1";
	var $order = "machine_time DESC";

	var $fields = array (
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"status_message",
		"status"
	);


	var $virtualFields = array(
		"is_synch" => "CONCAT('Synched')", // synch status automate
		"session_id" =>  "CONCAT('0')", //no session id
		"current" => "CONCAT('0')" //assume 0
	);

	//automate the return fields expected by client
	var $returnFields = array(
		"is_synch",
		"session_id",
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"status_message",
		"status",
		"current"
	);

	var $uniqueKeys = array(
		"user_id",
		"company_id",
		"machine_id",
		"local_time",
		"machine_time"
	);

	var $saveLatestData = true;
	var $latestDataFields = array(
		"status_message" => "status_message",
	//	"online_status" => "status"
	);



}