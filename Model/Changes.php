<?php
require_once("ClientDataModel.php");
require_once("SynchData.php");
class Changes extends ClientDataModel
{
	var $name = "Changes";
	var $useTable = "changes";

	var $fields = array(
		"schedules",
		"module_configs",
		"managers"
	);

	var $models = array(
		"schedules" => array(
			"className" => "ShiftSchedule",
			"method"=> "getEmployeeSchedule",
			"type" => "shiftschedule",
			"params"=> array("employee_id")
		),
		"module_configs" => array(
			"className" => "ClientModuleConfig",
			"method"=> "getConfigs",
			"type" => "userconfig",
			"params" => array("user_id", "company_id")
		),
		"managers" => array(
			"className" => "Managers",
			"method"=> "getManagers",
			"type" => "managers",
			"params" => array("user_id", "company_id")
		),
		"machine" => array(
			"className" => "MachineInfo",
			"method"=> "GetOtherMachineData",
			"type" => "machine",
			"params" => array("user_id", "company_id", "machine_id")
		)
	);

	function getChangedData($field)
	{
		try
		{
			if(isset($this->models[$field]))
			{
				$model = $this->models[$field]["className"];
				App::import("Model", $model);
				$model = new $model($model, null, $this->useDbConfig);
				$method = $this->models[$field]["method"];


				if(method_exists($model, $method) && $this->session)
				{
					$params = $this->models[$field]["params"];
					$parameterArray = array();
					foreach($params as $param)
					{
						if(isset($this->session[$param]))
						{
							$parameterArray[] = $this->session[$param];
						}
					}
					$model->overrideCache = true;
					$data = call_user_func(array($model, $method), $parameterArray);
					$model->overrideCache = false;

					return array("type"=>$this->models[$field]["type"], "data"=>$data);
				}

			}
		}
		catch (Exception $e)
		{
			debug($e->getMessage());
			return false;
		}

	}

	function getUserChanges($userId, $employeeId )
	{
		$userId = $userId ? $userId : 0;
		$employeeId = $employeeId ? $employeeId : 0;

		if(!isset($this->session)) return null;

		if(!isset($this->session["company_id"])) return null;

		$companyId = $this->session["company_id"];

		if(!$companyId || !$userId) return null;

		$conditions =array("company_id" => $companyId, "user_id" => $userId);

		$data = $this->find("first",  array("conditions"=> $conditions));

		if(!isset($data[$this->alias]))
		{
			$changes = array(
				"user_id" => $userId,
				"company_id" => $companyId,
				"employee_id" => $employeeId
			);

			$this->create();
			$this->save($changes);

			$changes["id"] = $this->getInsertID();
			$data = array($this->alias => $changes);
		}

		$eid = $data[$this->alias]["employee_id"];

		if(!$eid)
		{
			$employeeId = $this->getEmployeeId($companyId, $userId);
			$this->id = $data[$this->alias]["id"];
			$this->save(array("employee_id"=>$employeeId));

			$data[$this->alias]["employee_id"] = $employeeId;
		}

		return $data;
	}

	function getEmployeeId($companyId, $userId)
	{
		$conditions =array("company_id" => $companyId, "user_id" => $userId);
		if(!$employeeId)
		{
			App::import("Model", "Account");
			$Account = new Account();
			$employeeId = $Account->field("employee_id", $conditions);
		}
	}

	function updateColumn($id, $column)
	{
		if(array_search($column, $this->fields) >= 0)
		{
			$this->id = $id;
			$date = new DateTime("now", new DateTimeZone("UTC"));
			$this->save(array($column =>	$date->format("Y-m-d H:i:s")));
		}
	}
}

/** END OF FILE **/