<?php
require_once("ClientDataModel.php");
class EmployeeScheduleChanges extends ClientDataModel
{
	var $name = "EmployeeScheduleChanges";

	var $useTable = "employee_schedule_changes";
	var $order = "start_date";
	var $overrideCache = false;

	var $fields = array(
		"id",
		"employee_id",
		"user_id",
		"company_id",
		"schedule_id",
		"notes",
		"is_current",
		"start_date"
	);

	function GetScheduleChanges ()
	{
		$currentChanges = $this->find("all", array(
			"conditions" => array(
				"is_current" => 1
			),
			"order" => "created ASC",
			"group" => "user_id"
		));

		return $currentChanges;
	}


}
