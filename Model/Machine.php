<?php
class Machine extends AppModel
{
	var $name = "Machine";
	var $useTable = "machine";

	var $fields = array(
		"id",
		"os",
		"memory",
		"mac_address",
		"hd_size",
		"ip_internal",
		"login_username",
		"machine_name"
		);
	var $uniqueKeys = array(
		"mac_address"
		);


}

/** END OF FILE **/