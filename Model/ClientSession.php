<?php
class ClientSession extends AppModel
{
	var $name = "ClientSession";
	var $useTable = "sessions_1";

	var $fields = array(
		"user_id",
		"company_id",
		"session_id",
		"machine_id",
		"expire_time",
		"company_domain",
		"revision_number",
		"employee_id"
	);

	var $uniqueKeys = array(
		"user_id",
		"company_id"
	);

	var $validate = array(
		"user_id" => array(
			"rule"=>"required",
			"allowEmpty"=>false
		),
		"company_id" => array(
			"rule"=>"required",
			"allowEmpty"=>false
		)
	);


	/**
	 *
	 * @param string $token
	 */
	function getSession($token, $update = false)
	{
		if(empty($token)) return null;

		$token = (string) $token;

		$data = $this->findBySessionId($token);

		if(isset($data[$this->alias]))
		{
			return $data;
		}
		else
		{
			return null;
		}
	}

	/*
	 * $data mixed
	 */
	function updateSesion($data)
	{

		if(is_object($data['expire_time']))
		{
			$data['expire_time'] = $data['expire_time']->format("Y-m-d H:i:s");
		}

		$dataToSave = array($this->alias=>$data);

		$dataToSave[$this->alias]["created"] = $this->Now;

		$dataToSave[$this->alias]["created_by"] = "API Service";

		$fields = implode(",", array_keys($dataToSave[$this->alias]));

		$values = '"'. implode('","', array_values($dataToSave[$this->alias])) . '"';

		$updateFields = implode(',', array(
			"machine_time=VALUES(machine_time)",
			"local_time=VALUES(local_time)",
			"machine_id=VALUES(machine_id)",
			"session_id=VALUES(session_id)",
			"expire_time=VALUES(expire_time)",
			"last_updated=VALUES(last_updated)",
			"updated_by=VALUES(updated_by)",
			"revision_number=VALUES(revision_number)",
			"employee_id=VALUES(employee_id)",
		));

		$sql = $this->renderStatement("insert_on_update",
			array(
				"table"=>$this->tableName,
				"fields"=>$fields,
				"values"=>$values,
				"updateFields"=>$updateFields
			)
		);

		$this->query($sql);

		$this->data = $dataToSave;

		$this->token = $data["session_id"];
	}


	/**
	 *
	 * @param $userId
	 * @param $companyId
	 */
	function endSession($userId, $companyId, $machineId, $email)
	{
		if(empty($userId)) return false;
		if(empty($companyId)) return false;
		if(empty($machineId)) return false;



		$query = "DELETE FROM clm_sessions WHERE user_id = '$userId' AND company_id = '$companyId' AND machine_id = '$machineId'";
		$this->query($query);
		return true;
	}
}

/** END OF FILE **/