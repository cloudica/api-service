<?php
require_once("ClientDataModel.php");
class ImageModel extends ClientDataModel
{
	var $directory = "original";
	var $fileNameField = "file_name";
	var $filePathField = "file_path";
	var $images;


	var $fileName = array(
		"machine_id",
		"company_id",
		"local_time",
		"user_id"
	);


	/** test override **/
	function __updateRow(&$row, $index)
	{
		if(!isset($this->synchData->images))
		{
			$this->synchData->images = array();
		}
		$fields = $this->fields;
		$csv = str_getcsv($row);

		$array = array();

		$uniqueKey = array();

		$uniqueKey["user_id"] = $this->session["user_id"];

		$fieldKey = 0;
		foreach($csv as $key=>$value)
		{
			if($key == $this->imageIndex)
			{
				$array["image"] = $value;
			}
			else
			{
				$field = $fields[$fieldKey];
				$array[$field] = $value;
				if(in_array($field, $this->uniqueKeys))
				{
					$uniqueKey[$field] = $value;
				}
				$fieldKey ++;
			}

		}

		if(isset($array["image"]))
		{
			$this->synchData->images[$index] = array("image" => $array["image"],
			"key" => $uniqueKey);

			$search =  ',"' . $array["image"]. '"';

			 $pos = strrpos($row, $search);

		    if($pos !== false)
		    {
		        $row = substr_replace($row, "", $pos, strlen($search));
		    }

			unset($array["image"]);
		}


		parent::__updateRow($row, $index);

	}

	function SaveSynchData($returnId = false)
	{
		$success = parent::SaveSynchData($returnId);

		$this->__saveImages();

		return $success;
	}

	function __getFilePath($imageKey)
	{
		$date = substr($imageKey["local_time"], 0, 10);
		$prefix = "CLOUDICA_" .$date ;

		$path = WWW_ROOT . "upload" . DS . $prefix;

		$this->__makeSurePathExists($path);

		return $path . DS;
	}

	function __makeSurePathExists($path)
	{
		if(!file_exists($path)) @mkdir ($path);

		if(!file_exists($path . DS . "index.html")) fopen ($path . DS . "index.html", "w");
	}


	function __saveImages()
	{
		if(!isset($this->synchData->images))
		{
		}
		include_once("FileUpload.php");

		$keys = array();

		foreach($this->synchData->images as $image)
		{
			$keys[] = $image["key"];
		}

		$insertedData = $this->find("all", array("conditions" => array("OR" => $keys)));

		if(sizeof($insertedData) == 0) return;

		foreach($insertedData as $key => $data)
		{
			$data = $data[$this->alias];
			$uniqueKey = array();
			foreach($data as $fieldName => $fieldValue)
			{
				if(in_array($fieldName, $this->imageKeys))
				{
					$uniqueKey[$fieldName] = $fieldValue;
				}
			}

			foreach($this->synchData->images as &$image)
			{
				if($image["key"] == $uniqueKey)
				{
					$image[$this->primaryKey] = $data[$this->primaryKey];
					$image["data"] = $data;
				}
			}
		}


		$lastImage = false;


		foreach($this->synchData->images as &$image)
		{

			if(isset($image[$this->primaryKey]))
			{
				$imageString = $image["image"];
				$imageKey = $image["key"];

				$file = new FileUpload($imageString, true);
				$file->path =  $this->__getFilePath($imageKey);
				$file->_path = str_replace(WWW_ROOT."upload".DS, "",    $file->path);
				$file->fileName = str_replace(" ", "", str_replace(":", "", implode("_", array_values($imageKey)))) . ".jpeg";
				$file->fileType = "image/jpeg";
				$file->resize = false;

				$this->__saveImage($file, $image[$this->primaryKey]);

				$image["file"] = $file;

				if(isset($image["data"]))
				{
					$image["data"]["file_name"] = $file->fileName;
					$image["data"]["file_path"] = str_replace(DS, "", str_replace(WWW_ROOT . "upload" . DS , "",  $file->path));
				}

				$lastImage = $image;

			}
			else
			{
				 CakeLog::write('log', 'Line 183 Failed to save image data, primary key is missing for '. (json_encode($image["key"])) );
			}
		}
		//query
		$this->__saveLastData($lastImage["data"]);
	}


	function __saveImage($image, $id)
	{
		$result = false;
		if(!file_exists($image->path . $image->fileName))
			$result = $image->upload();

		if(!$result)
		{
			CakeLog::write('log', 'Failed to save image data ' . $id );
		}

		$this->__updateImageData($image, $id);
	}

	function __updateImageData($image, $id)
	{
		if(!$image->fileName)
		{
			$data = json_encode($image);
			$imageId = $id;
			CakeLog::write('log', 'Failed to save image data ' . $id );
			CakeLog::write('log', $data );

			/** JUST IN CASE IT FAILS **/
			$this->id = $id;
			$d = $this->save(
			array($this->alias => array(
				"original_name" => str_replace(DS, "", $image->base64EncodedString)
			)));
			return;
		}

		$this->id = $id;
		$d = $this->save(
			array($this->alias => array(
				"file_path" => str_replace(DS, "", $image->_path),
				"file_name" => $image->fileName
			)));
	}


}
/** END OF FILE **/