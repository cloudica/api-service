<?php
require_once("ClientDataModel.php");
class ActiveWindowInstance extends ClientDataModel
{
	var $directory = "original";
	var $name ="ActiveWindowInstance";
	var $useTable = "awi_raw";
	var $primaryKey = "id";

	var $saveLatestData = false;

	var $fields = array
	(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"start_time",
		"end_time",
		"machine_start_time",
		"machine_end_time",
		"app_id",
		"keystrokes",
		"keystrokes_raw_data",
		"keystrokes_formatted_data",
		"image_raw"
	);

	var $companyId;
	var $userId;
	var $index;

	var $uniqueKeys = array(
		"machine_id",
		"company_id",
		"local_time",
		"user_id",
		"app_id"
	);

	function decodeApplicationID($appId)
	{
		return base64_decode($appId);
	}
	function getLocalTime($appTime)
	{
		return date("Y-m-d", strtotime($appTime));
	}



	function __saveImage($image, $id)
	{
		$this->__saveOriginalImage($image);
		$this->__saveThumbnailImage($image);

		$this->__updateImageData($image, $id);

	}

	function __saveOriginalImage($image)
	{
		$original =clone $image;
		//make the original image folder  available
		$original->path = $image->path . "original" . DS;
		$this->__makeSurePathExists($original->path);

		if(!file_exists($original->path . $original->fileName))
			$original->upload();
	}

	function __saveThumbnailImage($image)
	{
		$thumbnail =clone  $image;
		//make the original image folder  available
		$thumbnail->path = $image->path . "thumbnail" . DS;
		$this->__makeSurePathExists($thumbnail->path);

		$originalSource = $image->path. "original" . DS . $thumbnail->fileName;

		$thumbnail->__generateThumbnail($originalSource);
	}


}
/** END OF FILE **/