<?php
require_once("Timesheet.php");
require_once("ClientDataModel.php");
class TimesheetCalculator extends ClientDataModel
{
	var $name = "TimesheetCalculator";

	static $log = null;

	//properties
	var $shift;
	var $timesheet;
	var $isCalculating;
	var $currentStatusLog;
	var $statusLogs;
	var $calls;
	var $sesssion;
	var $workTime;
	var $idleTime;

	var $useTable = false;

	var $calculation = null;


	var $currentTimesheetLog;


	const ALLOWED_INTERVAL = 5;
	const DATEMINVALUE = "";
	const SIGNOUT_STATUS = 'SIGNOUT';
	const ONBREAK_STATUS = 'ONBREAK';
	const IDLE_STATUS = "IDLE";
	const ACTIVE_STATUS = "ACTIVE";

	var $models = array(
		"Timesheet" =>  false,
		"StatusLog" =>  false,
		"MethodCall" => false
	);

	function __get($name)
	{
		if(isset($this->models[$name]))
		{
			$model = $this->models[$name];
			if(!$model)
			{
				if($this->session == null)
				{
					throw ("No session");
				}

				App::import("Model", $name);
				$Model = new $name();
				$Model->session = $this->session;
				$this->models[$name] = $model;
				return $Model;
			}

			return $this->models[$name];
		}

		return parent::__get($name);
	}

	function OnTimesheetIsCalculating($status)
	{
		$this->isCalculating = $status;
		if($status)
		{
			debug("Timesheet is Calculating");
			self::WriteToLog("Timesheet is Calculating ". $this->timesheet['id']);
		}
	}

	function OnTimesheetCalculated()
	{
		debug("Timesheet Calculated");
	}

	//always from the top if server calculation
	function StartTimesheetCalculation()
	{
		$this->calculation = null;

		$this->session["machine_id"] = $this->timesheet["machine_id"];

		$this->ResetCalculation();

		$this->ReloadMethodCalls();

		debug(sizeof($this->calls));
		if($this->calls && sizeof($this->calls) > 0)
		{
			//update timein
			$this->timesheet["timein"] = $this->calls[0][$this->MethodCall->alias]["local_time"];

			//update temptimeout
			$this->timesheet["temp_timeout"] = $this->calls[sizeof($this->Calls)][$this->MethodCall->alias]["local_time"];
			$this->timesheet ["tempmachine_timeout"] = $this->calls[sizeof($this->calls) - 1][$this->MethodCall->alias]["machine_time"];

		}

		$this->CalculateTimesheet();

	}

	function CalculateTimesheet()
	{
		if($this->isCalculating) return;
		$this->OnTimesheetIsCalculating(true);
		$this->Calculate();
		$this->OnTimesheetIsCalculating(false);
	}

	function Calculate()
	{
		if(!isset($this->timesheet))
		{
			throw("Undefined timesheet");
		}

		if(!isset($this->shift))
		{
			throw("Undefined shift");
		}

		if(!$this->statusLogs)
		{
			$this->statusLogs = array();
		}

		$calls = $this->calls; //should not include log_status = 1
		if($calls)
		{
			$lastCalculated = "";
			$count = sizeof($calls);
			$allowedInterval = self::ALLOWED_INTERVAL;
			$ids = array();

			for($i = 0; $i < sizeof($calls); $i++)
			{
				$current = $calls[$i][$this->MethodCall->alias];
				if($i < sizeof($calls) - 1) $next = $calls[$i+1][$this->MethodCall->alias];

				if(
					isset($next) &&
					$current["id"] != $next["id"]
				)
				{
					if($current["user_status"] == self::SIGNOUT_STATUS)
					{
						$ids[$i] = $current["id"];
					}
					else
					{
						$interval = self::TimeDifference($current["local_time"], $next["local_time"]);

						if($interval <= $allowedInterval)
						{
							$lastCalculated = $current["local_time"];
							$ids[$i] = $current["id"];

							$statusLog = $this->CreateLog($current, $next);
							$this->UpdateStatusLogs($statusLog);
						}
					}

				}

			}

			if(sizeof($ids) > 1)
			{
				if($this->timesheet["machine_id"] >= 0)
				$this->MethodCall->query("UPDATE ".$this->tablePrefix. $this->MethodCall->useTable." SET log_status=1 WHERE id IN (". implode(",", $ids).")");
			}

			$working = 0;
			$idle = 0;

			foreach($this->statusLogs as $log)
			{
				if(self::IsWorking($log))
				{
					$working += $log["work_time"];
				}
				if(self::IsWorkingIdle($log))
				{
					$idle += $log["work_time"];
				}

				//on update query
				$klog = $log; unset($klog["id"]); $fields = array_keys($klog); $values = array_values($klog);


				$query = "INSERT INTO clm_" . $this->StatusLog->table . "(" . implode(",", $fields) . ") VALUES ('" . implode("','", $values) . "')
						ON DUPLICATE KEY UPDATE status_message = VALUES(status_message),
						status = VALUES(status),
						is_calibrated = VALUES(is_calibrated),
						is_compromised = VALUES(is_compromised),
						start_time = VALUES(start_time),
						machine_start_time = VALUES(machine_start_time),
						local_time = VALUES(local_time),
						end_time = VALUES(end_time),
						work_time = VALUES(work_time),
						machine_end_time = VALUES(machine_end_time),
						work_time = VALUES(work_time)
				" ;

				try
				{
					$this->StatusLog->query($query);
				}
				catch(Exception $e)
				{
					debug($e->getMessage());
				}

			}

			$this->timesheet["temp_work_time"] = $this->workTime = $this->timesheet["work_time"] = $working;
			$this->timesheet["temp_idle_time"] = $this->idleTime = $this->timesheet["idle_time"] = $idle;

			$this->timesheet["last_calculated"] = $lastCalculated;

			$this->Timesheet->query(
				"UPDATE ".$this->Timesheet->tablePrefix.$this->Timesheet->useTable."
					SET temp_work_time = $working, work_time = $working, temp_idle_time = $idle, idle_time = $idle, last_calculated='$lastCalculated' WHERE id = ". $this->timesheet["id"]
			);

		}

		$this->OnTimesheetCalculated();

	}

	function WithinShift($time)
	{
		return Timesheet::WithinRange($this->timesheet, $time);
	}

	function CreateLog($current, $next)
	{
		$date = $this->shift["start_date"];
		if(!$date)
		{
			return null;
		}
		$statusLog = array(
			"status_message" => $current["user_status_message"],
			"status" => $current["user_status"],
			"data_compromised" => $current["data_compromised"],
			"start_time" => $current["local_time"],
			"machine_start_time" => $current["machine_time"],
			"local_time" => $date,

			"machine_id" => -1,
			"user_id" => $current["user_id"],
			"company_id" => $current["company_id"],
		);


		if($next == null)
		{
			$statusLog["end_time"] = "";
			$statusLog["work_time"] = 0;
		}
		else
		{
			$statusLog["end_time"] = $next["local_time"];
			$statusLog["machine_end_time"] = $next["machine_time"];
			$statusLog["work_time"] = self::TimeDifference($statusLog["start_time"], $statusLog["end_time"]);
		}


		return $statusLog;
	}

	function UpdateStatusLogs($statusLog)
	{
		if(!$statusLog) return;

		if(!$this->statusLogs)
		{
			$this->statusLogs = array();
		}

		//create unique index for status logs

		$key = self::getStatusLogUniqueKey($statusLog);
		if(!$this->currentStatusLog)
		{

			$this->currentStatusLog = $this->statusLogs[$key] = $statusLog;
		}
		else {
			$updated = $this->AddStatusLog($statusLog);

			if(!$updated)
			{

				//insert
				if(strtoupper($statusLog["status"]) != self::SIGNOUT_STATUS)
				{
					$this->currentStatusLog = $this->statusLogs[$key] = $statusLog;
				}
			}
		}


		if(self::IsWorking($this->currentStatusLog))
		{
			$this->workTime = $this->currentStatusLog["work_time"];
		}

		if(self::IsWorkingIdle($this->currentStatusLog))
		{
			$this->idleTime = $this->currentStatusLog["work_time"];
		}
	}

	static function WriteToLog($msg)
	{
		if(self::$log)
		{
			self::$log->lwrite($msg);
		}
	}

	function ResetCalculation()
	{
		self::WriteToLog("Reset Calculation");

		$this->calls = null;
		$this->statusLogs = null;
		$this->currentStatusLog = null;

		$this->ResetTimesheet();
		$this->ResetLogs();
	}

	function ReloadMethodCalls()
	{
		self::WriteToLog("Reload MethodCalls and StatusLogs");

		if(!$this->session) return;

		if(!$this->calls)
		{
			$this->calls = $this->LoadMethodCalls();
		}

		if(!$this->statusLogs)
		{
			$this->statusLogs = array();
		}


	}

	function LoadMethodCalls()
	{
		$calls = array();
		if(!$this->shift) return $calls;

		$conditions = array(
			"local_time >="=>$this->GetStartTime(),
			"local_time <="=> $this->GetEndTime(),
			"company_id"=>$this->session["company_id"],
			"user_id"=>$this->timesheet["user_id"]
		);
		if((int)$this->timesheet["machine_id"] > 0)
		{
			$conditions["machine_id"] = $this->timesheet["machine_id"];
		}

		self::WriteToLog("loading methodcalls: " . json_encode($conditions));
		$calls = $this->MethodCall->find("all", array(
			"conditions"=>$conditions,
			"order" => "local_time ASC"
		));

		self::WriteToLog("There are ".sizeof($calls) . " calls");

		return $calls;
	}

	static function getStatusLogUniqueKey($statusLog)
	{
		$unique = array(
				$statusLog["user_id"],
				$statusLog["company_id"],
				// $statusLog["machine_id"],
				$statusLog["local_time"],
				$statusLog["start_time"],
				$statusLog["machine_start_time"]
			);

		return implode("_", $unique);
	}

	function GetStartTime()
	{
		$mtimein = $this->timesheet["machine_timein"];
		$timein = $this->timesheet["timein"];
		$timein = AppModel::IsMinValue($timein) ? $mtimein : $timein;

		$shiftStart = $this->shift["start_time"];

		if(AppModel::IsMinValue($timein))
		{
			return $shiftStart;
		}


		if(strtotime($shiftStart) < strtotime($timein)) return $shiftStart;
		return $timein;
	}

	function GetEndTime()
	{
		$timeout = $this->timesheet["timeout"];
		$mtimeout = $this->timesheet["machine_timeout"];

		$timeout = AppModel::IsMinValue($timeout) ? $mtimeout : $timeout;

		$tempTimeout = $this->timesheet["temp_timeout"];
		$mtempTimeout = $this->timesheet["tempmachine_timeout"];
		$tempTimeout = AppModel::IsMinValue($tempTimeout) ? $mtempTimeout : $tempTimeout;

		$endShift = $this->shift["end_time"];

		if(AppModel::IsMinValue($timeout))
		{

			if(AppModel::IsMinValue($tempTimeout))
			{
				if(strtotime($endShift) > strtotime($tempTimeout)) return $endShift;

				return $tempTimeout;
			}

			return $endShift;
		}

		if(strtotime($endShift) > strtotime($timeout)) return $endShift;

		return $timeout;
	}

	function ResetTimesheet()
	{
		$this->Timesheet->id = $this->timesheet["id"];

		if(!$this->timesheet["work_time"])
			$this->Timesheet->saveField("work_time", 0);

		if(!$this->timesheet["idle_time"])
			$this->Timesheet->saveField("idle_time", 0);

		$this->timesheet["work_time"] = 0;
		$this->timesheet["idle_time"] = 0;
	}

	function ResetLogs()
	{
		$startTime = $this->GetStartTime();
		$endTime = $this->GetEndTime();

		$this->StatusLog->deleteAll(array(
			"start_time >="=> $startTime,
			"end_time <="=> $startTime,
			"company_id"=>$this->session["company_id"],
			"user_id"=>$this->timesheet["user_id"],
			"machine_id" => $this->timesheet["machine_id"]
		), false);

	}

	function ResetMethodCalls()
	{
		$startTime = $this->GetStartTime();
		$endTime = $this->GetEndTime();
		$companyId = $this->session["company_id"];
		$userId = $this->session["user_id"];

		$query = "UPDATE ".$this->MethodCall->tablePrefix.$this->MethodCall->useTable."
				SET log_status=0 WHERE local_time >= '$startTime' AND local_time <= '$startTime' AND company_id = $companyId AND user_id = $user_id";

		if((int) $this->timesheet["machine_id"] > 0)
		{
			$query .= " AND machine_id = " . $this->timesheet["machine_id"];
		}

		$this->MethodCall->query($query);
	}

	function AddStatusLog($statusLog)
	{
		$referenceLog = $this->currentStatusLog;

		$updated = false;
		if($this->AllowStatusLogAdding($statusLog))
		{
			$bEndTime = $referenceLog["end_time"];
			$cEndTime = $referenceLog["machine_end_time"];
			$bStatus = $referenceLog["status"];
			$bWorkTime = $referenceLog["work_time"];

			$referenceLog["end_time"] = $statusLog["end_time"];
			$referenceLog["machine_end_time"] = $statusLog["machine_end_time"];
			$referenceLog["status"] = $statusLog["status"];

			$workTime = self::TimeDifference($referenceLog["start_time"], $referenceLog["end_time"]);

			$referenceLog["work_time"] = $workTime;

			$key = self::getStatusLogUniqueKey($referenceLog);
			$this->currentStatusLog = $this->statusLogs[$key] = $referenceLog;
			$updated = true;
		}

		return $updated;
	}

	function AllowStatusLogAdding($statusLog)
	{
		$referenceLog = $this->currentStatusLog;

		$withinAllowedInterval = $this->WithinAllowedInterval($statusLog);
		$sameStatus = $referenceLog["status"] == $statusLog["status"];
		$allowed = $withinAllowedInterval && $sameStatus;
		return $allowed;
	}

	function WithinAllowedInterval($statusLog)
	{
		$referenceLog = $this->currentStatusLog;
		$allowedInterval = 1;
		$minutes = self::TimeDifference($referenceLog["end_time"], $statusLog["start_time"]);

		$allowed =  $minutes <= $allowedInterval;
		return $allowed;
	}
	/**
	 * return time in minutes
	 */
	static function TimeDifference($start, $end)
	{
		return (strtotime($end) - strtotime($start)) / 60;
	}


	static function IsWorking($statusLog)
	{
		return strtoupper($statusLog["status"]) != self::ONBREAK_STATUS && strtoupper($statusLog["status"]) != self::SIGNOUT_STATUS;
	}

	static function IsWorkingIdle($statusLog)
	{

		return strtoupper($statusLog["status"]) == self::IDLE_STATUS && self::IsWorking($statusLog);
	}
}

/** END OF FILE **/