<?php
/**
 * @copyright     Copyright 2012
 * @link          http://cloudica.com
 * @author		  Lorelie Dazo
 * @version 	  Cloudica1.0
 * @package
 * @since         Cloudica1.0
 * @license
 *
 */

App::uses('Model', 'Model');

class AppModel extends Model
{
	//sets the session of the model using the user session
	var $session;
	var $uniqueKeys = array();

	function __get($property)
	{
		if($property == "tableName")
		{
			return $this->tablePrefix . $this->table;
		}
		else
			return parent::__get($property);
	}


	/**
	 * Auto filling metadata last_updated, created, created_by and updated_by
	 */
	function beforeSave()
	{
		$now = isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");

		$this->data[$this->alias]['last_updated'] = $now;

		 if( !isset($this->data[$this->alias][$this->primaryKey]))
		 	 $this->data[$this->alias]['created']  = $now;

		 if(!isset($this->data[$this->alias]['created_by']) && !isset($this->data[$this->alias][$this->primaryKey]))
		 	$this->data[$this->alias]['created_by'] = 'Cloudica API Service';

		 if(!isset($this->data[$this->alias]['updated_by']) )
		 	$this->data[$this->alias]['updated_by'] = 'Cloudica API Service';

		return true;
	}


	/**
	 *
	 * @param $data
	 */
	function ToCSV($data, $fields = null)
	{
		if($fields == null)
		{
			$fields = $this->fields;
		}

		if($data != null && isset($data[0]))
		{
			if (!$this->isBound)
				$keys = "{n}.".$this->alias.".". implode($fields, "\n{n}.".$this->alias.".");
			else
				$keys = "{n}.". implode($fields, "\n{n}.");

			$keys = explode("\n", $keys);

			$values = rtrim('"{'. implode('}", "{', array_keys($fields) )).'}"';

			$csvData = Set::format($data, $values, $keys);

			$csvData = array_map(function($str){return str_replace(array("\\r\\n", "\\r", "\\n", "\r\n", "\r", "\n"), "<br />",  $str);}, $csvData);
			$csvData = implode("\n", $csvData);

			return $csvData;

		}
		else return "";
	}

	function parseCSV($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = true)
	{
		$lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $csv_string);
		return array_map(
			function ($line) use ($delimiter, $trim_fields) {
				$fields = $trim_fields ? array_map('trim', str_getcsv($line, $delimiter)) : str_getcsv($line, $delimiter);
				return array_map(
					function ($field) {
						return str_replace('!!Q!!', '"', utf8_decode(urldecode($field)));
					},
					$fields
				);
			},
			$lines
		);
	}


	function parseSingleArray($values, $fields, $addAlias = false )
	{
		//remove empty values;
		if(sizeof($values) != sizeof($fields))
		{
			$x = array_diff_key($values, $fields);

			if(sizeof($values)  > sizeof($fields))
			{
				$values = array_diff_key($values, $x);
			}
			else
			{
				$fields = array_diff_key($fields, $x);
			}
			if(sizeof($values) != sizeof($fields))
			{
				$x = array_diff_key($fields, $values);

				if(sizeof($values)  > sizeof($fields))
				{
					$values = array_diff_key($values, $x);
				}
				else
				{
					$fields = array_diff_key($fields, $x);
				}
			}
		}

		if(sizeof($fields) != sizeof($values))
		{
			return false;
		}


		$data = array_combine($fields, $values);
		$params = array();

		//automate parsing of classnames
		foreach($data as $key=>$value)
		{
			$vs = explode(".", $key);
			if(sizeof($vs) == 2)
			{

				$params[$vs[0]]= isset($params[$vs[0]]) ? $params[$vs[0]] : array();
				$params[$vs[0]][$vs[1]] = $value;
			}
			else
			{

				$params[$this->alias] = isset($params[$this->alias]) ? $params[$this->alias] : array();
				$params[$this->alias][$key] = $value;

			}
		}

		foreach($params as $key=>$value)
		{
			if(empty($value))
			{
				unset($params[$key]);
			}
		}

		if(!$addAlias) return $params[$this->alias];
		return $params;
	}

	function renderStatement($type, $data)
	{
		extract($data);
		switch(strtolower($type))
		{
			case "insert_all" :
				return "INSERT INTO {$table}
					({$fields})
					VALUES
				({$values})";
				break;
			case "insert_on_update" :

				return "INSERT INTO {$table}
				({$fields})
				VALUES
				({$values})
				ON DUPLICATE KEY UPDATE {$updateFields}";
				break;
		}

		return false;
	}



	static function GetUniqueFieldsStatement($uniqueKeys, $fields, $pkey = "id")
	{
		if($uniqueKeys > 0)
		{
			$uniqueKeys[] = $pkey;
			$updateFields = array_diff($fields, $uniqueKeys);

			foreach($updateFields as &$field)
			{
				$field = array("$field", "VALUES($field)");
				$field = implode("=", $field);
			}
			$updateFields = implode(",", $updateFields);
			return $updateFields;
		}
	}

	function __getUniqueFieldsStatement($fields)
	{
		return self::GetUniqueFieldsStatement($this->uniqueKeys, $fields, $this->primaryKey);
	}

	function areUnique($fields, $optional = array())
	{
		if(!isset($optional["otherFields"]))
			$optional["otherFields"] = array();

		$otherFields = $optional["otherFields"];
	 	$data = $this->data[$this->alias];


		array_walk(array_keys($data), function($key)use($otherFields, &$data,&$fields) {
			if(in_array($key,$otherFields))
			{
				$fields[$key] = $data[$key];
			}
		});

		$notProvided = (sizeof($otherFields) + 1) == sizeof(array_keys($fields));

		if(!$notProvided)
		{
			$required = isset($optional["required"]) ? $optional["required"] : false;
			$allowEmpty = isset($optional["allowEmpty"]) ? $optional["allowEmpty"] : true;

			if(!$required) return true;
			if($allowEmpty) return true;
		}
		$unique = $this->isUnique($fields, false);

		return $unique;
	}


	public static function IsMinValue($stringDate)
	{
		$stringDate = substr($stringDate, 0, 10);

		$minDate1 = "0000-00-00";
		$minDate2 = "0001-01-01";

		return $stringDate == $minDate1 || $stringDate == $minDate2;
	}
}

/** END OF FILE **/