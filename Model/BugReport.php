<?php
/**
 * what makes a bug report unique?
 * machine,
 * bug_id,
 *
 *
 * @author user
 *
 */
class BugReport extends AppModel
{
	var $name = "BugReport";
	var $useTable = "bug_reports";
	var $report ;

	function getBugReportId(Report $report, $bugId)
	{
		$reports = $this->getBugReportIdByBugIdAndReporter($bugId, $report->IpInternal, $report->MacAddress, $report->Email);

		if($reports == null)
		{
			$report = array("BugReport"=> array(
				"bug_id" => $bugId,
				"machine_time" =>$report->Timestamp,
				"machine_ip" => $report->IpInternal,
				"email" => $report->Email,
				"first_reported" => $report->Timestamp,
				"last_reported"=> $report->Timestamp,
				"instance" => 1,
				"comments" => $report->Comments,
				"update_reporter"=>$report->UpdateReporter,
				"client_build"=>$report->ClientBuild,
				"bugreporter_build"=>$report->BugReporterBuild,
				"mac_address"=>$report->MacAddress
			));

			$reportId = $this->save($report);

			return $reportId;

		}
		else if (sizeof($reports) ==1)
		{
			//update the bug report
			$reportId = $reports[0]["BugReport"]["id"];

			$timestamp = $report->Timestamp;
			$instance = $reports[0]["BugReport"]["instance"] + 1;

			//update sql
			$this->updateBugReport($reportId, $timestamp, $instance);

			return $reportId;
		}
		else
		{
			return "too many to mention";
		}
	}

	function updateBugReport($reportId, $timestamp, $instance)
	{
		$sql = "UPDATE ".$this->tableName. " SET machine_time = '$timestamp', last_reported = '$timestamp', instance='$instance' WHERE id=$reportId";

		$this->query($sql);
	}


	function getBugReportIdByBugIdAndReporter($bugId, $machineIp, $macAddress, $email)
	{
		$data = $this->find('all', array(
			'conditions' => array('bug_id' => $bugId, 'machine_ip' => $machineIp, "mac_address" =>$macAddress, "email"=>$email )
		));
		if(sizeof($data) == 0)
		{
			return null;
		}
		else
		{
			return $data;
		}

	}

}
/** END OF FILE **/