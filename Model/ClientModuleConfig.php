<?php
require_once("ClientDataModel.php");
class ClientModuleConfig extends ClientDataModel
{
	var $name = "ClientModuleConfig";

	var $useTable = "clientmodule_config";
	var $order = "company_id";

	var $virtualFields = array("mid" => "CONCAT(user_id, '_', company_id, '_', clientmodule, '_', configuration)");
	var $fields = array(
		"company_id",
		"clientmodule",
		"configuration",
		"configurationvalue",
		"configuration_type"
	);

	function returnResponse($userId, $companyId)
	{
		return $this->getConfigs($userId, $companyId);
	}


	function getConfigs($userId, $companyId = 0)
	{
		if(is_array($userId))
		{
			$companyId = isset($userId[1]) ? $userId[1] : 0;
			$userId = $userId[0];
		}
		$data =  $this->__getConfigs($userId);


		if(is_numeric($companyId) && $data != null)
		{
			$configuration = array();

			foreach ($data as $key=>$value)
			{
				$cId = $value["ClientModuleConfig"]["company_id"];
				if(!isset($configuration[$cId]))
				{
					$configuration[$cId] = array();
				}
				$configuration[$cId][] = $value;
			}

			if(isset($configuration[$companyId]))
				$data = $configuration[$companyId];
			else
				$data = null;
		}


		return $this->ToCSV($data, $this->fields);
	}
	/**
	 *
	 * @param $userId
	 */
	function __getConfigs($userId)
	{
		$fields = $this->fields;
		$configs = $this->find('all', array(
			'conditions' => array('user_id' => $userId),
			'fields' =>$fields
		));
		if(sizeof($configs) == 0)
		{
			return null;
		}
		else
		{
			return $configs;
		}
	}


}
