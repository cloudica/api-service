<?php
require_once('ClientDataModel.php');
class UserMachines extends ClientDataModel
{
	var $name = "UserMachines";
	var $useTable = "user_machines";
	var $primaryKey = "id";


	var $automateFields = array("company_id", "user_id", "last_updated", "created", "created_by", "updated_by");

	var $fields = array(
		"id",
		"os",
		"memory",
		"mac_address",
		"hd_size",
		"ip_internal",
		"login_username",
		"machine_name",
		"revision_number",
		"monitor_count",
		"localdb_revision_number",
		"configdb_revision_number",
		"maindb_revision_number",
		"company_id"
		);
	var $insertFields = array(
		"os",
		"memory",
		"mac_address",
		"hd_size",
		"ip_internal",
		"login_username",
		"machine_name",
		"revision_number",
		"monitor_count",
		"localdb_revision_number",
		"configdb_revision_number",
		"maindb_revision_number"
		);

	var $uniqueKeys = array(
		"os",
		"memory",
		"hd_size",
		"login_username",
		"machine_name",
		"user_id",
		"company_id"
		);
	var $machineKey ;
	var $machineId;

	var $saveLatestData = true;

	function __getMachineIdFromSynchData()
	{
		if(empty($this->data))
		{
			if(
				!isset($this->session) ||
				!isset($this->synchData)||
				!isset($this->synchData->rows)||
				!isset($this->synchData->rows[0])
			)
			{
				return 0;
			}

			$values = ($this->parseCSV($this->synchData->rows[0]));
			$data = $this->parseSingleArray($values[0], $this->fields);
			$data["user_id"] = $this->session["user_id"];
			$data["company_id"] = $this->session["company_id"];

			if($data["id"] == 0) unset($data["id"]);

			$this->data = $data;
		}


		$uniqueKeys = array_combine($this->uniqueKeys, $this->uniqueKeys);
		$uniqueKeys =array_intersect_key($this->data, $uniqueKeys);


	 	$id = $this->field("id", $uniqueKeys);

		if(isset($data))
		{
			$data["id"] = $id;
			//update rows
			$this->synchData->rows[0] = $this->ToCSV(array(array($this->alias=>$data)), $this->insertFields);
		}

		return $id;
	}

	function SaveSynchData()
	{
		$machineID = $this->__getMachineIdFromSynchData();

		if($machineID)
		{
			$this->data["id"] = $machineID;
			$this->id = $machineID;
		}

		$this->_data = $this->data;
		return parent::SaveSynchData(true);
	}

	function __updateRow(&$row, $index)
	{
		$today = isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");
		$creator = "API Service";
		$userId = $this->session["user_id"];
		$companyId = $this->session["company_id"];
		$row .= ",\"$companyId\", \"$userId\", \"$today\", \"$today\", \"$creator\", \"$creator\"";
	}

	function GetOtherMachineData($parameterArray)
	{
		if(sizeof($parameterArray) == 3)
		{
			$userId = $parameterArray[0];
			$companyId = $parameterArray[1];
			$machineId = $parameterArray[2];

			$data = $this->find("all", array("conditions"=>array("user_id"=>$userId, "company_id"=> $companyId, "id !="=>$machineId)));

			return $this->ToCSV($data);
		}
	}
}

/** END OF FILE **/