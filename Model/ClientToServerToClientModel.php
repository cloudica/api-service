<?php
/**
 * @copyright     Copyright 2012
 * @link          http://cloudica.com
 * @author		  Lorelie Dazo
 * @version 	  Cloudica1.0
 * @package
 * @since         Cloudica1.0
 * @license
 *
 */

require_once("ClientDataModel.php");
class ClientToServerToClientModel extends ClientDataModel
{

	var $virtualFields = array(
		"is_synch" => "CONCAT('Synched')",
		"session_id" =>  "CONCAT('0')"
	);


	//automate the return fields expected by client
	var $extraReturnFields = array(
		"is_synch",
		"session_id"
	);

	var $findVariables = array();


	/**
	 * Rules:
	 * If synching is successful return the data greater than last_synched
	 */
	function ProcessSynchData()
	{
		$newerData = $this->GetNewerData();
		$success=   $this->SaveSynchData();

		if($success)
		{
			//get data before synching
			$returnCSV = "";
			if(sizeof($newerData) > 0)
			{
				$returnCSV = $this->ToCSV($newerData, $this->GetReturnFields());
				$this->synchData->csvString = $returnCSV;
			}
		}
	}

	function GetReturnFields()
	{
		return isset($this->returnFields) ? $this->returnFields :  array_merge($this->extraReturnFields, $this->fields);
	}


	function GetNewerData()
	{
		if(isset($this->synchData))
		{
			$variables = $this->findVariables;

			$variables["fields"] = $this->GetReturnFields();
			$conditions = $this->GetNewerDataWhereClause();

			if(isset($variables["conditions"]))
			{
 				$variables["conditions"] = array_merge($variables["conditions"], $conditions);
			}
			else
			{
				$variables["conditions"] =$conditions;
			}

			$data = $this->find("all", $variables);


			return $data;
		}
	}


	/*
	 *
	 * return all data of the user less than last_synched
	 */
	function GetNewerDataWhereClause($automateLastSynched = true)
	{
		if(!isset($this->synchData)) return array();
		$companyIdField = "company_id";
		$userIdField = "user_id";
		$lastUpdatedField = "last_updated >";


		if(sizeof($this->belongsTo))
		{
			$companyIdField = $this->alias.".".$companyIdField;
			$userIdField = $this->alias.".".$userIdField;
			$lastUpdatedField = $this->alias.".".$lastUpdatedField;
		}

		$conditions = array(
			$companyIdField => $this->session['company_id'],
			$userIdField => $this->session['user_id']
		);
		if(strtotime($this->synchData->lastSynched))
		{

			$conditions[$lastUpdatedField] = $this->synchData->lastSynched;
		}
		return $conditions;
	}
}

/** END OF FILE **/