<?php
require_once("ClientToServerToClientModel.php");
class SynchDataModel extends ClientToServerToClientModel
{
	var $name = "SynchDataModel";

	var $useTable = "synch_data";


	var $uniqueKeys = array(
		"request_id",
		"synch_data"
	);

}
