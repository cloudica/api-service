<?php
require_once("ClientToServerToClientModel.php");
class SynchRequest extends ClientToServerToClientModel
{
	var $name = "SynchRequest";

	var $useTable = "synch_requests";

	var $fields = array(
		"company_id",
		"id",
		"machine_id",
		"status",
		"synch_date"
	);
	var $insertFields = array(
		"user_id",
		"company_id",
		"machine_id",
		"local_id",
		"status",
		"synch_date"
	);

	var $uniqueKeys = array(
		"user_id",
		"company_id",
		"machine_id",
		"local_id",
	);

}
