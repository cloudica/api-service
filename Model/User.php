<?php

class User extends AppModel
{

	var $name = "User";
	var $primaryKey = "user_id";
	var $useTable = "users";
	var $isMinion = false;

	var $fields = array(
		"user_id", "user_email", "user_password", "timezone", "display_name", "firstname", "lastname","offset_time","company_id"
	);



}

/*** END OF FILE **/