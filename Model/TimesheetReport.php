<?php
/**
 * @author Romeo Loyola
 */
require_once("ClientToServerToClientModel.php");
class TimesheetReport extends ClientToServerToClientModel
{
	var $name ="TimesheetReport";
	var $useTable = "timesheet_report";

	var $findVariables = array(
		"conditions"=>array("parent_id" => "0")
	);

	var $fields = array
	(
		"is_compromised",
		"is_calibrated",
		"id",
		"parent_id",
		"company_id",
		"is_deleted",
		"task_id",
		"details",
		"machine_starttime",
		"machine_endtime",
		"start_time",
		"end_time",
		"work_time",
		"timesheet_id",
		"shift_id" //just in case no timesheet but has shift
	);
	var $uniqueKeys = array(
		"user_id",
		"company_id",
		"task_id",
		"start_time",
		"timesheet_id",
		"parent_id"
	);

	var $saveLatestData = true;
	var $latestDataFields = array(
		"last_report" => array("field"=>"details", "transform"=>"getReport"),
		"last_task_local_time" => "start_time",
		"last_task" =>  array("field"=>"task_id", "transform"=>"getTask")
	);

	function getTask($args)
	{
		if(!isset($this->Task))
		{
			App::import("Model", "WorkTask");
			$this->Task = new WorkTask();
		}

		$k = $this->Task->read("title", $args);
		if(isset($k[$this->Task->alias]) && isset($k[$this->Task->alias]["title"]))
		{
			$k = $k[$this->Task->alias]["title"];
			return base64_encode($k);
		}

		return $args;
	}
	function getReport($args)
	{
		$args = urldecode($args);

		return base64_encode($args);

	}

	var $validate = array();
}