<?php
class Account extends AppModel
{
	var $name = 'Account';
	var $useTable = "accounts";

	var $fields = array(
		"Account.company_id",  "Company.company_name", "package", "validity", "Company.company_domain", "Account.employee_id"
	);
	var $returnFields = array(
		"company_id", "package", "validity",  "company_name",  "schedule_id", "schedule_name", "schedule_type", "current"
	);

	var $belongsTo = array(
		'Company' => array(
			'className' => 'Company',
			'foreignKey' => 'company_id',
			'fields' => 'company_name, company_domain',
			'conditions' => 'Company.is_deleted=0'
		)
	);

	function afterFind($results)
	{
		foreach($results as $key=> &$value)
		{
			if(isset($value[$this->Company->alias]))
			{
				if(isset($value[$this->Company->alias]["company_name"]))
				$value[$this->alias]["company_name"] = $value[$this->Company->alias]["company_name"];

				if(isset($value[$this->Company->alias]["company_domain"]))
				$value[$this->alias]["company_domain"] = $value[$this->Company->alias]["company_domain"];
			}

			$value[$this->alias]["license"] = 'Freemium';
			$value[$this->alias]["validity"] = '0001-01-01';

		}

		return $results;
	}

}