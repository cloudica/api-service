<?php
class Report
{ 
	public $Source;
	public $Message;
	public $StackTrace;
	public $Crashed;
	public $MacAddress;
	public $IpInternal;
	public $Email;
	public $Comments;
	public $UpdateReporter;
	public $ClientBuild;
	public $BugReporterBuild; 
	public $Timestamp;
	
	function __construct()
	{
		$this->Email = "";
		$this->Source = "";
		$this->Message = "";
		$this->StackTrace = "";
		$this->Crashed = false;
		$this->MacAddress ="";
		$this->IpInternal = "";
		$this->Comments = "";
		$this->UpdateReporter = false;
		$this->ClientBuild = "";
		$this->BugReportedBuild = "";
		$this->Timestamp = date ("Y-m-d H:i:s");
	}
	 
	 
	public static function Deserialize(SimpleXmlElement $simpleXmlElement, $timestamp)
	{
		 $report = new Report();
		
		$elements = $simpleXmlElement->children();
		foreach($elements as $key=>$value)
		{
			$value = (string)  $value;
			$report->$key = trim($value);
		}
		$report->Timestamp = $timestamp; 
		
		if($report->Crashed == "false") $report->Crashed = 0;
		else $report->Crashed = 1;
		
		if($report->UpdateReporter == "false") $report->Crashed = 0;
		else $report->UpdateReporter = 1;
		
		return $report;
	} 
	
}
/** END OF FILE **/