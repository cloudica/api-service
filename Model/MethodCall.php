<?php
require_once("MachineBasedClientData.php");
class MethodCall extends MachineBasedClientData
{
	var $name = "MethodCall";
	var $useTable = "method_calls_1";

	var $fields = array(
				"machine_id",
				"company_id",
				"is_compromised",
				"is_calibrated",
				"local_time",
				"machine_time",
				"log_status",
				"user_status",
				"user_status_message",
				"data_captured",
				"reference_id"
	);

	var $saveLatestData = true;
	var $LastMethodCall;


	var $virtualFields = array(
		"is_synch" => "CONCAT('Synched')", // synch status automate
		"session_id" =>  "CONCAT('0')", //no session id
		"log_status_0" => "CONCAT('0')" //force status to be 0 again
	);

	//automate the return fields expected by client
	var $returnFields = array(
		"is_synch",
		"session_id",
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"log_status_0",
		"user_status",
		"user_status_message",
		"data_captured",
		"reference_id"
	);


	var $uniqueKeys = array(
		"user_id",
		"company_id",
		"reference_id"
	);



	function __saveLastData($lastData)
	{
		try
		{
			 $query = array("table"=>"clm_last_method_call_1",
			 	"fields" => implode(",", array_keys($lastData)),
			 	"values" => '"'. implode('","', array_values($lastData)) . '"',
			 	"updateFields" => implode(",", array(
					"machine_time=VALUES(machine_time)",
					"local_time=VALUES(local_time)",
					"log_status=VALUES(log_status)",
					"user_status=VALUES(user_status)",
					"data_captured=VALUES(data_captured)",
					"last_updated=VALUES(last_updated)",
					"updated_by=VALUES(updated_by)",
					"user_status_message=VALUES(user_status_message)",
				))
			 );
			 $sql = $this->renderStatement("insert_on_update", $query);
			 $this->query($sql);
		 }
		 catch(Exception $e)
		 {
		 	debug($e->getMessage());
		 }
	}


}