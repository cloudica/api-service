<?php
require_once("ClientDataModel.php");
class ProcessData extends ClientDataModel
{
	var $name ="ProcessData";
	var $useTable = "process_data";

	var $fields = array(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"machine_time",
		"process_name"
	);

	var $uniqueKeys = array(
		"machine_id",
		"company_id",
		"local_time",
		"process_name",
		"user_id"
	);
}