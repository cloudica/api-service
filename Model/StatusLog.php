<?php
require_once("MachineBasedClientData.php");
class StatusLog extends MachineBasedClientData
{
	var $name ="StatusLog";
	var $useTable = "status_logs_1";

	var $fields = array(
		"machine_id",
		"company_id",
		"is_compromised",
		"is_calibrated",
		"local_time",
		"start_time",
		"end_time",
		"machine_start_time",
		"machine_end_time",
		"status",
		"status_message",
		"work_time",
		"temp_machine_end_time",
		"temp_end_time"
	);



	var $uniqueKeys = array(
		"company_id",
		"user_id",
		"machine_id",
		"local_time",
		"start_time"
	);


}
/** END OF FILE **/