<?php
require_once("ClientDataModel.php");
class Employee extends ClientDataModel
{
	var $name = "Employee";

	var $useTable = "employee_profile";


	var $belongsTo = array(
		'Schedule' => array(
			'className' => 'Schedule',
			'foreignKey' => 'schedule_id'
		)
	);


}
