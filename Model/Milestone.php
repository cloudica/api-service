<?php
require_once("ClientDataModel.php");
class Milestone  extends ClientDataModel //always servertoclient
{
	var $name = "Milestone";

	var $useTable = "milestones";

	var $fields = array(
		"id",
		"description",
		"due_date",
		"start_date",
		"assigned_by",
		"completed_tasks",
		"ongoing_tasks"
	);

}
/** END OF FILE **/