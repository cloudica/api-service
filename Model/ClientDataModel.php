<?php
/**
 * @copyright     Copyright 2012
 * @link          http://cloudica.com
 * @author		  Lorelie Dazo
 * @version 	  Cloudica1.0
 * @package
 * @since         Cloudica1.0
 * @license
 *
 */

App::uses('AppModel', 'Model');

class ClientDataModel extends AppModel
{
	var $saveLatestData = false;

	var $automateFields = array("user_id", "last_updated", "created", "created_by", "updated_by");

	//sets the session of the model using the user session
	var $session;

   public function __construct($id = false, $table = null, $ds = null)
   {
		if(isset($_SESSION["Cloudica"]))
		{
			App::import("Model", "Minion");
			$minion = Minion::getDatabase($_SESSION["Cloudica"]);
			$ds = $minion->name;

		}

		parent::__construct($id, $table, $ds);

		if(isset($_SESSION["Cloudica"])  )
		{
			//just in case it was not enforced;
			$this->useDbConfig = $ds;
		}

	}


	/**
	 * Auto filling user_id and company_id
	 */
	function beforeSave()
	{
		$now = isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");

		parent::beforeSave();
		if(isset($this->session["user_id"]) && !isset($this->data[$this->alias]["user_id"]))
		{
			$this->data[$this->alias]["user_id"] = $this->session["user_id"];
		}

		if(isset($this->session["company_id"]) && !isset($this->data[$this->alias]["company_id"]))
		{
			$this->data[$this->alias]["company_id"] = $this->session["company_id"];
		}

		return true;
	}


	function __updateRow(&$row)
	{
		$today = isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");
		$creator = "API Service";
		$automatedFields = array(
			"user_id" => $this->session["user_id"],
			"last_updated" =>$today,
			"created" => $today,
			"updated_by" => $creator,
			"created_by" =>$creator,
		);
		$row .= $append = ', "'. implode('","', array_values($automatedFields)).'"';
	}

	function __automateFields(){
		if(!isset($this->synchData) && $this->session)
		{
			return;
		}

		if(isset($this->synchData->rows))
		{
			foreach($this->synchData->rows as $index => &$row)
			{
				$this->__updateRow($row, $index);
			}
		}
	}

	static function GetUniqueFieldsStatement($uniqueKeys, $fields, $pkey = "id")
	{
		if($uniqueKeys > 0)
		{
			$uniqueKeys[] = $pkey;
			$updateFields = array_diff($fields, $uniqueKeys);

			foreach($updateFields as &$field)
			{
				$field = array("$field", "VALUES($field)");
				$field = implode("=", $field);
			}
			$updateFields = implode(",", $updateFields);
			return $updateFields;
		}
	}

	function __getUniqueFieldsStatement($fields)
	{
		return self::GetUniqueFieldsStatement($this->uniqueKeys, $fields, $this->primaryKey);
	}

	function __saveSynchData($fields, $values,$returnId=false)
	{
		if(sizeof($values) == 0) return true;

		$success = false;
		//custom is insert

		//auto include fields
		$fields = array_merge($fields, $this->automateFields);


		$data = array(
			"table"=>$this->tableName,
			"fields" => implode(",", $fields),
			"values" =>   implode("),(", $values)
		);

		$routine = "insert_all";

		if(sizeof($this->uniqueKeys) > 0)
		{
			$data["updateFields"]=$this->__getUniqueFieldsStatement($fields);
			$routine = "insert_on_update";
		}

		$sql = $this->renderStatement($routine, $data);

		if(empty($sql))
		{
			return false;
		}

		$dataSource = $this->getDataSource();
		$dataSource->begin($this);

		try
		{
			$success = $this->query($sql);
			if(is_array($success))
			{
				$success = true;
				if(sizeof($values) == 1 && $returnId)
				{
					$id = $this->query("SELECT LAST_INSERT_ID() FROM {$this->tableName}") ;
					if(is_array($id))
					{
						$id = array_values((Set::flatten($id)));
						if(isset($id[0]))
						{
							$this->id = (int) $id[0];
						}
					}
					if(!$this->id)
					{
						$this->__getIdFromSaveSynchData($values, $fields);
					}
				}
			}

			if($success ) $dataSource->commit($this);
			else $dataSource->rollback($this);
		}
		catch(Exception $e)
		{
			if(isset($sql))
			{
				debug($sql);
			}
			debug($this->name . ": " . $e->getMessage()); //usually for catching duplicates
			$dataSource->rollback($this);
		}
		return $success;
	}

	function __getIdFromSaveSynchData($values, $fields)
	{
		$data = $this->parseSingleArray(str_getcsv($values[0]), $fields, false);
		unset($data["last_updated"]);
		unset($data["created"]);

		$this->id = $this->field($this->primaryKey, $data);
	}

	/*
	 * last data is the last row in synchData
	 */
	function __getLastData()
	{
		if(isset($this->synchData) && isset($this->synchData->rows))
		{
			$rows = sizeof($this->synchData->rows);
			$lastIndex = $rows - 1;
			if($lastIndex >= 0 && isset($this->synchData->rows[$lastIndex]))
			{
				$lastRow = $this->synchData->rows[$lastIndex];

				$lastData = $this->parseSingleArray(str_getcsv($lastRow), $this->insertFields ? $this->insertFields : $this->fields);

				return $lastData;
			}
		}
	}

	/*
	 * last data is the last row in synchData
	 */
	function __saveLastData($lastData)
	{
		try
		{
			if(sizeof($lastData) > 0 && isset($this->latestDataFields) && sizeof($this->latestDataFields) > 0)
			{
				$data = array(); $updateFields = array();
				foreach($this->latestDataFields as $key=>$field)
				{
					$_field = ""; $_value = "";
					if(is_array($field))
					{
						if(isset($field["field"]) && isset($field["transform"]))
						{
							$_field = $field["field"];
							if(method_exists($this, $field["transform"]))
							{
								$_value = call_user_func(array($this, $field["transform"]), isset($lastData[$_field]) ? $lastData[$_field] : $lastData);
							}
						}
					}
					else
					{
						 if(isset($lastData[$field]))
						 {
						 	$_value = $lastData[$field];
						 }
					}

					$data[$key] = $_value;
					$updateFields[] = "$key=VALUES($key)";
				}
				if(sizeof($data) && $this->session)
				{
					$data["user_id"] = $this->session["user_id"];
					$data["company_id"] = $this->session["company_id"];
					$data["machine_id"] = $this->session["machine_id"];
					//update here
					$latestActivity = $this->renderStatement("insert_on_update", array(
						"table"=>"clm_last_activity",
						"fields" => implode(",", array_keys($data)),
						"values" => '"'.implode('","', array_values($data)).'"',
						"updateFields" => implode(",", $updateFields)
					));


					if(($latestActivity))
					{
						$this->query($latestActivity);
					}

				}
			}
		}
		catch(Exception $e)
		{
			debug($e->getMessage());
		}
	}

	function ProcessSynchData($returnId = false)
	{
		$success =  $this->SaveSynchData($returnId);
		return $success;
	}

	function SaveSynchData($returnId = false)
	{

		if(!isset($this->synchData))
		{
			return false;
		}
		$this->__automateFields();

		$fields = isset($this->insertFields) ? $this->insertFields :  $this->fields ;

		if(empty($this->synchData->csvString))
		{
			$success = true;
		}
		else
		{
			$success = $this->__saveSynchData($fields, $this->synchData->rows, $returnId);

			if($this->saveLatestData)
			{
				$this->__saveLastData($this->__getLastData());
			}
		}

		$this->synchData->success = $success;

		if($success)
		{
			$this->synchData->lastSynched =  isset($this->Now) ? $this->Now : date("Y-m-d H:i:s");
		}

		unset($this->synchData->rows);
		$this->synchData->csvString = ""; // no return

		return $success;
	}


}

/** END OF FILE **/