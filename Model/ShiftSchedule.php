<?php
require_once("ClientDataModel.php");
class ShiftSchedule extends ClientDataModel
{
	var $name = "ShiftSchedule";

	var $useTable = "schedules";
	var $alias = "D";

	var $isBound = true;
	var $scheduleFields = array(
		"A.company_id",
		"B.id",
		"B.schedule_type"
	);
	var $scheduleJoins = array(
		"clm_employee_profile as A",
		"JOIN clm_schedules as B",
		"ON A.schedule_id = B.id"
	);
	var $shiftFields = array(
		"C.schedule_id",
		"D.company_id",
		"D.id",
		"D.start_day",
		"D.start_time",
		"D.allowed_break_time",
		"D.required_hours"
	);
	var $flexibleShiftFields = array(
		"C.schedule_id",
		"D.company_id",
		"D.id",
		"D.start_day"
	);
	var $shiftJoins = array(
		"clm_shift_schedules as C",
		"JOIN clm_shifts as D",
		"ON C.shift_id = D.id"
	);

	var $fields = array(
		"id",
		"start_day",
		"start_time",
		"allowed_break_time",
		"required_hours"
	);
	var $scheduleType = "Flexible";
 	var $scheduleId = 0;

	function returnResponse($userId, $companyId)
	{
		if(!isset($this->account)
		|| !isset($this->account['Account']))
			return "";

		//$sid = (int) $this->account['Account']['schedule_id'];
		//$stype = $this->account['Account']['schedule_type'];

		$employeeId = (int) $this->account['Account']['employee_id'];
		if($employeeId==0) return "";

		return $this->getEmployeeSchedule($employeeId);
	}

	function getEmployeeSchedule($employeeId, $toCSV = true)
	{
		if(is_array($employeeId)) $employeeId = $employeeId[0];
		$sid = $this->query("SELECT schedule_id FROM clm_employee_profile as A WHERE  id = " . (int) $employeeId);
		$sid = isset($sid[0]) && isset($sid[0]["A"]) && isset($sid[0]["A"]["schedule_id"]) ? $sid[0]["A"]["schedule_id"] : 0;

		return $this->getSchedule($sid, "", $toCSV);
	}

	function getScheduleDetailsEmployeeId($employeeId,$toCSV = true){

		if(is_array($employeeId)) $employeeId = $employeeId[0];

		$schedule = $this->query("SELECT B.id, B.schedule_type, timezone FROM clm_employee_profile AS A JOIN clm_schedules as B ON  A.schedule_id = B.id WHERE A.id = " . (int) $employeeId);

		if(isset($schedule[0]) && isset($schedule[0]['B']))
		{
			$schedule = $schedule[0]['B'];
		}
		else $schedule = array();

		return $schedule;
	}

	function getSchedule($scheduleId = 0, $scheduleType = "Fixed", $toCSV = true)
	{
		if(is_array($scheduleId))
		{
			if(isset($scheduleId[1])) $scheduleType = $scheduleId[1];
			$scheduleId = $scheduleId[0];
		}

		$this->scheduleId = $scheduleId;
		$this->scheduleType = $scheduleType;

		$shifts = $this->getShifts($this->scheduleId, $this->scheduleType);

		if(!$toCSV) return $shifts;

		$csv = $this->ToCSV($shifts, $this->scheduleType == "Flexible" ? $this->flexibleShiftFields : $this->shiftFields);

		return $csv;
	}

	function getShifts($scheduleId, $scheduleType)
	{
		//construct fetching of schedule here
		$query = array(
			"fields" => implode(",", $scheduleType == "Flexible" ? $this->flexibleShiftFields : $this->shiftFields),
			"joins" => implode(" ", $this->shiftJoins),
			"conditions" =>  "C.schedule_id = $scheduleId"
		);
		extract($query);

		$scheduleData = array();

		$sql = "SELECT {$fields} FROM {$joins} WHERE {$conditions}  ";

 		$data = $this->query($sql);



		return $data;
	}



}
