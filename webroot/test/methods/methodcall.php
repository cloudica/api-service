<style>
	* {font-family: Arial, Sans}
	div:not(#OTHERS) {
		border: 1px solid #bbb;
		padding: 10px;
		margin: 20px;
		display: table;
		width: 90%;
	}

	input,textarea { width: 90%; display: block; float: right; border: 1px solid #ddd; padding: 6px; border-radius: 5px; }
	button {background: #0186A7; color: white; font-weight: bold; border: 1px solid #016780; padding: 6px; border-radius: 5px;}
</style>
<script type="text/javascript" src="jquery-1.11.3.min.js"></script>
<form method="post" action="methodcall.php">
	<div>
		<input type="text" name="url" value="" /> URL
	</div>
    <div>
        <input type="file" id="filedata"/> file
    </div>
	<div>
		<button type="button" onclick="send(event)">Submit</button>
	</div>
	<div>
		<button type="button" onclick="clearResult(event)">Clear</button>
	</div>

</form>
<hr />
<div id="result"></div>
<script>
function clearResult(e) {
	$("#result").html("");
}
function HtmlEncode(s)
{
  var el = document.createElement("div");
  el.innerText = el.textContent = s;
  s = el.innerHTML;
  return s;
}

function send(e) {
	e.preventDefault();

	var url = $("[name='url']").val();
	//var data =  new FormData();

    var $filedata = $('#filedata');
    var file = $filedata[0].files[0];
    var fr = new FileReader();

    fr.onload = function () {
        $.ajax({
          url: document.location.origin + "/" + url,
          contentType: "text/xml",
          dataType: "text",
          method: 'POST',
          headers: ['isTest: true'],
          data: fr.result,
          success: function (response) {
            $("#result").html(response);
            $("#result").append("<hr />");
            $("#result").append("<pre>"  + HtmlEncode( response ) + "</pre>");
          },
          error: function (response, error, message) {
            $("#result").html(response.responseText);
          }
        });
    };

    fr.readAsText(file);
}
</script>
