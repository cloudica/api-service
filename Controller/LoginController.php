<?php
/**
 * Removes the old session entry from database if exists for keys: user_id, company_id and machine_id
 * Creates a new session for unique key:  user_id, company_id and machine_id
 * Updates machine information
 *
 * @author Lorelie Dazo
 * @since 2012-04-20
 * revision 2012-07-02
 *
 *
 */

class LoginController extends AppController
{
	public $components = array('Crypt');

	var $machineId = 0;
	var $offsetTime;
	var $machineIds;
	var $revisionNumber;
	var $name = "GetToken";

	function __methodCall()
	{
		if(!$this->__inPostData("MACHINEID"))
		{

			if(!$this->__saveMachine())
			{
				$this->errorCode = 1001;
				$this->error = true;
				$this->responseData->EXTRA = "No MachineId";
				return;
			}
			{
				$this->machineId = (int) $this->responseData->MACHINEID;
			}
		}
		else
		{

			$this->machineId = (int) $this->requestData->MACHINEID;
			$this->responseData->MACHINEID = $this->machineId;
		}

		//check user credentials
		if(! $this->__checkUserCredentials()) return;

		if($this->error) return;
		$this->responseData->TOKEN = $this->__genToken();
		$this->__saveMachineInfo();

		$this->__identifyRequest();

		if(!isset($this->responseData->TOKEN))
		{
			$this->errorCode =8002;
			$this->error = true;
			return;
		}
		if(empty($this->responseData->PACKAGE))
		{
			$this->errorCode = 8027;
			$this->error = true;
			return;
		}
		if($this->error) return;

		if($this->session != null)
		{
			$_SESSION["Cloudica"] = $this->session;
			//login to websocket
			Cache::write($this->session["session_id"], $this->session["company_id"] . ":". $this->session["user_id"] . ":". $this->user->user_email);

			$this->__getChangedData();
			$this->__processSynchMethod();
		}
	}

	function __checkUserCredentials()
	{
		if(	!$this->__inPostData("COMPANYID")
			|| !$this->__inPostData("USERNAME")
			|| !$this->__inPostData("PASSWORD")
		)
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = " LOGIN DETAILS";
			return false;
		}

		$this->__importModel("User");
		$user = $this->User->findByUserEmail($this->requestData->USERNAME);

		if(!isset($user[$this->User->alias]))
		{
			$this->error = true;
			$this->errorCode = 8001;
			return false;
		}

		$this->user = (object) $user[$this->User->alias];

		//update the timezone
		if(!$this->user->timezone)
		{
			$user[$this->User->alias]["timezone"] = $this->user->timezone = (string) $this->requestData->TIMEZONE;
			$this->User->id = $this->user->user_id;
			$this->User->saveField("timezone", $this->user->timezone);
		}

		//make sure
		if(!$this->__isPasswordCorrect())
		{
			$this->error = true;
			$this->errorCode = 8001;
			return false;
		}

		$this->__importModel("Account");

		$account = $this->Account->findByUserIdAndCompanyId($this->user->user_id, (int) $this->requestData->COMPANYID);

		if(!isset($account[$this->Account->alias]) || $account[$this->Account->alias]['is_deleted'])
		{
			$this->error = true;
			$this->errorCode = 0005;
			return false;
		}
		$this->__getEmployeeDetails($account);

		$this->user->account = (object) $account[$this->Account->alias];
		$this->user->company = (object) $account[$this->Account->Company->alias];


		$this->__getUserResponse();



		return true;
	}

	function __getEmployeeDetails(&$account)
	{
		try
		{
			if($account["Account"]["employee_id"])
			{
				$this->session = array(
					"company_id" => $account["Account"]["company_id"],
					"company_domain" => $account["Account"]["company_domain"],
				);
				$this->Employee = null;
				$this->__importModel("Employee", "minion");
				$dbconfig = $this->Employee->useDbConfig;
				$this->Employee->Schedule->schemaName =
				$this->Employee->Schedule->useDbConfig = $this->Employee->useDbConfig;

				$employee = $this->Employee->read(null, $account["Account"]["employee_id"]);

				if($employee && isset($employee["Schedule"]))
				{
					$account["Account"]["schedule_id"] = $employee["Schedule"]["id"];
					$account["Account"]["schedule_name"] = $employee["Schedule"]["schedule_name"];
					$account["Account"]["schedule_type"] = $employee["Schedule"]["schedule_type"];
				}

			}
		}
		catch (Exception $e)
		{
			debug($e->getMessage());
			debug($e->getTrace());
		}
	}




	function __getChangedData()
	{
		try
		{
			//check for changes
			if(!$this->Changes)
			{
				$this->__importModel("Changes", "minion");
			}
			$this->Changes->session = $this->session;
			$clientChanges = $this->Changes->parseSingleArray( str_getcsv( (string) $this->__getSynchDataByType("changes", false)), $this->Changes->fields);

			$changes = $this->Changes->find("first", array("conditions"=>array("user_id"=>$this->user->user_id, "company_id"=>$this->session['company_id']), "fields"=> array_merge(array("id"), $this->Changes->fields)));

			if(!$changes)
			{
				$changes =  array(
						"user_id" => $this->session["user_id"],
						"company_id" => $this->session["company_id"],
						"employee_id" => $this->session["employee_id"]
				) ;
				if($this->Changes->save($changes))
				{
					$changes["id"] = $this->Changes->getInsertID();
					$changes = array( $this->Changes->alias => $changes);
				}
				else $changes = null;
			}

			if($this->error) return;

			if($changes && isset($changes[$this->Changes->alias]))
			{

				$this->Changes->id = $changes[$this->Changes->alias]["id"];
				unset( $changes[$this->Changes->alias]["id"]);
				$newchanges = array();


				//$timezone =  $this->user->timezone;

				foreach($changes[$this->Changes->alias] as $key=>$value)
				{
					if(!isset($clientChanges[$key] ) || AppModel::IsMinValue($clientChanges[$key]))
						$client = 0;
					else
						$client = strtotime($clientChanges[$key]);

					$server = new DateTime($value);
					//$server->setTimezone(new DateTimeZone($timezone));

					$value = $server->format("Y-m-d H:i:s");

					if(AppModel::IsMinValue($value))
						$server = 0;
					else
						$server = strtotime($value);


					if($server > $client)
					{
						$changed = $this->Changes->getChangedData($key);

						if(isset($changed["data"]) && isset($changed["type"]))
						{
							$xml = $this->responseData->SYNCH->addChild("DATA", $changed["data"]);
							$xml->addAttribute("type", $changed["type"]);
							$xml->addAttribute("lastSynch", $value);
							$xml->addAttribute("success", true);
							$newchanges[$key] = $this->Now;

							try
							{
								//also return clm_schedules of employee profile if there are changes in shift
								if($changed["type"] =="shiftschedule"){

									$this->__importModel("Schedule");

									if(isset($this->session["employee_id"]))
									{
										$schedule_csv = $this->Schedule->getScheduleFromEmployeeId($this->session["employee_id"]);
										$xml2 = $this->responseData->SYNCH->addChild("DATA", $schedule_csv);
										$xml2->addAttribute("type", "schedule");
										$xml2->addAttribute("lastSynch", $value);
										$xml2->addAttribute("success", true);
									}
								}
							}
							catch (Exception $er)
							{
								debug($er->getMessage());
							}
						}
					}
				}
			}
		}
		catch (Exception $e)
		{
			debug($e->getMessage());
		}

	}

	function __getUserResponse()
	{
		//displayname
		$this->responseData->DISPLAYNAME = $this->user->display_name;

		//timezone
		$this->responseData->TIMEZONE = $this->user->timezone;

		//offset
		$this->responseData->OFFSET = $this->user->offset_time;

		//license validity
		$this->responseData->VALIDTILL = $this->user->account->validity;

		//license packacge
		$this->responseData->PACKAGE = $this->user->account->package;

		$this->responseData->SCHEDULE_ID = isset($this->user->account->schedule_id) ? (int) $this->user->account->schedule_id : 0;
		$this->responseData->SCHEDULE = isset($this->user->account->schedule_name) ? $this->user->account->schedule_name : "";
	}


	function __genToken()
	{
		if(!isset($this->ClientSession))
		{
			$this->ClientSession = $this->__importModel("ClientSession");
		}

		if($this->utc)
		{
			$expireTime = new DateTime($this->Now, new DateTimeZone("UTC"));
			$interval = new DateInterval("PT". (int) TOKEN_TIMEOUT ."S");
			$expireTime->add($interval);
			$expireTime->format('Y-m-d H:i:s');
		}
		else
		{
			$expireTime = $this->__getServerTime(TOKEN_TIMEOUT, true);
		}

		$sessionKey = $this->user->user_id.$this->user->account->company_id.$this->machineId;

		$token = md5(
			$this->Crypt->ccEncrypt(
				md5(microtime(). CC_KEY. $sessionKey)
			)
		);


		//update session
		$data = array(
				"user_id"=>$this->user->user_id,
				"company_id"=>$this->user->account->company_id,
				"session_id"=>$token,
				"machine_time"=>(string) $this->requestData->TIMESTAMP,
				"machine_id"=>$this->machineId,
				"local_time"=>(string) $this->requestData->LOCALTIME,
				"expire_time"=>$expireTime,
				"company_domain" => $this->user->company->company_domain,
				"revision_number" => $this->revisionNumber,
				"employee_id" => $this->user->account->employee_id,
		);
		$this->ClientSession->updateSesion($data);

		$this->session = $this->ClientSession->data[$this->ClientSession->alias];

		return $this->ClientSession->token;
	}

	/*
	 * just return current machineID
	 */
	function __saveMachineInfo()
	{
		//get the user data in each synch data
		$machineString = (string) $this->__getSynchDataByType("machine");


		if(!$machineString )
		{
			return;
		}


		 $this->UserMachines = $this->__importModel("UserMachines", "minion");

		$machineData =$this->UserMachines->parseSingleArray(str_getcsv(  $machineString), $this->UserMachines->fields, true);

		//add session data
		unset($machineData[$this->UserMachines->alias]["id"]);
		$machineData[$this->UserMachines->alias]["user_id"] = $this->session["user_id"];
		$machineData[$this->UserMachines->alias]["company_id"] = $this->session["company_id"];

		//add revisions
		$machineData[$this->UserMachines->alias]['maindb_revision_number'] = (string) $this->requestData->MAINDB_BN;
		$machineData[$this->UserMachines->alias]['configdb_revision_number'] = (string) $this->requestData->CONFIGDB_BN;
		$machineData[$this->UserMachines->alias]['localdb_revision_number'] = (string) $this->requestData->LOCALDB_BN;
		$machineData[$this->UserMachines->alias]['revision_number'] = (string) $this->requestData->BUILD;

		$machineData[$this->UserMachines->alias]["machine_id"] = $this->machineId;

		$data =   $machineData;
		$data = $data[$this->UserMachines->alias];

		$id = $this->UserMachines->find("first", array(
			"conditions" => $data
		));

		if(!$id)
		{
			//save new
			$this->UserMachines->create();
			$this->UserMachines->save($machineData);
		}

	}


	function __getUserTestFolder()
	{
		return (string) $this->requestData->USERNAME;
	}
}