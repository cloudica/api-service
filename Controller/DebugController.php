<?php
require_once("SynchController.php");
/**
 * Allows adding/updating dtr related data, different from synchronization
 *
 * @author Romeo Loyola
 * @since 	2014-04-02
 */

class DebugController extends AppController
{
	var $name = "DebugController";
	var $components = array("Gmail");

	function __methodCall()
	{
		$user = (string) $this->requestData->EMAIL;
		$companyId = (int) $this->requestData->COMPANY_ID;
		$machineId = (int) $this->requestData->MACHINE_ID;

		$User = $this->__importModel("User");

		$userId = (int) $User->field("user_id", array("user_email"=>$user));

		if(!$userId || !$machineId)
		{
			$this->errorCode  = 1001;
			$this->error = true;
			return;
		}


		$Account = $this->__importModel("Account");
		$accountId = $Account->field("id", array("user_id"=>$userId, "company_id"=>$companyId));


		if(!$accountId)
		{
			$this->errorCode  = 1001;
			$this->error = true;
			return;
		}
		$Company = $this->__importModel("Company");
		$domain = $Company->field("company_domain", array( "company_id"=>$companyId));


		if(!$domain)
		{
			$this->errorCode  = 1001;
			$this->error = true;
			return;
		}

		$this->session = array(
			"company_id" => $companyId,
			"user_id" => $userId,
			"company_domain" => $domain,
			"machine_id" => $machineId
		);



		if(isset($this->requestData->URL) || isset($this->requestData->IDS))
		{
			$url = (string) $this->requestData->URL;
			$ids = (string) $this->requestData->IDS;

			$ids = explode(",", $ids);
			if(sizeof($ids) > 0 && !empty($url))
			{
				$PerMachine = $this->DebugRequestsPerMachine = $this->__importModel("DebugRequestsPerMachine", true);
				foreach($ids as $id)
				{
					$data = array("request_id" => $id,
						"machine_id" => $machineId,
						"request_link" => $url
					);
					$PerMachine->create();
					$PerMachine->save($data);
				}
				try
				{

					//email the developers ("lorelie dazo")
					$this->Gmail->to = array('email' => "l.dazo@eversun.ph", "name"=>"Lorelie Dazo");
					$this->Gmail->subject = "Debug Request for " . $user . " has been uploaded to ftp server ";
					$this->Gmail->send("The url for download is as follows: " . $url);
				}
				catch(Exception $e)
				{
					//failed to email
				}

				return;
			}

			$this->errorCode  = 1001;
			$this->error = true;
			return;
		}




		$DebugRequests = $this->DebugRequests = $this->__importModel("DebugRequests", true);
		$PerMachine = $this->DebugRequestsPerMachine = $this->__importModel("DebugRequestsPerMachine", true);

		if(!isset($this->requestData->SYNCH))
		{
			//fetch new requests
			$machineRequests = $PerMachine->find("all", array("fields"=>array("request_id"), "conditions"=>array("machine_id" => $this->session["machine_id"])));
			$requestIds = array();

			$conditions = array("user_id" => $this->session["user_id"], "company_id"=>$this->session["company_id"]);

			if( sizeof($machineRequests) >  0)
			{
				$requestIds =array_keys(  Set::combine($machineRequests, "{n}.". $PerMachine->alias . ".request_id",  "{n}.". $PerMachine->alias . ".request_id" ));
				$conditions["id NOT"] = $requestIds;
			}

			$newRequests = $DebugRequests->find("all", array("conditions"=>$conditions));

			$return = $DebugRequests->toCSV($newRequests, $DebugRequests->fields);

			$data = new SynchData();
			$data->type = "debugrequests";
			$data->success = "1";
			$data->csvString = $return;

			$this->__addToResponseData(SynchData::Serialize($data));
		}
		else
		{

		}

	}

}
/** END OF FILE **/