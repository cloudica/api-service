<?php
/**
 * returns the current time based on timezone sent
 *
 * @author Lorelie Dazo
 * @since 	2013-06-19
 * revision
 */
class TimeController extends AppController
{

	var $name = "TimeController";
	var $time = "";

	function index()
	{
		$this->autoRender = false;

		//check for request
		if(isset($this->requestData->TIME) && isset($this->requestData->TIMEZONE))
		{
			$time = date("Y-m-d H:i:s");
			$localTime = new DateTime($time, new DateTimeZone(DEFAULT_TIMEZONE));
			$localTime->setTimezone(new DateTimeZone($this->requestData->TIMEZONE));
			echo  $localTime->format('Y-m-d H:i:s');
		}




	}

}

/** END FILE **/
