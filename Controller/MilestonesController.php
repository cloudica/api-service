<?php
/**
 * Allows MileStone fetch
 */

class MilestonesController extends AppController
{
	var $name = "MilestonesController";
	
	var $sessionData; 
	var $user;
	var $userMilestones;

	function __methodCall()
	{  
		//get token
		if(!$this->__inPostData("TOKEN"))
		{
			$this->error = true;
			$this->errorCode = 1001; 
			return;
		}
		
		if(!$this->__inPostData("UNCATEGORIZED"))
		{
			$this->error = true;
			$this->errorCode = 1001; 
			return;
		}
		
		//check actiontype first
		if(!$this->__inPostData("ACTIONTYPE"))
		{
		    $this->errorCode = 1001;
			$this->error = true;	 
			$this->responseData->EXTRA = "doesnot indicate ACTIONTYPE option";
			return;
		}
		
		$this->action = (string) $this->requestData->ACTIONTYPE;

		if(!$this->__inPostData("FETCHTYPE"))
		{
			$this->errorCode = 1001;
			$this->error = true;	 
			$this->responseData->EXTRA = "doesnot indicate FETCHTYPE option";
			return;
		}
		else 
			$this->Fetch = (string) $this->requestData->FETCHTYPE;
		
		$token = (string) $this->requestData->TOKEN;
		 
		$this->__initializeUser($token);
		
		if($this->error) return;
		
		if(!isset($this->Milestones))
			$this->Milestones = $this->__importModel("Milestones","minion");
		if(!isset($this->UsersMilestones))
			$this->UsersMilestones = $this->__importModel("UsersMilestones","minion");
		if(!isset($this->MUser))
			$this->MUser = $this->__importModel("MUser","minion");
		if(!isset($this->WorkTask))
			$this->WorkTask = $this->__importModel("WorkTask","minion");
					
	   if(!isset($this->userMilestones))
		{
			$this->userMilestones = $this->UsersMilestones->find("all", array("fields"=>$this->UsersMilestones->fields,	'conditions'=>array('user_id'=>$this->user->user_id, "company_id" => $this->user->account->company_id))); 
		}
		
		$this->__processSynchMethod();
		$this->__getFetchResponse();
	}
	
	function __getFetchResponse()
	{
		$milestone_ids = array();
		$milestone_creator_ids = array();
	    $milestone_creator_fields = array("user_id", "user_email","timezone", "display_name", "firstname", "lastname", "company_id"	);
		//get milestones
		foreach ($this->userMilestones as $value)
		{
			$milestone_ids[] = $value["UsersMilestones"]["milestone_id"];
		}

        $search = array();
		switch ($this->Fetch)
		{
		case "ALL":
		  $search[] = 1;
		  $search[] = 0;
		  break;
		case "COMPLETED":
		  $search[] = 1;
		  break;
		case "ONGOING":
		  $search[] = 0;
		  break;
		}

		$milestones = $this->Milestones->find('all', array( 'conditions' => array('id' => $milestone_ids,'completed' =>$search)));
		
		if($milestones==null) return;
		
		$milestone_ids = array();
		//get milestone creator and update $milestone_ids based on $search
		foreach ($milestones as $value)
		{
		    $milestone_creator_ids[] = $value["Milestones"]["assigned_by"];
			$milestone_ids[] =  $value["Milestones"]["id"];
		}
		
		$milestoneCreator = $this->MUser->find('all', array( 'fields'=>$milestone_creator_fields, 'conditions' => array('user_id' => $milestone_creator_ids)));
		
		$api_uncategorizedcount = $this->WorkTask->find('count', array('conditions' => array('milestone_id'=>0,'user_id' =>$this->user->user_id,"company_id" => $this->user->account->company_id)));
		$local_uncategorizedcount = (int) $this->requestData->UNCATEGORIZED;

		$this->responseData->UNCATEGORIZEDFROMLOCAL = $local_uncategorizedcount;
		$this->responseData->UNCATEGORIZEDFROMAPI =  $api_uncategorizedcount;
		
		if($local_uncategorizedcount!=$api_uncategorizedcount)
		{
			//get user's worktask with milestones or user's worktask without milestone
			$worktasks = $this->WorkTask->find
			('all', 
				array( 
					'conditions' =>	
					array(
						'user_id' =>$this->user->user_id,"company_id" => $this->user->account->company_id,
						'OR' => array(array('milestone_id'=>$milestone_ids),array('milestone_id' =>0))
						)
					)
			);
		}
		else
		{
			//get user's worktask with milestones only
			$worktasks = $this->WorkTask->find
			('all', 
				array( 'conditions' =>	array('user_id' =>$this->user->user_id,'milestone_id'=>$milestone_ids,"company_id" => $this->user->account->company_id,))
			);
		}
		
		$this->responseData->SYNCH = "";
		$name = $this->Milestones->name;
		$fields = array();

		array_walk_recursive($this->Milestones->fields, function($item, $key) use (&$fields, &$name) {$fields[] = "$name.$item";}); 
		//echo print_r($this->Milestones->fields,true) . print_r($milestones);
		if(sizeof($milestones) > 0)
     	{ 
			$userXML = $this->responseData->SYNCH->addChild("DATA", $this->Milestones->ToCSV($milestones , $fields)); 
			$userXML->addAttribute("type", "milestones");	
		}
			
		$name = $this->MUser->name;
		$fields = array();

		array_walk_recursive($milestone_creator_fields, function($item, $key) use (&$fields, &$name) {$fields[] = "$name.$item";}); 
		
		if(sizeof($milestoneCreator) > 0)
     	{ 
			$userXML = $this->responseData->SYNCH->addChild("DATA", $this->MUser->ToCSV($milestoneCreator , $fields)); 
			$userXML->addAttribute("type", "milestonescreator");
		}
		
		$name = $this->WorkTask->name;
		$fields = array();

		array_walk_recursive($this->WorkTask->fields, function($item, $key) use (&$fields, &$name) {$fields[] = "$name.$item";}); 
		if(sizeof($worktasks) > 0)
     	{ 
			$userXML = $this->responseData->SYNCH->addChild("DATA", $this->WorkTask->ToCSV($worktasks , $fields)); 
			$userXML->addAttribute("type", "worktask");
		}
	}
}