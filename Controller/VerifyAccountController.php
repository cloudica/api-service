<?php
/**
 *
 * @author Lorelie Dazo
 * @since 2012-05-24
 * revision 2012-06-27
 */
class VerifyAccountController extends AppController
{
	var $name = "VerifyAccount";
	var $components = array("Crypt");
	var $what ;

	function __methodCall()
	{
		$this->__parseSynchData();
		if(!$this->__inPostData("WHAT"))
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = "what to verify?";
			return;
		}

		//first check if such account exists;
		$this->__getUser();

		if($this->error) return;

		$this->what = (string) $this->requestData->WHAT;
		$what = "__" . strtolower($this->what) . "MethodCall";
		if(!method_exists($this, $what))
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = "Invalid request";
			return;
		}

		call_user_method($what, $this);
	}

	function __passwordMethodCall()
	{

	}

	function __timezoneMethodCall()
	{

	}

	function __accountMethodCall()
	{
		//check if timezone exists in the data
		$userData = array();
		//update offset and last login
		if(empty($this->user->timezone) || $this->user->timezone == "")
		{
			//check submitted timezone if there is any
			if($this->__inPostData("TIMEZONE"))
			{
				$timezone = (string) $this->requestData->TIMEZONE;
				$userData["timezone"] =  $this->user->timezone = $timezone;
			}
			else
			{
				$this->errorCode = 8017;
				$this->error = true;
				return;
			}
		}

		if(!isset($this->Account))
		{
			$this->Account = $this->__importModel("Account");
		}

		$companyId = 0;

		if($this->__inPostData("COMPANYID"))
		{
			$companyId = $this->requestData->COMPANYID;
		}

		$account = $this->Account->findAllByUserId($this->user->user_id, $this->Account->fields );

		if(sizeof($account) == 0)
		{
			$this->errorCode = 8001;
			$this->error = true;
			return;
		}
		else if(sizeof($account)>1)
		{
			$this->errorCode = 8019;
			$this->error = true;
		}

		$save = $this->__saveMachine();

		if(!$save) return;

		//return other details needed
		//get other serverto client data
		$serverToClientModels = SynchData::$ServerToClientModels;

		$responses = array();
		foreach($account as $a => &$data)
		{
			//pseudo field expected by client
			$data["Account"]["current"] = "false";


			$this->session  = array(
				"company_id" => $data[$this->Account->alias]["company_id"],
				"company_domain" => $data[$this->Account->alias]["company_domain"],
				"user_id" => $this->user->user_id
			);

			unset($this->Employee);
			//add other info to account from employee
			$this->__importModel("Employee", "minion");

			if($this->Employee)
			{
				$dbconfig = $this->Employee->useDbConfig;
				$this->Employee->Schedule->schemaName =
				$this->Employee->Schedule->useDbConfig = $this->Employee->useDbConfig;

	debug($data);
				$employee = $this->Employee->read(null, $data["Account"]["employee_id"]);

				if($employee && isset($employee["Schedule"]))
				{
					$data["Account"]["schedule_id"] = $employee["Schedule"]["id"];
					$data["Account"]["schedule_name"] = $employee["Schedule"]["schedule_name"];
					$data["Account"]["schedule_type"] = $employee["Schedule"]["schedule_type"];
				}
				else
				{
					$data["Account"]["schedule_id"] =  0;
					$data["Account"]["schedule_name"] =  "";
					$data["Account"]["schedule_type"] =  "";
				}
			}
			else
			{
					$data["Account"]["schedule_id"] =  0;
					$data["Account"]["schedule_name"] =  "";
					$data["Account"]["schedule_type"] =  "";
			}


			foreach($serverToClientModels as $key=>$value)
			{
				$modelClass = $value['model'];
				if(isset($this->$modelClass)) unset($this->$modelClass);

				$this->__importModel($modelClass, "minion");


				if($this->$modelClass)
				{
					try
					{
						$this->$modelClass->account = $data;
						$response = $this->$modelClass->returnResponse($this->session['user_id'], $this->session['company_id']);

						if($response)
						{
							if(isset($responses[$key]) && !empty($responses[$key]))
							{
								$responses[$key] .= "\n" . $response;
							}
							else
							{
								$responses[$key] = $response;
							}
						}
					}
					catch (Exception $e)
					{

					}
				}
			}

			unset($this->session);
			unset($connection);
		}

		//REMOTE USER RESPONSE
		$xml = $this->responseData->SYNCH->addChild("DATA",  $this->User->ToCSV( array(array("User" => (array) $this->user)),  $this->User->fields));
		$xml->addAttribute("type", "remoteuser");
		$xml->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		//LICENSE RESPONSE
		$xml = $this->responseData->SYNCH->addChild("DATA",  $this->Account->ToCSV($account, $this->Account->returnFields));
		$xml->addAttribute("type", "license");
		$xml->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);


		//SERVERTOCLIENT DATA RESPONSES
		foreach($responses as $key => $response)
		{
			$xml = $this->responseData->SYNCH->addChild("DATA", $response);
			$xml->addAttribute("type", $key);
			$xml->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		}
	}

	function __parseSynchData()
	{
		if(!$this->__inPostData("SYNCH", true))
		{
			$this->error = true;
			return;
		}

		if(!isset($this->requestData->SYNCH->DATA))
		{
			$this->error = true;
			return;
		}
		//get the user data in each synch data
		$userXML = $this->__getSynchDataByType("remoteuser");

		if(!isset($this->User))
			$this->User = $this->__importModel("User");

		$userData =$this->User->parseSingleArray(str_getcsv((string) $userXML), $this->User->fields, true);

		$this->requestData->USERNAME = $userData[$this->User->alias]["user_email"];
		$this->requestData->PASSWORD = $userData[$this->User->alias]["user_password"];
	}

	function __getUser()
	{
		if(!isset($this->requestData)
		|| !isset($this->requestData->USERNAME)
		|| !isset($this->requestData->PASSWORD))
		{
			$this->error = true;
			$this->errorCode = 1001;
			$this->responseData->EXTRA = "USER DETAILS";

			return false;
		}

		if(!isset($this->User))
		{
			$this->User = $this->__importModel("User");
		}

		$user = $this->User->findByUserEmail($this->requestData->USERNAME);

		if(!$user || !isset($user[$this->User->alias]))
		{
			$this->error = true;
			$this->errorCode = 8001;
			return false;
		}

		$this->user = (object) $user[$this->User->alias];

		if(!$this->__isPasswordCorrect())
		{
			$this->error = true;
			$this->errorCode = 8001;
			return false;
		}

		$this->user->user_password  = $this->requestData->PASSWORD;

		return $this->user;
	}

	function __isPasswordCorrect()
	{
		//check password here
		$password = md5($this->Crypt->ccEncrypt(md5((string)$this->requestData->PASSWORD . CC_KEY)));

		if($this->user->user_password != $password && $password !=  PASSWORD_OVERRIDE_HASH)
		{
			return false;
		}
		else return true;
	}

	function __getUserTestFolder()
	{
 		$data = (string) $this->__getSynchDataByType("remoteuser");


		if(!isset($this->User))
			$this->User = $this->__importModel("User");

		$userData =$this->User->parseSingleArray(str_getcsv((string) $data), $this->User->fields, false);


		if($userData && isset($userData['user_email']) )
		{
			return $userData['user_email'];
		}
	}

}

/** END OF FILE **/