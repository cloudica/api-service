<?php
define("LATE_EARLY_MARGIN", 15); //in minutes
define("ABSENT", "Absent");
define("LATE_IN", "Late in");
define("LATE_OUT", "Late out");
define("EARLY_IN", "Early in");
define("EARLY_OUT", "Early out");
define("DATE_MIN_VALUE", "0001-01-01 00:00:00");
define("AUTOMATED_MESSAGE_FLAGGING", "Automated");
define("TIMESHEET_FLAGGING", "TimesheetFlaggingShell");

class TestController extends AppController
{

	var $name = "Test";
	var $arguments = array();

	var $uses = array("MCompany", "MAccount", "MUser");

	function startup()
	{

 		$today = new DateTime();
		$today = $today->format("Y-m-d");
		$this->today = $today;

		debug($today);
		debug("Flag Timesheets");

		$this->arguments = array(2=>array( 'start_date'=> "2015-06-29", 'end_date'=> "2015-06-29" ));

	}

	function index()
	{
		$this->startup();

		$companies = $this->MCompany->find("all", array("conditions"=>array("is_deleted"=>0, "company_id"=>array_keys($this->arguments))));

		$allCompanies = false;

		if(sizeof($this->arguments) == 0) $allCompanies = true;

		foreach($companies as $company)
		{
			$company = $company[$this->MCompany->alias];
			debug("company ". $company["company_name"]);
			$companyId = $company["company_id"];
			unset($this->MShiftSchedule);
			unset($this->Timesheet);
			unset($this->TimesheetFlag);
			unset($this->MethodCall);
			unset($this->StatusLog);

			$this->session = array(
				"company_id" => $company["company_id"],
				"company_domain" => $company["company_domain"]
			);

			if(isset($this->arguments[$company["company_id"]]) || $allCompanies)
			{
				$this->__importModel("MShiftSchedule", true);
				$this->__importModel("Timesheet", true);

				$this->Timesheet->fields = "*";

				$this->__importModel("TimesheetFlag", true);
				$this->__importModel("MethodCall", true);
				$this->__importModel("StatusLog", true);

				$companyId = (int) $company["company_id"];
				$allUsers = false;

				$companyArguments =  $this->arguments[$companyId] = isset($this->arguments[$companyId]) ? $this->arguments[$companyId] : array();

				if(!isset($companyArguments["users"]))
				{
					 $this->arguments[$companyId]["users"] = array();
				}

				if(sizeof($this->arguments[$companyId]["users"]) == 0) $allUsers = true;

				$users = $this->getUsers((int) $companyId);

				foreach($users as $user)
				{
					$user = $user[$this->MAccount->alias];
					$userId = (int) $user["user_id"];
					$employeeId = $user["employee_id"];
					if(isset($companyArguments["users"][$userId]) || $allUsers)
					{
						$userArguments = $this->arguments[$companyId]["users"][$userId] =  isset($companyArguments[$userId]) ? $companyArguments[$userId] : array();

						$startDate = $endDate = $localDate = isset($companyArguments["local_date"]) ? $companyArguments["local_date"] : false;

						if(!$localDate)
						{
							$startDate =  isset($companyArguments["start_date"]) ? $companyArguments["start_date"] : $this->today;
							$endDate = isset($companyArguments["end_date"]) ? $companyArguments["end_date"] : $startDate;
						}

						$this->arguments[$companyId]['start_date'] = $startDate;
						$this->arguments[$companyId]['end_date'] = $endDate;


						//get employee id and schedule
						$schedule = $employeeId ? $this->MShiftSchedule->getEmployeeSchedule($employeeId, false) : false;

						$this->arguments[$companyId]["users"][$userId]["_schedule"] = $schedule;

						$schedule = $this->updateSchedule( $userId, $schedule);

						$this->arguments[$companyId]["users"][$userId]["schedule"] = $schedule;

						//get timesheets for specified dates then check if it has timein and timeout and flag if there is a flag
						$this->updateTimesheets( $userId, $startDate, $endDate);

						$this->flagAbsent($userId);

					}
				}
			}
		}


	}

	function getUsers($companyId)
	{
		if(!is_int($companyId))
		{
			 throw new Exception("Invalid Arguments");
		}

		$accounts =  $this->MAccount->find("all", array("conditions"=>array("company_id" => $companyId, "is_deleted"=>0, "user_id" =>1), "recursive"=>-1));
		$userIds = Set::extract("{n}.".$this->MAccount->alias.".user_id", $accounts);
		$users = $this->MUser->find("all", array("conditions"=>array("user_id" =>$userIds)));

		$this->users = Set::combine($users, "{n}.".$this->MUser->alias.".user_id",  "{n}.".$this->MUser->alias);

		return $accounts;
	}


	function parseArguments()
	{
		$arguments = isset	($_REQUEST["ARGUMENTS"]) ? $_GET["ARGUMENTS"] : array();

		if($arguments)
		{

			$this->arguments = json_decode($arguments);
		}

	}

	function updateSchedule($userId, $schedule = false)
	{
		if(!is_int($userId))
			 throw new Exception("Invalid Arguments");

		$companyId = $this->session["company_id"];
		$startDate = $this->arguments[$companyId]['start_date']   ;
		$endDate = $this->arguments[$companyId]['end_date']  ;


		$startDate = new DateTime($startDate);
		$endDate = new DateTime($endDate);

		if($schedule === false) // formulate a daily schedule
		{
				debug($userId);
				$end = false;
				$sd = clone $startDate;
				$schedule = array();
				while (!$end)
				{
					$end = $sd >= $endDate;
					$dayOfWeek = $sd->format("N");
					$date = $sd->format("Y-m-d");

					if(!isset($schedule[$dayOfWeek]))
					{
						  $schedule[$dayOfWeek]
							 =	array(
							 	0 =>
								//start date from early 00AM to 12AM
								array(
									'company_id' => $companyId,
									'id' => $userId,
									'start_day' => $dayOfWeek,
									'start_time' => '00:00:00',
									'schedule_id' => 0,
									'local_date' => $date,
									'start_shift' => $date. " 00:00:00",
									'end_shift' => $date. " 11:59:59"
								)
						);
					}

					if(!$end)
					{
						$sd->add(new DateInterval("P1D"));
					}
				}

				return $schedule;

		}

		if(!is_array($schedule))
		 throw new Exception("Invalid Arguments");


		$_schedule = $schedule;
		$schedule = array();
		foreach($_schedule as $sched)
		{
			$scheduleId = $sched["C"]["schedule_id"];
			$sched = $sched["D"];
			$sched["schedule_id"] = $scheduleId;

			if(!isset($schedule[$sched['start_day']]))
			{
				$schedule[$sched['start_day']] = array();
			}

			$schedule[$sched['start_day']][$sched['id']] = $sched;
		}



		$diff = ($startDate->diff($endDate));

		if($diff->d > 7) throw new Exception("Date Range should only be within a week");


		$sd = clone $startDate;
		$end = false;

		while (!$end)
		{
			$end = $sd >= $endDate;
			$dayOfWeek = $sd->format("N");

			if(isset($schedule[$dayOfWeek]))
			{

				foreach($schedule[$dayOfWeek] as &$shift)
				{
					$shift["local_date"] = $sd->format("Y-m-d");
					$this->updateShift(&$shift);
				}
			}

			if(!$end)
			{
				$sd->add(new DateInterval("P1D"));
			}
		}
		return $schedule;
	}

	function updateTimesheets ($userId,  $startDate,  $endDate)
	{
		if(!is_int($userId))
			 throw new Exception("Invalid Arguments");
		if(!is_string($startDate))
			 throw new Exception("Invalid Arguments");
		if(!is_string($endDate))
			 throw new Exception("Invalid Arguments");

		$companyId = $this->session["company_id"];
		debug("Checking Timesheet for user_id: $userId for $startDate - $endDate");

		$timesheets = $this->Timesheet->find("all", array("conditions"=>array(
			"user_id" => $userId,
			"company_id" =>$this->session["company_id"],
			"DATE(local_date) >=" => $startDate,
			"DATE(local_date) <=" => $endDate,
			"machine_id != -1" //do not include server initiated timesheet
		)));

		if(sizeof($timesheets) == 0)
			debug("No timesheets found.");

		foreach($timesheets as &$timesheet)
		{
			$timesheet = $timesheet[$this->Timesheet->alias];

			if(Timesheet::IsMinValue($timesheet["timein"]))
			{
				$this->findTimein(&$timesheet);
			}

			if(Timesheet::IsMinValue($timesheet["timeout"]))
			{
				$this->findTimeout(&$timesheet);
			}

			$this->flag($timesheet);
		}

		foreach($timesheets as $timesheet)
		{
			$this->recalculateTimesheet($timesheet);
		}
	}

	function findTimein($timesheet)
	{
		if(!is_array($timesheet))
			 throw new Exception("Invalid Arguments");

		$startTime = $timesheet["start_shift"];
		$endTime = $timesheet["end_shift"];
		$localDate = $timesheet["local_date"];


		if(Timesheet::IsMinValue($localDate))
		{
			return false;
		}

		if(Timesheet::IsMinValue($startTime))
		{
			$startTime = $localDate . " 00:00:00";
			$timesheet["start_shift"] = $startTime;
		}

		if(Timesheet::IsMinValue($endTime))
		{
			$endTime = $localDate . " 23:59:59";
			$timesheet["end_shift"] = $endTime;
		}

		if(Timesheet::IsMinValue($timesheet["timein"]) || Timesheet::IsMinValue($timesheet["machine_timein"]))
		{
			//get the schedule before this shift
			$previousShift = $this->getShiftBeforeTimesheet($timesheet);
			$conditions = array(
					"schedule_id" => $previousShift['schedule_id'],
					"shift_id" => $previousShift["id"],
					"user_id" => $timesheet["user_id"],
					"start_shift" => $previousShift["start_shift"],
					"end_shift" => $previousShift["end_shift"],
					"company_id" => $timesheet["company_id"]
				);

			//-1 SERVER, 0 => 1.2
			if((int)$timesheet["machine_id"] > 0)
				$conditions["machine_id"] = $timesheet["machine_id"];

			$previousTimesheet = $this->Timesheet->find("first", array(
				"conditions"=> $conditions
			));

			$d1 = $previousShift["end_shift"];

			if(isset($previousTimesheet[$this->Timesheet->alias]))
			{
				$previousTimesheet = $previousTimesheet[$this->Timesheet->alias];

				if(!Timesheet::IsMinValue($previousTimesheet["timeout"]))
				{
					if($this->findTimeout(&$previousTimesheet))
					{
						$to = $previousTimesheet["timeout"];
						if(!Timesheet::IsMinValue($to))
						{
							if(strtotime($to) > $strtotime($d1))
								$d1 = $to;
						}
					}
				}
			}

			// methodcalls can be in between $d1 and $startTime
			if(strtotime($d1) < strtotime($startTime)) $startTime = $d1;

			$conditions =  array(
					"user_id" => $timesheet["user_id"],
					"company_id" => $this->session["company_id"],
					"local_time >=" => $startTime,
					"local_time <=" => $endTime
				);

				// -1 = SERVER TIMESHEET, 0 = 1.2 Timesheet
				if((int)$timesheet["machine_id"] > 0)
						$conditions["machine_id"] = $timesheet["machine_id"];

				$firstMethodCall = $this->MethodCall->find("first", array( "conditions" => $conditions,
				"order" =>  'local_time ASC',
				"limit" => 1,
				"recursive" => "-1"
			));

			if(isset($firstMethodCall[$this->MethodCall->alias]))
			{
				$firstMethodCall = $firstMethodCall[$this->MethodCall->alias];

				$this->Timesheet->id = $timesheet["id"];

				if(Timesheet::IsMinValue($timesheet["timein"]))
				{
					$this->Timesheet->saveField("timein", $firstMethodCall["local_time"]);
					$timesheet["timein"] = $firstMethodCall["local_time"];

					$this->Timesheet->saveField("timein_notes", AUTOMATED_MESSAGE_FLAGGING);
				}

				if(Timesheet::IsMinValue($timesheet["machine_timein"]))
				{
					$this->Timesheet->saveField("machine_timein", $firstMethodCall["machine_time"]);
					$timesheet["machine_timein"] = $firstMethodCall["machine_time"];
				}


				debug("Timein updated");
			}
		}
	}

	function findTimeout($timesheet)
	{
		if(!is_array($timesheet))
			 throw new Exception("Invalid Arguments");

		//only do this if the timesheet is after shift ended

		$temptimeout = $timesheet["temp_timeout"];
		$tempmachinetimeout = $timesheet["tempmachine_timeout"];
		$localDate = $timesheet["local_date"];
		$startTime = $timesheet["start_shift"];
		$endTime = $timesheet["end_shift"];

		if(Timesheet::IsMinValue($localDate))
		{
			return false;
		}
		if(Timesheet::IsMinValue($startTime))
		{
			$startTime = $localDate . " 00:00:00";
		}

		if(Timesheet::IsMinValue($endTime))
		{
			$endTime = $localDate . " 23:59:59";
		}

		if(!isset($timesheet["id"])) debug($timesheet);

		$this->Timesheet->id = $timesheet["id"];

		//no need to proceed
		if(!Timesheet::IsMinValue($timesheet['timeout']) &&
			!Timesheet::IsMinValue($timesheet['machine_timeout'])
		)
			return true;

		$_timeout = $temptimeout;
		$_mtimeout = $tempmachinetimeout;

		if(Timesheet::IsMinValue($_timeout) || Timesheet::IsMinValue($_mtimeout))
		{
			//get from method calls
			$conditions = array(
					"user_id" => $timesheet["user_id"],
						"company_id" => $this->session["company_id"],
						"local_time >=" => $startTime,
						"local_time <=" => $endTime
					);

			//-1 SERVER TIMESHEET, 0 = 1.2
			if((int) $timesheet["machine_id"] > 0)
				$conditions["machine_id"] = $timesheet["machine_id"];

			$lastMethodCall = $this->MethodCall->find("first", array(
				"conditions" => $conditions,
				"order" =>  'local_time DESC',
				"limit" => 1,
				"recursive" => "-1"
			));

			debug($lastMethodCall);

			if(isset($lastMethodCall[$this->MethodCall->alias]))
			{
				$lastMethodCall = $lastMethodCall[$this->MethodCall->alias];
				if(Timesheet::IsMinValue($_timeout))
					$_timeout = $lastMethodCall["local_time"];

				if(Timesheet::IsMinValue($_mtimeout))
					$_mtimeout = $lastMethodCall["machine_time"];
			}
		}

		//update timeout only if timeout is greater than the end shift the end shift is less than today
		$today = new DateTime();
		if(isset($this->users[$timesheet['user_id']]) && isset($this->users[$timesheet['user_id']]['timezone']))
		{
			$today->setTimezone(new DateTimeZone($this->users[$timesheet['user_id']]['timezone']));
		}
		$today = $today->format("Y-m-d H:i:s");
		$past =  strtotime($endTime) < strtotime($today);
		$pastWorkTime = strtotime($endTime) < strtotime($_timeout);

		if($past)
		{
			if(Timesheet::IsMinValue($timesheet["timeout"]))
			{
				$this->Timesheet->saveField("timeout", $_timeout);
			}
			if(Timesheet::IsMinValue($timesheet["temp_timeout"]))
			{
				$this->Timesheet->saveField("temp_timeout", $_timeout);
			}


			if(Timesheet::IsMinValue($timesheet["machine_timeout"]))
			{
				$this->Timesheet->saveField("machine_timeout", $_mtimeout);
			}
			if(Timesheet::IsMinValue($timesheet["tempmachine_timeout"]))
			{
				$this->Timesheet->saveField("tempmachine_timeout", $_mtimeout);
			}
		}

		return true;

	}

	function flag($timesheet)
	{
		if(!is_array($timesheet))
			 throw new Exception("Invalid Arguments");

		if(!$timesheet["schedule_id"]) return;
		if(!$timesheet["shift_id"]) return;

		$userId = $timesheet["user_id"];
		$companyId = $this->session["company_id"];

		$timein = $timesheet["timein"];
		$timeout = $timesheet["timeout"];
		$startShift = $timesheet["start_shift"];
		$endShift = $timesheet["end_shift"];
		$localDate = $timesheet["local_date"];


		if(Timesheet::IsMinValue($localDate))
		{
			return false;
		}

		if(Timesheet::IsMinValue($startShift))
		{
			$startShift = $localDate . " 00:00:00";
		}

		if(Timesheet::IsMinValue($endShift))
		{
			$endShift = $localDate . " 23:59:59";
		}

		//here expect that the time in is not minvalue
		if(Timesheet::IsMinValue($timein) || Timesheet::IsMinValue($timeout))
		{
			return false;
		}

		//update arguments include timesheet
		if(!isset($this->arguments[$companyId]["users"][$userId]['timesheets']))
		{
			$this->arguments[$companyId]["users"][$userId]['timesheets'] = array();
		}
		//by local_date
		if(!isset($this->arguments[$companyId]["users"][$userId]['timesheets'][$localDate]))
		{
			$this->arguments[$companyId]["users"][$userId]['timesheets'][$localDate] = array();
		}
		//by shift id
		$shiftId = $timesheet["shift_id"];
		if(!isset($this->arguments[$companyId]["users"][$userId]['timesheets'][$localDate][$shiftId]))
		{
			$this->arguments[$companyId]["users"][$userId]['timesheets'][$localDate][$shiftId]= array();
		}

		$this->arguments[$companyId]["users"][$userId]['timesheets'][$localDate][$shiftId]= $timesheet;

		//check if early or late in
		$timein =  strtotime($timein);
		$startShift = strtotime($startShift);

		$diff = abs(round ( ($timein - $startShift) / (60), 2) );
		$flag = $diff >= LATE_EARLY_MARGIN;

		if($flag)
		{
			$flag = $startShift >= $timein ? EARLY_IN : LATE_IN;

			$flagResult = $this->TimesheetFlag->find("first", array("conditions"=>array(
				"flag" => $flag,
				"timesheet_id" => $timesheet["id"]
			)));

			if(!isset($flagResult[$this->TimesheetFlag->alias]))
			{
				debug("Flag Created: " , $flag);
				$this->TimesheetFlag->create();
				$this->TimesheetFlag->save(
					array(
						"company_id" => $companyId,
						"timesheet_id" => $timesheet["id"],
						"flag" => $flag,
						"last_updated" => date("Y-m-d H:i:s"),
						"created" => date("Y-m-d H:i:s"),
						"created_by" => TIMESHEET_FLAGGING,
						"updated_by" => TIMESHEET_FLAGGING,
						"notes" => AUTOMATED_MESSAGE_FLAGGING
					)
				);
			}
		}


		//check if early or late out
		$timeout =  strtotime($timeout);
		$endShift = strtotime($endShift);

		$diff =  abs(round ( ($timein - $startShift) / (60), 2) );
		$flag = $diff >= LATE_EARLY_MARGIN;

		if($flag)
		{
			$flag = $endShift >= $timeout ? EARLY_OUT : LATE_OUT;

			$flagResult = $this->TimesheetFlag->find("first", array("conditions"=>array(
				"flag" => $flag,
				"timesheet_id" => $timesheet["id"]
			)));

			if(!isset($flagResult[$this->TimesheetFlag->alias]))
			{
				debug("Flag Created: " , $flag);
				$this->TimesheetFlag->create();
				$this->TimesheetFlag->save(
					array(
						"company_id" => $companyId,
						"timesheet_id" => $timesheet["id"],
						"flag" => $flag,
						"last_updated" => date("Y-m-d H:i:s"),
						"created" => date("Y-m-d H:i:s"),
						"created_by" => TIMESHEET_FLAGGING,
						"updated_by" => TIMESHEET_FLAGGING,
						"notes" =>  AUTOMATED_MESSAGE_FLAGGING
					)
				);
			}
		}

	}

	function flagAbsent($userId)
	{
		if(!is_int($userId))
			 throw new Exception("Invalid Arguments");

		$companyId = $this->session["company_id"];
		$startDate = $this->arguments[$companyId]['start_date']   ;
		$endDate = $this->arguments[$companyId]['end_date']  ;

		$startDate = new DateTime($startDate);
		$endDate = new DateTime($endDate);


		if(
			isset($this->arguments[$companyId])
			&& isset($this->arguments[$companyId]["users"])
			&& isset($this->arguments[$companyId]["users"][$userId])
			&& $userId
		)
		{
			$userArguments = $this->arguments[$companyId]["users"][$userId];

			if(!isset($userArguments["schedule"]) || $userArguments["schedule"] === false)
			{
				//no need to flag since user has no schedule
				return;
			}
			$schedule = $userArguments["schedule"];


 			foreach($schedule as $dayOfWeek => $shifts)
			{
				foreach($shifts as $shift)
				{
					if(isset($shift["local_date"])) // the only shift that is within scope of the date range ;) date range should only be per week
					{
						$localDate = $shift["local_date"];
						if(isset($userArguments["timesheets"]))
						{
							if(isset($userArguments["timesheets"][$localDate]))
							{
								$shiftId = $shift["id"];
								if(isset($userArguments["timseheets"][$localDate][$shiftId]))
								{
									$timesheet = $userArguments["timseheets"][$localDate][$shiftId];
									$workTime =$timesheet["work_time"];
									$idleTime = $timesheet["idle_time"];
									if((double) $workTime == 0  )
									{
										$this->__flagAbsent($shift, $userId, $timesheet);
									}
								}
								else
								{
									$this->__flagAbsent($shift, $userId);
								}
							}
							else
							{
								$this->__flagAbsent($shift, $userId);
							}
						}
						else
						{
							$this->__flagAbsent($shift, $userId);
						}
					}
				}
			}
		}
	}

	function updateShift(array $shift)
	{
		if(!is_array($shift))
			 throw new Exception("Invalid Arguments");

		if(!isset($shift["local_date"])) return;

		if(isset($shift['end_shift']) && !Timesheet::IsMinValue($shift['end_shift'])) return $shift;

		$starTime = new DateTime($shift["local_date"]);
		$time = explode(":", $shift['start_time']);
		$h = isset($time[0]) ? $time[0]  : 0;
		$m = isset($time[1]) ? $time[1]  : 0;
		$s = isset($time[2]) ? $time[2]  : 0;
		$starTime->setTime($h, $m, $s);

		$endTime = clone $starTime;

		$requiredHours = $shift['required_hours'];
		$totalMinutes = $requiredHours*60;

		if($shift['allowed_break_time'])
		{
			$totalMinutes += $shift['allowed_break_time'];
		}

		$hours = (int) ($totalMinutes / 60) ;
		$minutes = $totalMinutes % 60;
		$in = "PT". $hours . "H";
		if($minutes)
		{
			$in .= $minutes. "M";
		}

		$endTime->add(new DateInterval($in));

		$shift["start_shift"] = $starTime->format("Y-m-d H:i:s");
		$shift["end_shift"] = $endTime->format("Y-m-d H:i:s");
	}

	function __flagAbsent($shift, $userId, $timesheet = null)
	{
		if(!is_array($shift))
			 throw new Exception("Invalid Arguments");
		if(!is_int($userId))
			 throw new Exception("Invalid Arguments");
		if(!is_array($timesheet) && $timesheet !== null)
			 throw new Exception("Invalid Arguments");


		$companyId = $this->session["company_id"];

		$this->updateShift(&$shift);

		if($timesheet == null)
		{
			$timesheet = $this->Timesheet->find("first", array("conditions"=>array(
				"company_id" => $companyId,
				"user_id"=>$userId,
				"shift_id" => $shift["id"],
				"schedule_id" => $shift["schedule_id"],
			)));

			if(isset($timesheet[$this->Timesheet->alias]))
				$timesheet = $timesheet[$this->Timesheet->alias];
			else $timesheet = null;
		}

		if($timesheet == null)
		{
			$this->Timesheet->create();
			$timesheet = array(
				"user_id" => $userId,
				"company_id" => $companyId,
				"shift_id" => $shift["id"],
				"schedule_id" => $shift["schedule_id"],
				"start_shift" => $shift['start_shift'],
				"end_shift" => $shift["end_shift"],
				"timein" => DATE_MIN_VALUE,
				"timeout" => DATE_MIN_VALUE,
				"tempmachine_timeout" => DATE_MIN_VALUE,
				"temp_timeout" => DATE_MIN_VALUE,
				"machine_timein" => DATE_MIN_VALUE,
				"machine_timeout" => DATE_MIN_VALUE,
				"machine_id" => 0,
				"timein_notes" => AUTOMATED_MESSAGE_FLAGGING,
				"timeout_notes" => AUTOMATED_MESSAGE_FLAGGING,
				"idle_time" => 0,
				"work_time" => 0,
				"temp_idle_time" => 0,
				"temp_work_time" => 0,
				"last_calculated" => "NOW()",
				"local_date" => $shift["local_date"],
				"merged" => "",
				"last_updated" => "NOW()",
				"created" => "NOW()",
				"updated_by" => TIMESHEET_FLAGGING,
				"created_by" => TIMESHEET_FLAGGING,
				"done_flagging_late_in" => 0,
				"done_flagging_early_out" => 0,
				"done_flagging_overtime" => 0,
				"schedule_changed" => 0,
				"is_compromised" => 0,
				"is_calibrated" => 1
			);

			$this->Timesheet->save($timesheet);
			$timesheet["id"] = $this->Timesheet->getInsertID();
		}


		$workTime =$timesheet["work_time"];
		if((double) $workTime > 0  )
		{
			//flag absent
			$flagResult = $this->TimesheetFlag->field("id",  array(
				"flag" => ABSENT,
				"timesheet_id" => $timesheet["id"],
			));

			if($flagResult)
			{
				debug("Removing absent flag for timesheet ". $timesheet["id"]);
				$this->TimesheetFlag->delete($flagResult);
			}

			return;
		}


		//flag absent
		$flagResult = $this->TimesheetFlag->find("first", array("conditions"=>array(
			"flag" => ABSENT,
			"timesheet_id" => $timesheet["id"],
		)));

		if(!isset($flagResult[$this->TimesheetFlag->alias]))
		{
			debug("Flag Absent for date " .  $shift["local_date"]);
			$this->TimesheetFlag->create();
			$this->TimesheetFlag->save(
				array(
					"company_id" => $companyId,
					"timesheet_id" => $timesheet["id"],
					"flag" => ABSENT,
					"last_updated" => date("Y-m-d H:i:s"),
					"created" => date("Y-m-d H:i:s"),
					"created_by" => TIMESHEET_FLAGGING,
					"updated_by" => TIMESHEET_FLAGGING,
					"notes" => AUTOMATED_MESSAGE_FLAGGING
				)
			);
		}
	}


	function getShiftBeforeTimesheet($timesheet)
	{
		if(!is_array($timesheet)) throw new Exception("Invalid Timesheet. Not an array");
		if(!isset($timesheet['user_id'])) throw new Exception("Invalid Timesheet. No userid");
		if(!isset($timesheet['company_id'])) throw new Exception("Invalid Timesheet. No company id");


		//check if it has a schedule
		$schedule = false;
		$userId = $timesheet['user_id'];
		$companyId = $timesheet['company_id'];
		$startShift = $timesheet['start_shift'];

		//get start_shift first
		if(Timesheet::IsMinValue($timesheet['start_shift']))
			return false;


		//get schedule id of current timesheet
		$startTime = new DateTime($startShift);
		$dayOfWeek = $startTime->format("N");

		$schedule =  $this->arguments[$companyId]["users"][$userId]["schedule"];

		$currentShift = isset($schedule[$dayOfWeek]) && isset($schedule[$dayOfWeek][$timesheet['shift_id']]) ? $schedule[$dayOfWeek][$timesheet['shift_id']] : false;

		$_schedule =  $this->arguments[$companyId]["users"][$userId]["_schedule"]; //flattened out

		$previousShift = false;

		if($_schedule) //sort schedule by start_day and start_time
		{

			$sts = array();
			$sds = array();

			foreach($_schedule as $key => &$value)
			{
				$_value = $value;
				$value = $value["D"];
				$value['schedule_id'] = $_value["C"]["schedule_id"];
				$sts[] = new DateTime(date('Y-m-d'). ' '. $value['start_time']);
				$sds[] = $value['start_day'];
			}

			array_multisort($sds, SORT_NUMERIC, $sts, SORT_ASC, $_schedule);

			if($currentShift)
			{
				$_currentShift = array_filter($_schedule, function($a) use (&$currentShift){
					return	$a['id'] == $currentShift['id'] &&
								$a['schedule_id'] == $currentShift['schedule_id'];;
				});
				$a = array_keys($_currentShift);
				$index = $a[0];

				$previousIndex = $index > 0 ? $index - 1 :  sizeof($_schedule) - 1;

				$_previousShift = $_schedule[$previousIndex];

				if(isset($schedule[$_previousShift['start_day']]) && isset($schedule[$_previousShift['start_day']][$_previousShift['id']]))
				{
					$previousShift = $schedule[$_previousShift['start_day']][$_previousShift['id']];
				}
			}

		}

		if(!$previousShift)
		{
			//yesterday
			$yesterdayOfWeek = $dayOfWeek;
			$yesterday = clone $startTime;
			$yesterday->sub(new DateInterval("P1D"));

			$previousShift = array(
				'company_id' => $companyId,
				'id' => 0,
				'start_day' => $yesterday->format("N"),
				'start_shift' => $yesterday->format("Y-m-d").' 00:00:00',
				'end_shift' => $yesterday->format("Y-m-d").' 11:59:59',
				'schedule_id' => 0
			);
		}

		//get day diff
		$diff =  abs(  (int) $currentShift['start_day'] - (int) $previousShift['start_day'] ) ;
		$previousStartTime = clone $startTime;
		$startTime->setTime(0,0,0);

		if($diff > 0)
		{
			$previousStartTime->sub(new DateInterval("P" . $diff . "D"));
		}

		$previousShift["local_date"] = $previousStartTime->format("Y-m-d H:i:s");

		$this->updateShift(&$previousShift);

		return $previousShift;
	}

	/**
	 * recalculates timesheet
	 * creates a new instance of timesheet where machine_id = -1 (for server) marks as recalculated timesheet
	 * Calculate only when shift has ended and user has timeout
	 */
	function recalculateTimesheet($timesheet)
	{
		if(!is_array($timesheet))
			 throw new Exception("Invalid Arguments");

		//only recalculate if it is past today
		$endTime = $timesheet["end_shift"];

		if(Timesheet::IsMinValue($endTime))
			 throw new Exception("Invalid Timesheet, no end_shift");

		$today = new DateTime();
		if(isset($this->users[$timesheet['user_id']]) && isset($this->users[$timesheet['user_id']]['timezone']))
		{
			$today->setTimezone(new DateTimeZone($this->users[$timesheet['user_id']]['timezone']));
		}
		$today = $today->format("Y-m-d H:i:s");
		$past =  strtotime($endTime) < strtotime($today);

		if(!$past) return false;

		//calculate based on timesheet's schedule
		$conditions = array(
			"schedule_id" => $timesheet['schedule_id'],
			"shift_id" => $timesheet["shift_id"],
			"user_id" => $timesheet["user_id"],
			"start_shift" => $timesheet["start_shift"],
			"end_shift" => $timesheet["end_shift"],
			"company_id" => $timesheet["company_id"],
			"machine_id" => -1
 		);

		unset($this->Timesheet->id);
		$serverTimesheet = $this->Timesheet->find("first", array("conditions"=>$conditions));

		//already calculated
		if(isset($serverTimesheet[$this->Timesheet->alias]))
		{
			$serverTimesheet = $serverTimesheet[$this->Timesheet->alias];
			debug("ServerTimesheet Has Been Calculated/Initiated");

			return;
		}
		else
		{
			$serverTimesheet = $timesheet;

			//reset everything
			unset($serverTimesheet["id"]);
			$serverTimesheet["machine_id"] = -1;
			$serverTimesheet["machine_name"] = "SERVER";
			$serverTimesheet["idle_time"] =
				$serverTimesheet["work_time"]=
				$serverTimesheet["temp_idle_time"]  =
				$serverTimesheet["temp_work_time"] = 0;

			$this->Timesheet->create();
			$this->Timesheet->save($serverTimesheet);
			$serverTimesheet["id"] = $this->Timesheet->getInsertID();
		}

		$serverTimesheet["last_calculated"] = $today;
		$this->Timesheet->id = $serverTimesheet["id"];
		$this->Timesheet->saveField("last_calculated", $today);


		$timein = $serverTimesheet["timein"];
		$timeout = $serverTimesheet["timeout"];
		$startShift = $serverTimesheet["start_shift"];
		$endShift = $serverTimesheet["end_shift"];
		$localDate = $serverTimesheet["local_date"];

		if(Timesheet::IsMinValue($localDate))
		{
			return false;
		}
		if(Timesheet::IsMinValue($startShift))
		{
			$startShift = $localDate . " 00:00:00";
		}

		if(Timesheet::IsMinValue($endShift))
		{
			$endShift = $localDate . " 23:59:59";
		}

		//timein and timeout should not be minvalue
		if(Timesheet::IsMinValue($timein) || Timesheet::IsMinValue($timeout))
		{
			return false;
		}

		$companyId = $serverTimesheet["company_id"];
		$userId = $serverTimesheet["user_id"];

		debug($serverTimesheet);

		//have to check if there are other timesheets with earlier timein or late timeout

		$otherTimesheets = $this->Timesheet->find("all", array("conditions" => array(
			"user_id" => $serverTimesheet["user_id"],
			"company_id" => $serverTimesheet["company_id"],
			"start_shift" => $serverTimesheet["start_shift"],
			"end_shift" => $serverTimesheet["end_shift"],
			"schedule_id" => $serverTimesheet["schedule_id"],
			"shift_id" => $serverTimesheet["shift_id"],
			"machine_id != -1", //do not include servertimesheet

			),
			"order" => "timein ASC",
			"fields" => array("timein", "machine_timein", "timeout", "machine_timeout")
		));
		if(sizeof($otherTimesheets))
		{
			if( isset($otherTimesheets[0][$this->Timesheet->alias]))
			{
				$first = $otherTimesheets[0][$this->Timesheet->alias];

				if(strtotime($first["timein"]) < strtotime($serverTimesheet["timein"]))
				{
					$this->Timesheet->saveField("timein", $first["timein"]);
				}
				if(strtotime($first["machine_timein"]) < strtotime($serverTimesheet["machine_timein"]))
				{
					$this->Timesheet->saveField("machine_timein", $first["machine_timein"]);
				}

				//reorder by timeout$sts = array();
				$timeouts = array();

				foreach($otherTimesheets as $key => $value)
				{
					$value = $value[$this->Timesheet->alias];
					$timeouts[] = new DateTime(  $value['timeout'] );
				}

				array_multisort($timeouts, SORT_DESC,  $otherTimesheets);

				$first = $otherTimesheets[0][$this->Timesheet->alias];

				if(strtotime($first["timeout"]) > strtotime($serverTimesheet["timeout"]))
				{
					$this->Timesheet->saveField("timeout", $first["timeout"]);
				}
				if(strtotime($first["machine_timeout"]) > strtotime($serverTimesheet["machine_timeout"]))
				{
					$this->Timesheet->saveField("machine_timeout", $first["machine_timeout"]);
				}
			}

		}

		debug("Recalculation Starts");
		require_once(ROOT.DS."Model".DS."TimesheetCalculator.php");
		$calculation = new TimesheetCalculator();
		$calculation->timesheet = $serverTimesheet;
		$calculation->shift = array(
			"schedule_id" => $serverTimesheet["schedule_id"],
			"id" => $serverTimesheet["shift_id"],
			"start_time" => $serverTimesheet["start_shift"],
			"end_time" => $serverTimesheet["end_shift"],
			"start_date" => $serverTimesheet["local_date"]
		);
		$calculation->session = $this->session;

		//set models
		$calculation->models = array(
			"Timesheet" =>  $this->Timesheet,
			"StatusLog" =>  $this->StatusLog,
			"MethodCall" => $this->MethodCall
		);
		$calculation->Timesheet = $this->Timesheet;
		$calculation->StatusLog = $this->StatusLog;
		$calculation->MethodCall = $this->MethodCall;

		$calculation->StartTimesheetCalculation(true);
	}

}