<?php
/**
 * Allows syncronization
 *
 * @author Lorelie Dazo
 * @since 	2012-07-09
 * revision
 */

class SynchController extends AppController
{
	var $name = "SynchController";

	var $sessionData;
	var $user;

	///8006 needs to call getToken call again
	///8007 (renewToken)

	function __methodCall()
	{
		//get token
		if(!$this->__inPostData("TOKEN"))
		{
			$this->error = true;
			$this->errorCode = 1001;
			return;
		}

		$token = (string) $this->requestData->TOKEN;

		$this->__initializeUser($token);

		if($this->error) return;

		//synch what needs to be synch
		$this->__processSynchMethod();
	}


	function __getUserTestFolder()
	{
		if(isset($this->requestData->TOKEN))
		{
			//debug($this->__genToken());
			$token = (string) $this->requestData->TOKEN;
			if($token)
			{
				$this->__initializeUser($token);

				return $this->user->user_email;
			}
		}
	}


}
/** END OF FILE **/