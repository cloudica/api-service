<?php
/**
 * allows access of timesheet flagging shell tru web request
 *
 * @author Lorelie Dazo
 * @since 	2013-06-19
 * revision
 */
App::import("Model", "Version");
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class FlagController extends AppController
{
	var $name = "FlagController";

	var $uses = array("Minion");

	function __methodCall()
	{
		$hasArguments = $this->__inPostData("flags");
		if($hasArguments)
		{
			$hasArguments = $this->__inPostData("timesheet_id") || ($this->__inPostData("user_id") &&  $this->__inPostData("date"));
		}

		if($hasArguments)
		{

			if(!$this->__inPostData("TOKEN"))
			{
				$this->error = true;
				$this->errorCode = 1001;
				return;
			}

			$token = (string) $this->requestData->TOKEN;

			$this->__initializeUser($token);

			if(isset($this->user) && isset($this->session) && isset($this->session['content']))
			{
				$session = json_decode($this->session['content']);

				if(json_last_error() == JSON_ERROR_NONE)
				{
					$role = $session->role;

				}

				$params = array(
					"flags" => $this->requestData->flags
				);

				$userIds = array();

				if($this->__inPostData("timesheet_id"))
				{
					$params["timesheet_id"] = $this->requestData->timesheet_id;
				}
				else
				{
					$params["date"] = $this->requestData->date;

					if($role != "Owner"  )
					{
						switch($role)
						{
							case "Manager":
								$userIds = explode(",", $this->requestData->user_id);
								$userIds = array_map(function($user){return (int)($user);}, $userIds);
								$userIds = array();
								$Managers = $this->__importModel("MManagers", "minions");
								$Managers->fields = array("user_id");
								$managers = $Managers->find("all", array(
									"conditions"=>array("manager_id"=>$session->user_id, "user_id"=>$userIds),
									"fields"=>"user_id")
								);
								if($managers)
								{
									$managers = array_values(Set::flatten($managers));
									$userIds = array_intersect($managers, $userIds);
								}
								$userIds =  implode(",",$userIds);
								$params["user_id"] = $userIds;

								break;
							case "Remote User":
								$params["user_id"] = $session->user_id;
							break;

						}
					}
					else {
						$params['user_id']  = $this->requestData->user_id;
					}
				}

				$arguments = array($session->company_id=>$params);

				App::import('Core', 'Shell');
				$args = array ("main", "company", json_encode($arguments));

				App::Import('Shell','TimesheetFlagging');
				$flag = new TimesheetFlaggingShell();
				$flag->company = array("company_id" => $session->company_id, "company_domain" => $session->company_domain);
				$flag->initialize();
				$flag->loadTasks();
				$flag->runCommand("main", $args);

				$this->responseData = array();
				$this->responseData['OPERATIONS'] = $flag->operations;

			}
		}
		else
		{
			$this->error = true;
			$this->errorCode = 1001;
			$this->responseData->EXTRA = "Insufficient Arguments";
		}
	}

	function __inPostData($fieldName)
	{
		$xpath = "//request/$fieldName";

		return isset($this->requestData->$fieldName);
	}

	function __initializeUser($token)
	{

		if(!isset($this->MSession))
		{
			$this->MSession = $this->__importModel("MDashboardSession");
		}

		parent::__initializeUser($token);
	}


	function __getReturnSynch($model, $machineInstances, $success, $lastSynched)
	{
	}

	function __showResponse()
	{
		if(!empty($this->token ))
		{
			$this->responseData['TOKEN'] = $this->token;
		}

		$error = $this->__getErrorMessage();

		if(!isset($this->responseData)) $this->responseData = array();

		$this->responseData['ERRCODE'] = $this->errorCode;
		$this->responseData['ERRMESSAGE'] = $error["error_message"];
		$this->responseData['FLAGGED'] = $error["error_comment"];


		$this->__getReturnCode();

		$this->responseData['FLAGS'] = $this->userFlags;

		$this->responseData['VERSION'] = $this->__getVersion();

		$this->set("response", $this->responseData);
		$return = array("response" => $this->responseData, "endResponse" => "");
		echo json_encode($return);
	}

}

/** END FILE **/
