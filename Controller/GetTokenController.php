<?php
require_once("VerifyController.php");

/**
 * Removes the old session entry from database if exists for keys: user_id, company_id and machine_id
 * Creates a new session for unique key:  user_id, company_id and machine_id
 * Updates machine information
 *
 * @author Lorelie Dazo
 * @since 2012-04-20
 * revision 2012-07-02
 *
 *
 */

class GetTokenController extends VerifyController
{
	public $components = array('Crypt');

	var $machineId = 0;
	var $offsetTime;
	var $machineIds;
	var $revisionNumber;
	var $name = "GetToken";

	function __methodCall()
	{
		//check user credentials
		$this->__checkUserCredentials();
		if($this->error) return;
		$this->__updateUser();
		if($this->error) return;
		$this->__saveMachineInfo();
		if($this->error) return;
		$this->responseData->TOKEN = $this->__genToken();

		$this->__identifyRequest();

		if(!isset($this->responseData->TOKEN))
		{
			$this->errorCode =8002;
			$this->error = true;
			return;
		}
		if(empty($this->responseData->PACKAGE))
		{
			$this->errorCode = 8027;
			$this->error = true;
			return;
		}
		if($this->error) return;

		if($this->session != null)
		{
			$_SESSION["Cloudica"] = $this->session;
			$this->__getChangedData();
			$this->__processSynchMethod();
		}
	}

	function __checkUserCredentials()
	{
		if(	!$this->__inPostData("COMPANYID")
			|| !$this->__inPostData("USERNAME")
			|| !$this->__inPostData("PASSWORD")
		)
		{
			$this->errorCode = 1001;
			$this->error = true;
			$this->responseData->EXTRA = " LOGIN DETAILS";
			return;
		}
		//verify account first
		$user = $this->__getUser();
		if($this->error) return;
		$this->__checkUserErrors();
		if($this->error) return;
		$this->__verifyAccount($user);
		if($this->error) return;

		if($this->user == null) return;

		if($this->user->account == null) return;

		if(!$this->user->timezone)
		{
			$this->user->timezone = (string) $this->requestData->TIMEZONE;
		}

		if(!$this->user->timezone  ) return;

		$this->__getUserResponse();
	}

	function __updateUser()
	{
		if($this->error) return;

		//update last login in account
		$lastLogin = (string) $this->requestData->LASTLOGIN;
		if(empty($lastLogin))
		{
			$lastLogin = (string) $this->requestData->TIMESTAMP;
		}
		$lastLogin = date("Y-m-d H:i:s", strtotime($lastLogin) + $this->user->offset_time);


		$this->MAccount->updateLastLogin($this->user->user_id, $this->user->account->company_id, $lastLogin);
		$this->responseData->OFFSET_TIME = (int) $this->user->offset_time;
	}

	public static function IsMinValue($stringDate)
	{
		$stringDate = substr($stringDate, 0, 10);

		$minDate1 = "0000-00-00";
		$minDate2 = "0001-01-01";

		return $stringDate == $minDate1 || $stringDate == $minDate2;

	}


	function __getChangedData()
	{
		try
		{
			//check for changes
			if(!$this->Changes)
			{
				$this->__importModel("Changes", "minion");
			}
			$this->Changes->session = $this->session;
			$clientChanges = $this->Changes->parseSingleArray( str_getcsv( (string) $this->__getSynchDataByType("changes", false)), $this->Changes->fields);

			$changes = $this->Changes->find("first", array("conditions"=>array("user_id"=>$this->user->user_id, "company_id"=>$this->session['company_id']), "fields"=> array_merge(array("id"), $this->Changes->fields)));

			if(!$changes)
			{
				$changes =  array(
						"user_id" => $this->session["user_id"],
						"company_id" => $this->session["company_id"],
						"employee_id" => $this->session["employee_id"]
				) ;
				if($this->Changes->save($changes))
				{
					$changes["id"] = $this->Changes->getInsertID();
					$changes = array( $this->Changes->alias => $changes);
				}
				else $changes = null;
			}

			if($this->error) return;

			if($changes && isset($changes[$this->Changes->alias]))
			{

				$this->Changes->id = $changes[$this->Changes->alias]["id"];
				unset( $changes[$this->Changes->alias]["id"]);
				$newchanges = array();


				$timezone =  $this->user->timezone;

				foreach($changes[$this->Changes->alias] as $key=>$value)
				{
					if(!isset($clientChanges[$key] ) || self::IsMinValue($clientChanges[$key]))
						$client = 0;
					else
						$client = strtotime($clientChanges[$key]);

					$server = new DateTime($value);
					$server->setTimezone(new DateTimeZone($timezone));

					$value = $server->format("Y-m-d H:i:s");

					if(self::IsMinValue($value))
						$server = 0;
					else
						$server = strtotime($value);


					if($server > $client)
					{
						$changed = $this->Changes->getChangedData($key);

						if(isset($changed["data"]) && isset($changed["type"]))
						{
							$xml = $this->responseData->SYNCH->addChild("DATA", $changed["data"]);
							$xml->addAttribute("type", $changed["type"]);
							$xml->addAttribute("lastSynch", $value);
							$xml->addAttribute("success", true);
							$newchanges[$key] = $this->Now;
						}
					}
				}
			}
		}
		catch (Exception $e)
		{
			debug($e->getMessage());
		}

	}

	function __getUserResponse()
	{
		//displayname
		$this->responseData->DISPLAYNAME = $this->user->display_name;

		//timezone
		$this->responseData->TIMEZONE = $this->user->timezone;

		//offset
		$this->responseData->OFFSET = $this->user->offset_time;

		//license validity
		$this->responseData->VALIDTILL = $this->user->account->validity;

		//license packacge
		$this->responseData->PACKAGE = $this->user->account->package;

		$this->responseData->SCHEDULE_ID = isset($this->user->account->schedule_id) ? (int) $this->user->account->schedule_id : 0;
		$this->responseData->SCHEDULE = isset($this->user->account->schedule_name) ? $this->user->account->schedule_name : "";
	}


	function __genToken()
	{
		if(!isset($this->MSession))
		{
			$this->MSession = $this->__importModel("MSession");
		}

		$expireTime = $this->__getServerTime(TOKEN_TIMEOUT, true);

		$sessionKey = $this->user->user_id.$this->user->account->company_id.$this->MMachineInfo->id;

		$token = md5(
			$this->Crypt->ccEncrypt(
				md5(microtime(). CC_KEY. $sessionKey)
			)
		);

		//update session
		$data = array(
				"user_id"=>$this->user->user_id,
				"company_id"=>$this->user->account->company_id,
				"session_id"=>$token,
				"machine_time"=>(string) $this->requestData->TIMESTAMP,
				"machine_id"=>$this->machineId,
				"local_time"=>(string) $this->requestData->LOCALTIME,
				"expire_time"=>$expireTime,
				"company_domain" => $this->user->account->company_domain,
				"revision_number" => $this->revisionNumber,
				"employee_id" => $this->user->account->employee_id,
		);
		$this->MSession->updateSesion($data);

		$this->session = $this->MSession->data["MSession"];

		return $this->MSession->token;
	}

	/*
	 * just return current machineID
	 */
	function __saveMachineInfo()
	{
		$account = $this->user->account;
		$companyId =$account->company_id;
		$companyDomain = $account->company_domain;

		$this->responseData->MACHINEID = parent::__saveMachineInfo($companyId, $companyDomain);

	}


	function __getUserTestFolder()
	{
		return (string) $this->requestData->USERNAME;
	}
}