<?php require_once ("SynchController.php");
class LogoutController extends SynchController
{
	var $name = "Logout";

	function __methodCall()
	{
		//synch first
		parent::__methodCall();

		//then logout
		$this->ClientSession->endSession($this->sessionData->user_id, $this->sessionData->company_id, $this->sessionData->machine_id);
	}
}
/** END OF FILE **/