<?php
/**
 * returns the current version of client
 *
 * @author Lorelie Dazo
 * @since 	2013-06-19
 * revision
 */
App::import("Model", "Version");
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
class VersionController extends AppController
{

	var $name = "VersionController";
	var $dir = null;
	var $currentVersion = null;
	var $uses = array("Version");

	function index()
	{
		$this->autoRender = false;

		//check for request
		$requestData = (array) $this->request->requestData;
		$this->currentVersion = new Version( $this->__getVersion());

		//check if has local version
		if(isset($requestData["LOCAL"]))
		{
			$this->__checkVersion($requestData["LOCAL"]);

		}
		else
			echo $this->currentVersion->content;
	}

	function __checkVersion($version)
	{
		App::import("Model", "Version");
		$localVersion = new Version($version);

		$updates = null;
		$hasUpdates = true;
		$fileUpdate = "";

		if($localVersion->content == $this->currentVersion->content)
		{
			// no updates;
			$hasUpdates = false;
		}
		else
		{
			//compare versions
			if($localVersion->versionNumber != $this->currentVersion->versionNumber)
			{
				$updates = $this->currentVersion->getLatestFromVersionNumber($localVersion->versionNumber);

				$fileUpdate = "version_" . $localVersion->versionNumber ."_". $this->currentVersion->versionNumber .".zip" ;
			}
			else
			{
				//lower version or higher version?
				if($localVersion->versionNumber > $this->currentVersion->versionNumber)
				{
					$this->__invalidRequest();
				}
				else
				{
					//if same version just check the build numbers, serials are sometimes confusing
					//check build numbers
					$updates = $this->currentVersion->getLatestFromBuildNumber($localVersion->build);
					$fileUpdate = "build_" . $localVersion->build ."_". $this->currentVersion->build .".zip" ;
				}
			}
		}

		if($hasUpdates)
		{
			$this->__createUpdates($fileUpdate, $updates);
		}
		else
		{
			$this->__upToDate();
		}
	}

	function __uptodate()
	{
		echo $this->currentVersion->content;
	}

	function __invalidRequest()
	{
		echo 0;
	}

	function __createUpdates($fileUpdate, $updates)
	{
		if($updates == null || empty($fileUpdate))
		{
			$this->__invalidRequest();
			return;
		}
		//check if file exists
		//check first if the file to be downloaded is available
		$file = $this->__initializeDownloadFile($fileUpdate);
		$success = true;
		if(!$file)
		{
			//create updates for download file
			//collect
			if(sizeof($updates) > 0)
			{
				$zip = new ZipArchive($fileUpdate);
				$destination = CLIENT_UPDATES_DIR. DS. $fileUpdate;
				if($zip->open($destination, ZipArchive::CREATE))
				{
					$files = array();
					foreach($updates as $update)
					{
						$update = $update[0];

						$build = $update["build"];
						$build = trim(trim($build, "0"), ".");

						$filename = CLIENT_UPDATES_DIR. DS.$update["version"] . DS . $build . DS. $update["filename"];

						$validFile = file_exists($filename);
						if($validFile)
						{
							$zip->addFile($filename,  $update["filename"]);
						}
					}

					$this->__createVersionText();
					$zip->addFile($this->dir->path .DS."version.txt",  "version.txt");
					$zip->close();
					$file = $fileUpdate;
				}  else $success = false;
			}
			else $success = false;
		}
		if($success)
			$this->__downloadFile($file);
	}

	function __createVersionText()
	{
		//create version.txt
		$versionFile = new File($this->dir->path .DS."version.txt");
		$versionFile->write($this->currentVersion->content);
		$versionFile->close();
	}


	function __initializeDownloadFile($filename, $dir = null)
	{
		if($dir == null)
		{
			$this->__initializeDownloadDir();
			$dir = $this->dir;
		}
		$file = $dir->find($filename);
		if(isset($file[0]))
		{
			$file = $file[0];
			$path = $dir->path .DS. $file;
			return new File($path);
		}

		return false;
	}

	function __initializeDownloadDir()
	{
		if ($this->dir == null)
		{
			$this->dir = new Folder(CLIENT_UPDATES_DIR);
		}
	}

	function __initializeDir($path)
	{
		$this->__initializeDownloadDir();

		$dir = new Folder($this->dir->path . $path);

		return $dir;
	}

	/**
	* $file should be object
	**/
	function __downloadFile($file)
	{
		if(is_string($file))
		{
			$file = $this->__initializeDownloadFile($file);
		}
		if(!$file)
		{
			$this->__invalidRequest();
			return;
		}
		//initialize download
		header('Content-Disposition: inline; filename="' . $file->path . '"');
		header("Content-length: ".filesize( $file->path));
		header('Content-Type: application/zip');
		header('Content-Disposition: attachment; filename="' . $file->name . '"');

		$contents = $file->read();
		$file->close();
		echo $contents;
	}
}

/** END FILE **/
