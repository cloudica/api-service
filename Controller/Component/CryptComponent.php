<?php 
/**
 * This is the crypt controller of Live Monitor
 * It creates a custom hashing of string 
 * Used for login and logout functions of live monitor
 * Last Modified by:	Lorelie Dazo-Defensor
 * Latest Revision: 	March 17, 2011
 * @copyright	2011 Eversun Software Philippines Corp.
 * @license		http://www.eversunsoft.com/license
 * @version		Release
 * @author		
 * @since
 * 
 * Revision Changes:
 * 
 */
?>
<?php
class CryptComponent extends Component 
{
	 
	
	function ccEncrypt($text) {
		$key = CC_KEY;
		$key = md5($key);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv);
		return base64_encode($crypttext);
	}

	function ccDecrypt($enc) {
		$key = CC_KEY;
		$enc =base64_decode($enc);
		$key = md5($key);
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $enc, MCRYPT_MODE_ECB, $iv);
		$decrypttext1 = trim($decrypttext);
		return ($decrypttext1) ;
	}
	
	function passwordEncrypt($password) {
		return md5($this->ccEncrypt(md5($password . CC_KEY)));
	}
	
	function customEncrypt($text)
	{
		return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, KEYSTROKE_KEY, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
	}
}
?>