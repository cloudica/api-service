<?php 
/**
 * This is the SMTPGmail component of Live Monitor
 * Uses GMAIL as smtp server for sending emails
 * Extends the core emailcomponent of cakephp
 * Last Modified by:	Lorelie Dazo-Defensor
 * Latest Revision: 	March 17, 2011
 * @copyright	2011 Eversun Software Philippines Corp.
 * @license		http://www.eversunsoft.com/license
 * @version		Release
 * @author		Lorelie Dazo-Defensor [Eversun Software Philippines Corp - Matalino QC Office]
 * @since
 * 
 * Revision Changes:
 * 
 */
?>
<?php
App::import('Component', 'Email');

class GmailComponent extends EmailComponent	{
	
	const GUSER = "agentguardiansupport@gmail.com";
	const GPASSWORD = "abc12345"; 
	
	function initialize( $controller ) 
	{ 
       	$this->controller = $controller; 
	}
	
	function send() {	
		$this->smtpOptions = array(
			'port'=>'465', 
			'timeout'=>'30',
			'host' => 'ssl://smtp.gmail.com',
			'username'=> self::GUSER,
			'password'=> self::GPASSWORD
		);
		$this->xMailer = "AgentGuardian Live Monitor - Gmail Server"; 
		$this->attachments = array(
				HTTP_SERVER."/img/ag_logo.png"
		);
		$this->from = array(
			"name" => "AgentGuardian Live Monitor Support",
			"email" => self::GUSER,
			"password" => self::GPASSWORD
		);
		
		$this->delivery = 'smtp';
		
		$this->to = $this->to['name'] . ' <' . $this->to['email'] . '>';
		$this->from = $this->from['name'] . ' <' . $this->from['email'] . '>';
		
		if(parent::send()) {
			return true;
		}
		
		$this->log($this->to);
		$this->log($this->from);
		$this->log($this->smtpError);
		$this->log($this->smtpOptions); 
		
		return false;
	}

}

/** END OF FILE **/
