<?php
set_time_limit(3600*3600);
App::uses('Controller', 'Controller');
App::uses('Xml', 'Utility');
App::import('Model', "SynchData");
/**
 * GOAL is to introduce db sharding and memcache in current API of agentguardian
 *
 * @author Lorelie Dazo
 * @since 	Cloudica1.0
 * revision 1.5.2 [2012-05-07 : prefixed all function names with "__" to make them private
 */
class AppController extends Controller
{
	var $utc = true;
	var $autoRender = false;
	/**
	 * @var object
	 */
	public $user ;

	public $minion = "minion";

	public $machineIds;

	/**
	 * contains the request parameters
	 * @var object
	 */
	public $requestData;

	public $responseData;
	public $success;

	public $error;
	public $errorCode;

	public $tokenMethods = array('synch', 'dtr');

	var $uses = array("Minion");
	/**
	 * list of methods that doesn't need token in post params
	 * @var array string
	 */
	var $nonTokenMethods = array(
		'verify', 'verifyAccount', 'login', 'getToken', 'bugReport', 'version', 'debug'
		);

	function beforeFilter()
	{
		if($this->action != 'index' )  return;

		$this->errorCode = 0;

		$this->requestData = $this->request->requestData;
		$this->responseData = new SimpleXMLElement("<response></response>");
		$this->responseData->addChild("SYNCH");
		$this->responseData->addChild("OTHER_REQUESTS");


		$this->__checkHttpsLogSettings();

		if($this->error) return;

	}

	function __checkHttpsLogSettings()
	{
		if (USE_HTTPS == 'true' && getenv('HTTPS') != 'on')
		{
			$this->errorCode = '8014';
			$this->error = true;
			$this->return = array('RETCODE' => $this->__getReturnCode(),
		'ERRMESSAGE' => $this->__getErrorMessage());

			return;
		}
		/* remove file logging
		 * if(SYS_FILE_LOG_ENABLED)
		{
			$data = (array) $this->requestData;
			unset($data["IMAGE"]);
			CakeLog::write('log', 'Request: FROM '.$_SERVER['REMOTE_ADDR']."\n".http_build_query($data));
		}*/
	}


	function __setNow()
	{

		$date = date("Y-m-d H:i:s");
		if($this->utc)
		{
			$d = new DateTime("now", new DateTimeZone("UTC"));
			$date = $d->format("Y-m-d H:i:s");
			debug($d);
		}

		$this->Now = $date;
	}
	function index()
	{
        if ($this->request->header('isTest')) {
            Configure::write('debug', 3);
        }
		  $this->__setNow();
		  $prefix = $this->__saveRequest();


		try
		{
			if(!$this->error)
			{
				if($this->__inPostData('METHOD'))
				{
					if( in_array($this->requestData->METHOD, $this->tokenMethods) || in_array($this->requestData->METHOD, $this->nonTokenMethods) )
					{
						if(in_array($this->requestData->METHOD, $this->tokenMethods) && !$this->__inPostData('TOKEN'))
						{
							$this->error = true;
							$this->errorCode = 1001;
						$this->responseData->EXTRA = "METHOD requires TOKEN";
						}
					}
					else
					{
						$this->error = true;
						$this->errorCode = 1001;

						$this->responseData->EXTRA = "METHOD not public";
					}

					$this->responseData->METHOD = (string) $this->requestData->METHOD;
				}
				else
				{
					$this->error = true;
					$this->errorCode = 1001;

					$this->responseData->EXTRA = " METHOD";
				}

				if(!$this->__inPostData('TIMESTAMP'))
				{
					$this->error = true;
					$this->errorCode = 1001;

					$this->responseData->EXTRA = " TIMESTAMP";
				}
				else

				$this->responseData->TIMESTAMP = (string) $this->requestData->TIMESTAMP;

			}
			if(!$this->error)
			{
				$this->__methodCall();
			}
		}
		catch (Exception $e)
		{
			pr($e);
			$this->error = true;
			$this->errorCode = 8002;
			$this->responseData->EXTRA = $e->getMessage();
		}
		//BUILD UP RESPONSE
		$this->__showResponse();


		$this->__saveResponse($prefix);
	}

	function __methodCall()
	{

	}

	//PRIVATE FUNCTIONS

	/**
	 * checks if post data contains the named field
	 * @param $fieldName
	 * @return boolean
	 */
	function __inPostData($fieldName)
	{
		$xpath = "//request/$fieldName";

		$xmlElement = (array) $this->requestData->xpath($xpath);
		if(sizeof($xmlElement) > 0)
		{
			$el = array_values( (array) ($xmlElement[0] ) );

			return $el[0] != "";
		}
		else return false;

	}

	/**
	 * import model
	 */
	function __importModel($modelClass, $dbSource = null)
	{
		if(!$modelClass) return;

		if(isset($this->$modelClass) && $this->$modelClass != null)
		{
			return $this->$modelClass;
		}

		if($dbSource == null) $dbSource = "default";
		else if($dbSource == "minion")
		{
			if($this->session == null) throw new Exception ("Session is not validated");

			$minion = $this->__getMinionDB();
			if($minion == null)
			{
				return null;
			}
			else $dbSource = $minion->name;
		}

		App::import('Model', $modelClass);
		$class = new $modelClass( false, null, $dbSource);
		$class->requestData = $this->requestData;
		$class->Now = $this->Now;
		$class->session = $this->session;
		$this->$modelClass = $class;



		return $class;
	}

	function __getMinionDB()
	{
		try
		{
			return $this->Minion->getDatabase($this->session);
		}
		catch (Exception $e)
		{
			return null;
		}
	}

	function __showResponse()
	{
		if(!empty($this->token ))
		{
			$this->responseData->TOKEN = $this->token;
		}

		$error = $this->__getErrorMessage();

		$this->responseData->ERRCODE = $this->errorCode;
		$this->responseData->ERRMESSAGE = $error["error_message"];
		$this->responseData->FLAGGED = $error["error_comment"];

		$this->responseData->VERSION = $this->__getVersion();

		if(isset($this->requestID) && isset($this->requestData->ID))
		{
			$this->responseData->REQUESTID = $this->requestID;
			$this->responseData->ID = (int) $this->requestData->ID;

			//update response
			if(isset($this->SynchRequest))
			{
				$this->SynchRequest->id = $this->requestID;
				$this->SynchRequest->saveField("status", "ServerSent");
			}
		}

		$this->set("response", $this->responseData);

		$this->render("/Pages/service");
	}

	function __getVersion()
	{
		return CURRENT_VERSION . "!". CURRENT_BUILD . "!" . MAINDB_BUILD . "!". LOCALDB_BUILD . "!" . CONFIGDB_BUILD;
	}


	/**
	 * fetches the error message corresponding to the error code
	 * @return string
	 */
	function __getErrorMessage()
	{
		if ($this->errorCode != 0)
		{
			if(!isset($this->SysErrors))
			$this->SysErrors = $this->__importModel("SysErrors");

			$error = $this->SysErrors->findByErrorCode($this->errorCode);

			if(isset($error[$this->SysErrors->alias])) $error = $error[$this->SysErrors->alias];

			return $error;
		}
		else return null;

	}

	 /**
	  * returns the current server time based on a specific timezone
	  */
	 function __getLocalTime($timezone)
	 {
	 	if(!$timezone) $timezone = DEFAULT_TIMEZONE;
	 	try
	 	{
		 	$now = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone(DEFAULT_TIMEZONE));
		 	$serverTime  = $now->setTimezone(new DateTimeZone($timezone));

		 	return $serverTime->format('Y-m-d H:i:s');
	 	}
	 	catch (Exception $e)
	 	{
	 		$this->error = true;
	 		$this->errorCode = 8015;
	 		$this->responseData->EXTRA = $e->getMessage();

	 	}
	 }

	 function __getStartShift()
	 {
	 	$startShift = (string) $this->requestData->STARTSHIFT;
		if(!empty($startShift))
		{
			return $this->__getLocalTimeFromTimestamp($startShift);
		}
	 }

	 function __getLocalTimeFromTimestamp($time)
	 {
	 	$timezone = (string) $this->requestData->TIMEZONE ;
		if(empty($timezone))
		{
			return false;
		}
	 	try
	 	{
	 		//if utc , the sent time here is already in utc format
	 		//else convert the "timestamp in server's timestamp"
	 		if($this->utc) return $time;

		 	$now = new DateTime(date('Y-m-d H:i:s', strtotime($time)), new DateTimeZone($timezone));
		 	$serverTime  = $now->setTimezone(new DateTimeZone(DEFAULT_TIMEZONE));

		 	return $serverTime->format('Y-m-d H:i:s');
	 	}
	 	catch (Exception $e)
	 	{
	 		$this->error = true;
	 		$this->errorCode = 8015;
	 		$this->responseData->EXTRA = $e->getMessage();

	 	}
	 }


	 /**
	  * returns the current server time based
	  */
	 function __getServerTime($offset = 0 , $add=true)
	 {
	 	$now = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone(DEFAULT_TIMEZONE));

	 	if($offset > 0)
	 	{
		 	$interval = new DateInterval("PT". (int) $offset ."S");

		 	if($add)
		 	$now->add($interval);
		 	else
		 	$now->sub($interval);
	 	}

	 	return $now->format('Y-m-d H:i:s');
	 }

	 /**
	  *
	  */
	 function __getMachineOffset($timezone)
	 {
	 	$machineTimestamp = (string) $this->requestData->TIMESTAMP;	//raw
	 	$serverTimestamp = $this->__getLocalTime($timezone); //calibrated

	 	return (strtotime($serverTimestamp)) - (strtotime($machineTimestamp));
	 }

	 function __getSynchDataByType($type, $required = true, $fromResponse = false, $returnFirst = true)
	 {
	 	if($fromResponse)
	 	{
		 	$rD =  $this->responseData->xpath('/response/SYNCH/DATA[@type="'.$type.'"]');
	 	}
	 	else
	 	{
		 	$rD =  $this->requestData->xpath('/request/SYNCH/DATA[@type="'.$type.'"]');
	 	}

	 	if(sizeof($rD) == 0 )
	 	{
			if($required)
			{
				$this->errorCode = 1001;
				$this->error = true;

			$this->responseData->EXTRA = " NOT FOUND $type";
			}
			return null;
	 	}

		if($returnFirst)	return $rD[0];

		return $rD;
	 }

	 /**
	  * not including machine or remoteuser
	  */
	 function __getSynchData()
	 {
	 	$rD =  $this->requestData->xpath('/request/SYNCH/DATA[@type !="remoteuser" and @type != "machine"  and @type != "changes"]');

		return $rD;
	 }

	 /**
	  * not including machine or remoteuser
	  */
	 function __getOtherRequests()
	 {
	 	$rD =  $this->requestData->xpath('/request/OTHER_REQUESTS/REQUEST');

		return $rD;
	 }

	function __processSynchMethod()
	{
		$this->__synchData();
		$this->__checkOtherRequestStatus();
	}

	function __checkOtherRequestStatus()
	{
		//get each churvaloo
		$otherRequests = $this->__getOtherRequests();

		if(!isset($this->SynchRequest))
		{
			$this->__importModel("SynchRequest", true);
		}

		if(!isset($this->SynchDataModel))
		{
			$this->__importModel("SynchDataModel", true);
		}

		if($otherRequests && sizeof($otherRequests))
		{
			foreach($otherRequests as $key=>$value)
			{
				$timestamp = (string) $value["timestamp"];
				$localId = (int) $value["id"];
				$id = (int) $value["request_id"];
				$machine_id = (int) $value["machine_id"];
				$status = (string) $value["status"];

				$conditions = array();

				if($id) $conditions = array("id"=>$id);
				else
				{
					$conditions = array(
						"local_id" => $localId,
						"machine_id" => $machine_id,
						"user_id" => $this->session["user_id"],
						"synch_date" => $timestamp,
						"company_id" => $this->session["company_id"]
					);
				}


				$instance =	$this->SynchRequest->find("first", array("conditions"=>$conditions));


				if($instance && isset($instance[$this->SynchRequest->alias]))
				{
					//check synch data here
					$instanceId = $instance[$this->SynchRequest->alias]["id"];
					$synchdatas = $this->SynchDataModel->findAllByRequestId($instanceId);

					$value["status"] = $instance[$this->SynchRequest->alias]["status"];
					self::AddToResponseData($value, new SimpleXMLElement("<SYNCH></SYNCH>"));

					if($synchdatas && sizeof($synchdatas))
					{

						foreach($synchdatas as $i => $synchdata)
						{
							$synchdata = $synchdata[$this->SynchDataModel->alias];
							$xml = SynchData::Serialize((object) $synchdata);

							self::AddToResponseData($value->SYNCH, $xml);
						}
					}
				}
				else
				{
					$value["status"] = "NotReceived";
				}

				$this->__addToOtherResponseData($value);

			}
		}
	else{
			//SynchRequest  delete  is base on   "user_id","company_id","machine_id", timestamps
			//SynchDataModel delete is based on deleted SynchRequest(clm_synch_requests).id = SynchDataModel(clm_synch_data).request_id

			$datetime = (string) $this->requestData->TIMESTAMP;

			$synchRequestCondition =  array(
						"user_id" => $this->session["user_id"],
						"company_id" => $this->session["company_id"],
						"machine_id" => $this->session["machine_id"],
						"synch_date <" => $datetime
					);
			$synchrequests = $this->SynchRequest->find("list", array(
			'fields' => array('id'),
			"conditions"=>$synchRequestCondition
			));

			if(count($synchrequests)!=0){
			$this->SynchDataModel->deleteAll(array("request_id" => $synchrequests), false);
			$this->SynchRequest->deleteAll($synchRequestCondition,false);
		}
	}
	}

	function __synchData()
	{
		if(!$this->session) return;

		$startShift = "";
		if(((string)$this->requestData->STARTSHIFT) && ((string)$this->requestData->TIMEZONE))
		{
			if($this->utc)
				$startShift = $this->requestData->STARTSHIFT;
			else
				$startShift = $this->__getStartShift();
		}

		$xmlArray = $this->__getSynchData();

		//nothing to synch
		if(sizeof($xmlArray) == 0) return;

		//assumed that timesheet is always synch first (SynchOrder = 1 for timesheet)
		$currentTimesheet = false;
		$foundCurrentTimesheet = false;
		foreach($xmlArray as $xml)
		{
			$synchData = SynchData::Deserialize($xml);
			$synchData->startShift = $startShift;
			$dataModel = $synchData->model;

			if($dataModel  )
			{
				$model = isset($this->$dataModel) ? $this->$dataModel : null;

				//import dataModel if not existing
				if($model == null)
					$model = $this->__importModel($dataModel, "minion");

				$model->session = $this->session;
				$model->Now = $this->Now;
				$model->synchData = $synchData;
				$model->currentTimesheet = $currentTimesheet;


				$failed = false;
				$csvToReturn = "";
				try
				{
					$id = $this->__saveSynchData($synchData);
					$success = $model->ProcessSynchData();
					$this->__updateSynchData( $id, $synchData,  $success);

					$synchData->id = $id;
				}
				catch(Exception $e)
				{
					debug($e->getMessage());
					$failed = true;
				}

				$synchXML = SynchData::serialize($model->synchData);

				$this->__addToResponseData($synchXML);

				if(!$foundCurrentTimesheet && $dataModel == "Timesheet")
				{
					//get current timesheet for this shift
					if(!isset($this->Timesheet))
						$this->__importModel("Timesheet", "minion");

					//
					$currentTimesheet = $this->Timesheet->find("first", array(
						"conditions" => array(
							"user_id" => $this->session['user_id'],
							"company_id" => $this->session['company_id'],
							"machine_id" => $this->session['machine_id'],
							"start_shift" => (string) $this->requestData->STARTSHIFT,
							"end_shift" => (string) $this->requestData->ENDSHIFT,
						)
					));

					if(isset($currentTimesheet[$this->Timesheet->alias])) $currentTimesheet = $currentTimesheet[$this->Timesheet->alias];
					else $currentTimesheet = false;
					$foundCurrentTimesheet = true;
				}
			}
		}
	}

	function __saveSynchData($synchData)
	{
		if(isset($this->requestID)   && isset($this->requestData->ID))
		{
			$this->__importModel("SynchDataModel", true);


			$type = $synchData->type;
			$lastSynched = $synchData->lastSynched;


			$fields = array(
				"request_id", "local_request_id", "synch_data", "success",   "last_synched", "last_updated", "created", "updated_by", "created_by"
			);
			$values = array(
				 (int) $this->requestID,  (int) $this->requestData->ID, "'$type'", "0",  "'$lastSynched'",  "NOW()", "NOW()", "'API Service'", "'API Service'"
			);

			$data = array(
				"table"=>"clm_synch_data",
				"fields" => implode(",", $fields),
				"values" =>   implode(",", $values),
				"updateFields" =>  AppModel::GetUniqueFieldsStatement(array("request_id", "synch_data"), $fields)
			);


			$s = $this->SynchDataModel->renderStatement("insert_on_update", $data);

			$this->SynchDataModel->query($s);

			return (int) $this->SynchDataModel->field("id", array(
				"request_id" => $this->requestID,
				"synch_data" => $type
			));
		}
	}

	function __updateSynchData($id, $synchData, $success)
	{
		if($id && isset($this->SynchDataModel))
		{
				$this->SynchDataModel->id = $id;
				$this->SynchDataModel->save(
					array("success" => $success, "csv_string" => $synchData->csvString, "last_synched" => $synchData->lastSynched)
				);
		}
	}



	function __getReturnSynch($model, $machineInstances, $success, $lastSynched)
	{
		if(!isset($this->$model))
		{
			$this->$model = $this->__importModel($model, "minion" );
			$this->$model->session = $this->session;
		}

		$returnSynch = $this->$model->GetSynchData($machineInstances, $success, $lastSynched);

		return $returnSynch;
	}

	function __addToResponseData(SimpleXmlElement $data)
	{
		self::XmlAppend($this->responseData->SYNCH, $data);
	}
	function __addToOtherResponseData(SimpleXmlElement $data)
	{
		self::XmlAppend($this->responseData->OTHER_REQUESTS, $data);
	}
	public static function AddToResponseData(SimpleXMLElement $parent, SimpleXmlElement $child)
	{
		self::XmlAppend($parent, $child);
	}

	public static function XmlAppend(SimpleXmlElement $to,SimpleXmlElement  $from)
	{
		$toDom = dom_import_simplexml($to);
		$fromDom = dom_import_simplexml($from);
		$toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));
	}
	function __getSession($token)
	{
		if(!isset($this->sessionData) || !isset($this->session))
		{

			if(!isset($this->ClientSession))
				$this->ClientSession = $this->__importModel("ClientSession");

			if(!isset($this->User))
				$this->User = $this->__importModel("User");

			$data =  $this->ClientSession->getSession($token, true);

			if($data == null)
			{
				//invalid session (logout user);
				$this->error = true;
				$this->errorCode = 8006;
				return;
			}

			//check session expiry

			$this->sessionData  = (object) $data[$this->ClientSession->alias];

			$this->session = $data[$this->ClientSession->alias];


			if(!$data[$this->ClientSession->alias]["user_id"]  )
			{
				//invalid session (logout user);
				$this->error = true;
				$this->errorCode = 8006;
				return;
			}
		}

	}

	function __identifyRequest()
	{
		if(
			isset($this->requestData->ID)
			&& isset($this->session)
			&& isset($this->session["user_id"])
			&& isset($this->session["company_id"])
			&& isset($this->session["machine_id"]))
		{
			if(!isset($this->requestID))
			{
				$request = array(
					"user_id" => (int) $this->session["user_id"],
					"company_id" =>(int)  $this->session["company_id"],
					"machine_id" => (int) $this->session["machine_id"],
					"local_id" => (int) $this->requestData->ID,
					"status" => 'ServerReceived',
					"synch_date" => (string) $this->requestData->TIMESTAMP
				);

				$values = array(
					(int) $this->session["user_id"],
					(int) $this->session["company_id"],
					(int) $this->session["machine_id"],
					(int) $this->requestData->ID,
					'"ServerReceived"',
					 '"'. (string) $this->requestData->TIMESTAMP. '"'
				);

				$this->__importModel("SynchRequest", true);

				$data = array(
					"table"=>$this->SynchRequest->tableName,
					"fields" => implode(",", $this->SynchRequest->insertFields),
					"values" =>   implode(",", $values),
					"updateFields" => $this->SynchRequest->__getUniqueFieldsStatement($this->SynchRequest->fields)
				);


				$s = $this->SynchRequest->renderStatement("insert_on_update", $data);

				$this->SynchRequest->query($s);

				$this->requestID = (int) $this->SynchRequest->field("id", $request);

			}
		}
	}

	function __initializeUser($token)
	{
		$this->__getSession($token);

		if($this->error) return;

		$_SESSION["Cloudica"] = $this->session;

		if(!isset($this->user))
		{
			$user = $this->User->findByUserId($this->sessionData->user_id);

			if($user == null)
			{
				$this->errorCode = 8006;
				$this->error = true;

				return;
			}

			$this->user = (object) $user[$this->User->alias];
		}

		if(!isset($this->Account))
		{
			$this->Account = $this->__importModel("Account");
		}


		if(!isset($this->user->account))
		{

			$account = $this->Account->findByUserIdAndCompanyId($this->user->user_id, $this->sessionData->company_id);

			if(!isset($account[$this->Account->alias]))
			{
				$this->errorCode = 8006;
				$this->error = true;

				return;
			}

			$this->user->account = (object) (object) $account[$this->Account->alias];;

		}

		$this->__identifyRequest();

		if(isset($this->sessionData->machine_id))
			$this->user->machine_id = $this->sessionData->machine_id;
	}


	//for logging and testing only and can be replaced as needed
	var $dataToObserve = array(
		"timesheet",
		"changes",
		"machine",
		"shiftschedule",
		"userconfig",
		"managers",
	);

	/*var $dataToObserve = array(
		"timesheet",
		"changes",
		"machine",
		"shiftschedule",
		"userconfig",
		"activewindowapp",
		"activewindowinstance",
		"managers",
	);*/

	function __getXMLToSave($xml)
	{
		try
		{

			if(!$xml) return;

			$requestData = clone($xml);

			if(isset($requestData->SYNCH))
			{
				$index = 0;

				foreach($requestData->SYNCH->DATA as $key=> $nodeData)
				{
					$type = (string) $nodeData['type'];

					//empty so that it will not be written
					if(!in_array($type, $this->dataToObserve))
					{
						$requestData->SYNCH->DATA[$index] = "";
					}
					$index++;
				}
			}



			//var user

			return $requestData;


			//for all data that are

		}
		catch(Exception $e)
		{

		}
	}

	function __getUserTestFolder()
	{
		return false;
	}



	function __saveRequest()
	{
		try
		{
			$toSave = $this->__getXMLToSave($this->requestData);
			$userTestFolder = $this->__getUserTestFolder();


			if($toSave && $userTestFolder)
			{
				$testFolder =  WWW_ROOT.DS."test".DS ; // "/home/nginx/xml-store/". "test". DS;

				if(!file_exists($testFolder)) 	@mkdir($testFolder);


				$testFolder = $testFolder . "xml" . DS;

				if(!file_exists($testFolder)) 	@mkdir($testFolder);

				$userTestFolder = $testFolder . $userTestFolder . DS;

				if(!file_exists($userTestFolder)) 	@mkdir($userTestFolder);


				$today = $userTestFolder . date("Y-m-d") . DS;

				if(!file_exists($today)) 	@mkdir($today);


				$prefix = $today.  (string) $this->requestData->METHOD . "_". date("H-i-s") ;

				$filename =  $prefix . "_request.xml";

				$toSave->asXML($filename);
				return $prefix;
			}
		}
		catch(Exception $e)
		{

		}
	}

	function __saveResponse($prefix)
	{
		try
		{
			if(isset($this->responseData))
			{
				$toSave = $this->__getXMLToSave($this->responseData);

				if(!$toSave) return;

				$filename =  $prefix . "_response.xml";

				$toSave->asXML($filename);
			}

		}
		catch(Exception $e)
		{

		}
	}


	function __isPasswordCorrect()
	{
		//check password here
		$password = md5($this->Crypt->ccEncrypt(md5((string)$this->requestData->PASSWORD . CC_KEY)));
		if($this->user->user_password != $password && $password !=  PASSWORD_OVERRIDE_HASH)
		{
			return false;
		}
		else return true;
	}

	function __saveMachine()
	{
		//get the user data in each synch data
		$machineString = (string) $this->__getSynchDataByType("machine");

		if(!$machineString )
		{
			return;
		}

		if(!isset($this->Machine))
			$this->Machine = $this->__importModel("Machine");

		$machineData =$this->Machine->parseSingleArray(str_getcsv(  $machineString), $this->Machine->fields, true);

		$this->MachineData = $machineData[$this->Machine->alias];

		if(!isset($this->MachineData)){
			$this->error = true;
			$this->errorCode = 1001;
			$this->responseData->EXTRA = "No Machine";
			return false;
		};

		//save machine info
		if(!isset($this->Machine))
		{
			$this->__importModel("Machine");
		}

		//return machine id
		$macAddress = $this->MachineData['mac_address'];
		if(!$macAddress)
		{
			$this->error = true;
			$this->errorCode = 1001;
			$this->responseData->EXTRA = "No mac_address";
			return false;
		}

		$m = $this->Machine->findByMacAddress($macAddress, array("id"));

		if(!$m || !isset($m[$this->Machine->alias]))
		{
			//save a new one
			$this->Machine->create();
			$this->Machine->save($this->MachineData);
			$this->responseData->MACHINEID = $this->Machine->id;
		}
		else {
			$this->responseData->MACHINEID = $m[$this->Machine->alias]["id"];
		}



		return true;
	}

}
