<?php
App::import('Model', "Report");
class BugReportController extends AppController
{
	var $name = "BugReport";

	var $report;


	function __methodCall()
	{
		if(!$this->__inPostData("Report"))
		{
			$this->error = true;
			$this->errorCode = "1001";
			return;
		}

		$this->Bug = $this->__importModel("Bug");
		$this->BugReport = $this->__importModel("BugReport");

		$this->report = Report::Deserialize($this->requestData->Report, (string) $this->requestData->TIMESTAMP);

		//check if bug exists
		$bugId = $this->Bug->getBugId($this->report);

		if($bugId > 0)
		{
			$bugReportId = $this->BugReport->getBugReportId($this->report, $bugId);
		}
	}
}
/** END OF FILE **/