<?php
require_once("SynchController.php");
/**
 * Allows adding/updating dtr related data, different from synchronization
 *
 * @author Romeo Loyola
 * @since 	2014-04-02
 */

class DTRController extends SynchController
{
	var $name = "DTRController";

	var $sessionData;
	var $user;

	var $olds = array(
			"dtrnotes" => "tasknotes",
			"dtrreport" => "timesheetreport",
			"dtrtask" => "worktask"
		);

	var $oldVersion = false;

	function __methodCall()
	{
		 if(!$this->__inPostData("TOKEN"))
		{
			$this->error = true;
			$this->errorCode = 1001;
			return;
		}

		$token = (string) $this->requestData->TOKEN;

		$this->__initializeUser($token);

		 return parent::__methodCall();
	}


	function __processSynchMethod()
	{
		$method = (string) $this->requestData->ROUTINE;

		if(empty($method))
		{
			$this->error = true;
			$this->errorCode = 1001;
			return;
		}

		$method = "__" . $method;

		if(!method_exists($this, $method))
		{
			$this->error = true;
			$this->errorCode = 1001;
			return;
		}

		call_user_func(array($this, $method));
	}

	function __AddTask($update = false)
	{
 //CakeLog::write('ERROR',"AddTask");
		$this->__saveData("worktask");
	}

	function __UpdateTask()
	{
	// CakeLog::write('ERROR',"UpdateTask");
		$this->__saveData("worktask", true);
	}

	function __SaveReports()
	{
		//is the same as processing synch method
		$this->__saveData("timesheetreport");
	}

	function __UpdateReport()
	{
		$this->__saveData("timesheetreport", true);
	}

	function __SaveNotes()
	{
		$this->__saveData("tasknotes");
	}

	function __UpdateNotes()
	{
		$this->__saveData("tasknotes", true);
	}
	function __ImportData()
	{
		 parent::__processSynchMethod();
	}

	function __saveData($type = null, $update = false)
	{
		$xmlArray = array();
		if($type)
		{
			$xmlArray[] = $this->__getSynchDataByType($type);
		}
		else
		{
			$xmlArray = $this->__getSynchData();
		}

		 foreach($xmlArray as $data)
		 {
		 	if(!$data)
			{
				$this->error = true;
				$this->errorCode = 1001;
				return;
			}

			$synchData = SynchData::Deserialize($data);
			if(!$synchData || !isset($synchData->rows))
			{
				$this->error = true;
				$this->errorCode = 1001;
				return;
			}
			$stringModel = ($synchData->model);
			$model = $this->__importModel($stringModel, "minion");
			$this->$stringModel = $model;
			$model->session = $this->session;

			$newData = array();
			$errors = array();

			$lastRow;
			foreach($synchData->rows as &$row)
			{
				$model->__updateRow($row);

				$data = $model->parseSingleArray(str_getcsv($row), array_merge($model->fields, $model->automateFields));
				$lastRow = $data;

				$data["company_id"] = $this->session["company_id"];
				$id = $data["id"];
				$parentId = $data["parent_id"];

				if($id) {
					if(!$parentId) $parentId = $id;
					$data["parent_id"] = $id;
					$data["id"] = 0;
				}

				$model->create();


				if($model->save($data))
				{
					$data["id"] = $model->getInsertId();
					$data["is_synch"] = "Synched";
				}
				else
				{
					$data["id"] = 0;
					$validationError = $model->__getValidationErrors($data);
					$data["is_synch"] = "NotSynched";

					if(sizeof($validationError)  > 0)
					{
						$synchData = new SynchData();
						$synchData->csvString = implode("\n",$errors);
						$synchData->lastSynched = $this->Now;
						$synchData->type =$type."_error";
						$this->__addToResponseData(SynchData::Serialize($synchData));
					}
				}
				$data["session_id"] = 0;

				if($parentId)
				{
					$pData = $data;
					$model->id = $parentId;
					$pData["parent_id"] = 0;
					$pData["id"] = $parentId;
					$model->save($pData);

					$data = $pData;
				}


				$newData[] = array($model->alias=>$data);
			}

			if($lastRow && $model->saveLatestData)
			{
				$model->__saveLastData($lastRow);
			}
			$synchData = new SynchData();
			$synchData->csvString = $model->ToCSV($newData, $model->GetReturnFields());
			$synchData->lastSynched = $this->Now;
			$synchData->success = 1;
			$synchData->type = $type;
			$this->__addToResponseData(SynchData::Serialize($synchData));


		 }

	}

}
/** END OF FILE **/