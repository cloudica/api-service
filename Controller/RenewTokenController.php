<?php
require_once("VerifyController.php");

class RenewTokenController extends VerifyController
{
	var $name = "RenewTokenController";
	
	function __methodCall()
	{
		//get token
		if(!$this->__inPostData("TOKEN"))
		{
			$this->error = true;
			$this->errorCode = 1001; 
			return;
		}
		
		if(!isset($this->MSession))
			$this->MSession = $this->__importModel("MSession");
		
		//renew session
		$data =  $this->MSession->getSession($this->requestData->TOKEN, true); 

		if($data == null)
		{
			//invalid session (logout user);
			$this->error = true;
			$this->errorCode = 8006;
		}
		$this->__initializeUser($this->requestData->TOKEN);
		$this->__checkUserErrors();
		
		if($this->error) return;
		
		$this->__getUserResponse();
	}
	
	function __getUserResponse()
	{
		$this->responseData->OFFSET_TIME = $this->user->offset_time;;
		$this->responseData->TOKEN = $this->requestData->TOKEN;
		$this->responseData->TIMEZONE = $this->requestData->TIMEZONE;
	}

}