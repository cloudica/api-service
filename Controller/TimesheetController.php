<?php
/**
 * Used in timesheet calculation
 * @author Romeo Loyola
 * @since 	2014-12-05
 * http://api.localhost/Timesheet/calculate/TIMESHEETID
 */
 App::uses('TimesheetLogs', 'Lib/Cloudica');
class TimesheetController extends AppController
{
	var $name = "TimesheetCalculationController";

	var $timesheet;

	var $timesheetLogs;

	var $currentTimesheetLog;

	const ALLOWED_INTERVAL = 5;

	const DATEMINVALUE = "";

	const SIGNOUT_STATUS = 'SignOut';

	const ONBREAK_STATUS = 'OnBreak';

	const IDLE_STATUS = "Idle";

	const ACTIVE_STATUS = "Active";

	function index(){	}

	function calculate($timesheetId, $force){

		// set database to minion
		$this->initializedModels('minion');

		try
		{
			//recalc timesheet based on timesheetid
			$this->timesheet = $this->calculateTimesheet($timesheetId); // calculate timesheet id 5
			//reset logs
			$this->deleteTimesheetLogs($this->timesheet);
			//save new logs
			$this->saveTimesheetLogs($this->timesheetLogs);
			//update timesheet
			$this->updateTimesheet($this->timesheet);
		}
		catch (Exception $e)
		{
			CakeLog::write('TIMESHEET_ERROR', $e);
		}
	}

	function initializedModels($dbConfig ){

			$this->loadModel('Timesheet');

			$this->Timesheet->useDbConfig = $dbConfig;

			$this->loadModel('StatusLog');

			$this->StatusLog->useDbConfig = $dbConfig;

			$this->loadModel('MethodCall');

			$this->MethodCall->useDbConfig = $dbConfig;

			$this->loadModel('Shift');

			$this->Shift->useDbConfig = $dbConfig;
	}

	function updateTimesheet($timesheet){

		if($timesheet == null) return;

		$workTime = $timesheet['work_time'];

		$idleTime = $timesheet['idle_time'];

		$timesheetId = $timesheet['id'];

		$table =  "clm_" . $this->Timesheet->table;

		$sql = "UPDATE $table SET idle_time = $idleTime, work_time = $workTime, idle_time = $idleTime, last_updated = NOW(), last_calculated = NOW() WHERE id = $timesheetId";

		return $this->Timesheet->query($sql);
	}

	function saveTimesheetLogs($timesheetLogs){

		if($timesheetLogs == null||count($timesheetLogs) == 0) return;

		$data = array();

		foreach( $this->timesheetLogs as $val){
			$data[] = $val->toCsv($this->StatusLog->automateFieldsValues);
		}

		$values = implode(",", $data);

		$fields = implode(",", array_merge($this->StatusLog->fields,$this->StatusLog->automateFields));

		$table =  "clm_" . $this->StatusLog->table;

		$sql = "INSERT INTO $table ($fields) VALUES $values";

		return $this->StatusLog->query($sql);
	}

	function deleteTimesheetLogs($timesheet){

		if($timesheet == null) return;

		$table =  "clm_" . $this->StatusLog->table;

		$userId = $timesheet['user_id'];

		$timein = $timesheet['timein'];

		$timeout = $timesheet['timeout'];

		$sql = "DELETE FROM $table WHERE user_id = $userId and (start_time BETWEEN '$timein' AND '$timeout') and (end_time BETWEEN '$timein' AND '$timeout')";

		return $this->StatusLog->query($sql);
	}

	function setCurrentDate(){
		if(!isset($this->Now)){

			date_default_timezone_set ("Asia/Manila");

			$this->Now = date("Y-m-d H:i:s");
		}
	}

	function loadMethodCalls($timesheet)
	{
		$data = $this->MethodCall->find(
		  "all",
		  array(
			'conditions'=>array(
				 'MethodCall.user_id' =>$timesheet['user_id'],
				 'MethodCall.company_id' => $timesheet['company_id'],
				 'MethodCall.local_time >=' => $timesheet['timein'],
			 ),

			 'order' => "MethodCall.local_time ASC",
			)
		);

		$returnData = array();

		foreach ($data as $value){
			$returnData[] = $value['MethodCall'];
		 }

		  return $returnData;
	}


	function loadStatusLogs($timesheet)
	{
		 return $this->StatusLog->find(
		  "all",
		  array(
			 'conditions'=>array(
					 'StatusLog.user_id' => $timesheet['user_id'],
					 'StatusLog.company_id' => $timesheet['company_id'],
					 'StatusLog.start_time >=' => $timesheet['timein'],
					 'StatusLog.end_time <=' => $timesheet['timeout'],
				 ),

				 'order' => "StatusLog.start_time ASC",
			 )
		  );
	}

	function loadShift($shiftId)
	{
		$data = $this->Shift->find( "first", array( 'conditions'=>array( 'Shift.id' => $shiftId )));

		return $data["Shift"];
	}

	function loadTimesheet($timesheetId)
	{
		$this->Timesheet->fields = array("company_id", "user_id","shift_id", "timein",	"timeout","last_calculated","work_time","idle_time");

		$data = $this->Timesheet->find( "first", array( 'conditions'=>array( 'Timesheet.id' => $timesheetId )));

		return $data["Timesheet"];
	}

	function calculateTimesheet($timesheetId)
	{
		$timesheet = $this->loadTimesheet($timesheetId);

		$shift = $this->loadShift($timesheet['shift_id']);

		$shiftDate =  $this->getShiftDate($timesheet['timein'],$shift['start_day']);

		$uncalculated = $this->loadMethodCalls($timesheet);

		$count = count($uncalculated);

		for ($i = 0; $i < $count; $i++) {

		    $current = null;

            $next = null;

			$current = $uncalculated[$i];

			if ($i < $count - 1) $next = $uncalculated[$i + 1];

			if
			(
			   // if next has value
				isset($next)
				// and same id
				&& $current['id']!= $next['id']
				//and same date for current and next
                && date('Y-m-d', strtotime($current['local_time'])) ==  date('Y-m-d', strtotime($next['local_time']))
			){

				if($current['user_status'] != self::SIGNOUT_STATUS){

					$interval = $this->timeInMinutes($current['local_time'],$next['local_time']);

					if ($interval <= self::ALLOWED_INTERVAL){

						$timesheet['last_calculated'] = $current['local_time'];

						$log = $this->createLog($shiftDate,$current,  $next);

						$this->updateStatusLogs($log);

					}
                }
			}
		}

		//reset first
		$timesheet['work_time'] = 0;
		$timesheet['idle_time'] = 0;

		foreach($this->timesheetLogs as $val){

			$timesheet['work_time'] = $timesheet['work_time'] + $val->workTime;

			if( $val->status == self::IDLE_STATUS)
				$timesheet['idle_time'] = $timesheet['idle_time']+ $val->workTime;
		}

		return $timesheet;
	}

	function createLog($date,$current,  $next){

		$log = new TimesheetLogs();

		$log->statusMessage = $current['user_status_message'];

		$log->dataCompromised = $current['data_compromised'];

		$log->status = $current['user_status'];

		$log->companyId = $current['company_id'];

		$log->userId = $current['user_id'];

		$log->localTime = $date;

		$log->startTime = $current['local_time'];

		$log->machineStartTime = $current['machine_time'];

		$log->machineId= $current['machine_id'];

        if (!isset($next))
        {
            $log->endTime = self::DATEMINVALUE;

            $log->workTime = 0;
        }
        else
        {
            $log->endTime = $next['local_time'];

            $log->machineEndTime = $next['machine_time'];

            $log->workTime = $this->timeInMinutes($log->startTime,$log->endTime);
         }

        return $log;
	}

	function validateValue($val){
		if(!isset($val)) $val="";
		return $val;
	}

	function timeInMinutes($startTime,$endTime){
		return (strtotime($endTime) - strtotime($startTime)) / 60; // conver to minutes
	}

	function updateStatusLogs($log)
	{
	    if (!isset($log)) return;

		if(!isset($this->timesheetLogs)) $this->timesheetLogs = array();

		if(!isset($this->currentTimesheetLog))
		{
			$this->timesheetLogs = array();

			$this->timesheetLogs[] = $log;

			$this->currentTimesheetLog = $log;
		}
		else
		{
		    $updated = $this->currentTimesheetLog->add($log);

			if(!$updated){
			    //if log is not updated, add new log
                if ($log->status != self::SIGNOUT_STATUS)
                {
                    $this->timesheetLogs[] = $log;

                    $this->currentTimesheetLog = $log;
                }
			}
		}
	}

	function getShiftDate($date,$shiftStartDay)
	{
		return date('Y-m-d', strtotime('this week last sunday +' .$shiftStartDay .' days', strtotime($date)));
	}

}
/** END FILE **/