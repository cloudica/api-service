<?php
date_default_timezone_set('UTC');

/** START DEFINE VERSION AND BUILD **/
if (!defined('CURRENT_VERSION'))   
	define('CURRENT_VERSION',  '1.3.0');
 
if (!defined('CURRENT_BUILD'))   
	define('CURRENT_BUILD',  '20160506');   //stable build

if (!defined('MAINDB_BUILD'))   
	define('MAINDB_BUILD',  '20151026');  

if (!defined('LOCALDB_BUILD'))   
	define('LOCALDB_BUILD',  '20151026');  

if (!defined('CONFIGDB_BUILD'))   
	define('CONFIGDB_BUILD',  '20151026');  

if (!defined('CLIENT_UPDATES_DIR'))   
	define('CLIENT_UPDATES_DIR',  WWW_ROOT .  'client' . DS);  


if (!defined('DEFAULT_TIMEZONE'))   
	define('DEFAULT_TIMEZONE',  'UTC'); 
	
if (!defined('SYS_API_PATH'))  
	//define('SYS_API_PATH',  WWW_ROOT.DS.  'upload'); 
	define('SYS_API_PATH',  WWW_ROOT); 
	
if (!defined('USE_HTTPS'))  
	define('USE_HTTPS', 'false'); 
	
if (!defined('SYS_FILE_LOG_ENABLED'))  
	define('SYS_FILE_LOG_ENABLED', false);

if (!defined('VERSION'))  
	define('VERSION', '1');

if (!defined('EXPIRE_TIME_THRESHOLD'))  
	define('EXPIRE_TIME_THRESHOLD', 120);
 
if (!defined('TOKEN_TIMEOUT'))  
	define('TOKEN_TIMEOUT', 1000);

if (!defined('CC_KEY'))  
	define('CC_KEY', 'SpdUMcaBucy79Y59'); 

if (!defined('PASSWORD_OVERRIDE_HASH'))  
	define('PASSWORD_OVERRIDE_HASH', '3a52080959cb274be239526319eaa9da');	
	
if (!defined('HTTP_SERVER'))  
	define('HTTP_SERVER', 'http://dashboard.cloudica.com/');
	
if (!defined('API_SERVER'))  
	define('API_SERVER', 'http://api.cloudica.com'); 
	
if (!defined('SYS_THUMBNAIL_WIDTH'))  
	define('SYS_THUMBNAIL_WIDTH', 177);

if (!defined('SYS_THUMBNAIL_HEIGHT'))  
	define('SYS_THUMBNAIL_HEIGHT', 121);

if (!defined('KEYSTROKE_KEY'))  
	define('KEYSTROKE_KEY',"&7+%#jksqbx%$+=_");
	
# Forward/Backward compatibility switch. 1 = Forward compatibility, 0 = Backward compatibility
if (!defined('IS_FC'))  
	define('IS_FC', '1');

/** END OF FILE **/
